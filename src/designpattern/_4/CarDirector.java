package tang.quanwei._4;

public class CarDirector {
  private final CarBuilder builder;

  public CarDirector(CarBuilder builder) {
    this.builder = builder;
  }

  public void construct() {
    builder.buildAutoBody();
    builder.buildMotor();
    builder.buildTyre();
    builder.buildGearbox();
  }
}
