package tang.quanwei._4;

public abstract class CarBuilder {
  public abstract void buildAutoBody();

  public abstract void buildMotor();

  public abstract void buildTyre();

  public abstract void buildGearbox();

  public abstract Car getCar();
}
