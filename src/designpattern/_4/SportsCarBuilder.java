package tang.quanwei._4;

public class SportsCarBuilder extends CarBuilder {
  private final String TRAIT = "SportsCar ";

  private final Car car = new Car() {
    @Override
    public String toString() {
      return "SportsCar" + super.toString();
    }
  };

  public void buildAutoBody() {
    car.setAutoBody(TRAIT + "autoBody");
  }

  public void buildMotor() {
    car.setMotor(TRAIT + "motor");
  }

  public void buildTyre() {
    car.setTyre(TRAIT + "tyre");
  }

  public void buildGearbox() {
    car.setGearbox(TRAIT + "gearbox");
  }

  public Car getCar() {
    return car;
  }

}
