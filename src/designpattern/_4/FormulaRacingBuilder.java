package tang.quanwei._4;

public class FormulaRacingBuilder extends CarBuilder{
  private final Car car = new Car(){
    @Override
    public String toString() {
      return "方程式赛车"+super.toString();
    }
  };

  @Override
  public void buildAutoBody() {
    car.setAutoBody("方程式赛车车身");
  }

  @Override
  public void buildMotor() {
    car.setMotor("方程式赛车发动机");
  }

  @Override
  public void buildTyre() {
    car.setTyre("方程式赛车轮胎");
  }

  @Override
  public void buildGearbox() {
    car.setGearbox("方程式赛车变速箱");
  }

  @Override
  public Car getCar() {
    return car;
  }
}
