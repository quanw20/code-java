package tang.quanwei._6;

/**
 * 单例(Double Check Lock)
 */
public class VirtualUserGenerator_DCL {
    private static volatile VirtualUserGenerator_DCL INSTANCE;

    private VirtualUserGenerator_DCL() {
    }

    public static VirtualUserGenerator_DCL getInstance() {
        if (INSTANCE == null) {
            synchronized (VirtualUserGenerator_DCL.class) {
                if (INSTANCE == null) {
                    INSTANCE = new VirtualUserGenerator_DCL();
                }
            }
        }
        return INSTANCE;
    }

    public VirtualUser generate() {
        return new VirtualUser();
    }
}
