package tang.quanwei._3;

// 抽象数据库工厂类
public abstract class DBFactory {
  // 抽象方法，创建连接对象
  public abstract Connection createConnection();

  // 抽象方法，创建语句对象
  public abstract Statement createStatement();
}
