package tang.quanwei._3;

//自定义数据库连接对象Connection
public abstract class Connection {
  public abstract void connect();

  public abstract void disconnect();

  /**
   * 共有方法
   */
  public void status() {
    System.out.println("Connection.status");
  }
}
