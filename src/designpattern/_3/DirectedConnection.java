package tang.quanwei._3;

//
public class DirectedConnection extends Connection {
  @Override
  public void connect() {
    System.out.println("DirectConnection.connect");
  }

  @Override
  public void disconnect() {
    System.out.println("DirectConnection.disconnect");
  }
}
