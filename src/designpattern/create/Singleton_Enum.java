package designpattern.create;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;

public enum Singleton_Enum {
  INSTANCE;

  void say() {
    System.out.println(this.hashCode());
  }

  public static void main(String[] args) throws ClassNotFoundException, IOException {
    // testMThread();
    // testSer();
    // testClone();
  }

  private static void testClone() {
    Singleton_Enum instance = Singleton_Enum.INSTANCE;
    Singleton_Enum clone = null;
    try {
      clone = (Singleton_Enum) instance.clone();
    } catch (CloneNotSupportedException e) {
      System.out.println("clone not supported");
    }
    System.out.println(instance == clone);// 不支持克隆
  }

  private static void testMThread() {
    Runnable r = () -> {
      Singleton_Enum i = Singleton_Enum.INSTANCE;
      i.say();
    };
    new Thread(r).start();
    new Thread(r).start();
  }

  private static void testSer() throws IOException, ClassNotFoundException {
    Singleton_Enum instance = Singleton_Enum.INSTANCE;
    instance.say();
    var bos = new ByteArrayOutputStream(1024);
    var oos = new ObjectOutputStream(bos);
    oos.writeObject(instance);
    oos.close();
    var bis = new ByteArrayInputStream(bos.toByteArray());
    var ois = new ObjectInputStream(bis);
    var o = ois.readObject();
    ois.close();
    System.out.println(o == instance);// true
  }
}
