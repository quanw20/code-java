package designpattern.create;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Singleton Inner Class
 * 
 * 优点：无锁线程安全，延迟加载
 */
public class Singleton_InnerClass implements Serializable, Cloneable {
  static {
    System.out.println("outer init");
  }

  private Singleton_InnerClass() {
  }

  static class SingletonInner {
    static final Singleton_InnerClass INSTANCE = new Singleton_InnerClass();
    static {
      System.out.println("inner init");
    }
  }

  public static void hello() {
    System.out.println("hello called by Quanwei");
  }

  public static Singleton_InnerClass getInstance() {
    return SingletonInner.INSTANCE;
  }

  /**
   * 解决序列化破坏单例
   */
  // private Object readResolve() {
  // return SingletonInner.INSTANCE;
  // }

  /**
   * 解决克隆破坏单例
   */
  @Override
  protected Object clone() throws CloneNotSupportedException {
    return SingletonInner.INSTANCE;
  }

  public static void main(String[] args) throws IOException, ClassNotFoundException, CloneNotSupportedException {
    // System.out.println("***** test lazy ****");
    // testLazy();
    // System.out.println("***** test multi thread ****");
    // testMThread();
    // System.out.println("***** test serialize ****");
    // testSer();
    // System.out.println("***** test clone ****");
    // testClone();
    // System.out.println("***** test reflect ****");
    // testReflect();
    // System.out.println("***** test serialize2 ****");
  }

  /**
   * 反射破坏单例
   */
  private static void testReflect() {
    var instance = Singleton_InnerClass.getInstance();
    var clazz = instance.getClass();
    try {
      var constructor = clazz.getDeclaredConstructor();
      constructor.setAccessible(true);
      var o = constructor.newInstance();
      System.out.println(o == instance);// false 不是同一个对象
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void testClone() throws CloneNotSupportedException {
    var instance = Singleton_InnerClass.getInstance();
    var clone = (Singleton_InnerClass) instance.clone();
    System.out.println(instance == clone);// true 是同一个对象

  }

  /**
   * 测试序列化
   * 
   * 1. 序列化时，会调用writeObject方法
   * 2. writeObject方法会调用writeReplace方法
   * 3. writeReplace方法返回的对象，会被序列化
   * 
   * 1. 反序列化调用readObject方法
   * 2. readObject方法会调用readResolve方法
   * 3. readResolve方法返回的对象，会被反序列化
   */
  private static void testSer() throws IOException, ClassNotFoundException {
    var instance = Singleton_InnerClass.getInstance();
    var bos = new ByteArrayOutputStream(1024);
    var oos = new ObjectOutputStream(bos);
    oos.writeObject(instance);
    oos.close();
    var bis = new ByteArrayInputStream(bos.toByteArray());
    var ois = new ObjectInputStream(bis);
    var o = ois.readObject();
    ois.close();
    System.out.println(o == instance);// true
  }

  /**
   * 测试多线程
   * 两个线程访问输出的hashcode相同，表明是同一个对象
   */
  private static void testMThread() {
    Runnable r = () -> {
      var instance = Singleton_InnerClass.getInstance();
      System.out.println(instance);
    };
    new Thread(r).start();
    new Thread(r).start();
  }

  /**
   * 测试懒加载
   * 
   * 1. 加载外部类时，不会加载内部类
   * 2. 调用getInstance时，才会加载内部类
   * 3. 加载内部类时，会初始化静态变量INSTANCE
   * 4. 调用hello时，不会初始化静态变量INSTANCE
   */
  private static void testLazy() {
    System.out.println("step 1");
    Singleton_InnerClass.hello();
    System.out.println();
    var instance = Singleton_InnerClass.getInstance();
  }

  public static void testSer2() {
    try {
      // step[1] write 注释掉read
      System.out.println("write:" + testSer2Write());
      // step[2] read 注释掉write
      System.out.println("read:" + testSer2Read());
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static String testSer2Write() throws IOException, ClassNotFoundException {
    var instance = Singleton_InnerClass.getInstance();
    var bos = new FileOutputStream("./object.ser");
    var oos = new ObjectOutputStream(bos);
    oos.writeObject(instance);
    oos.close();
    return instance.hashCode() + "";
  }

  private static String testSer2Read() throws IOException, ClassNotFoundException {
    var bis = new FileInputStream("./object.ser");
    var ois = new ObjectInputStream(bis);
    var o = ois.readObject();
    ois.close();
    return o.hashCode() + "";
  }
}
