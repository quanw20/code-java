package designpattern.create;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Singleton 懒汉式
 * 
 * 优点：线程安全
 * 缺点：无法延迟加载
 */
public class Singleton implements Serializable, Cloneable {
    private Singleton() {
    }

    private static final Singleton INSTANCE = new Singleton();

    public static Singleton getInstance() {
        return INSTANCE;
    }

    private Object readResolve() {
        return INSTANCE;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return INSTANCE;
    }

    public void say() {
        System.out.println(this.hashCode());
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException, CloneNotSupportedException {
        // testMThread();
        // testSer();
        // testClone();
    }

    private static void testClone() throws CloneNotSupportedException {
        var instance = Singleton.getInstance();
        var clone = (Singleton) instance.clone();
        System.out.println(instance == clone);// true 是同一个对象
    }

    private static void testSer() throws IOException, ClassNotFoundException {
        var instance = Singleton.getInstance();
        var bos = new ByteArrayOutputStream(1024);
        var oos = new ObjectOutputStream(bos);
        oos.writeObject(instance);
        oos.close();
        var bis = new ByteArrayInputStream(bos.toByteArray());
        var ois = new ObjectInputStream(bis);
        var o = ois.readObject();
        ois.close();
        System.out.println(o == instance);// true 是同一个对象

    }

    private static void testMThread() {
        Runnable r = () -> {
            Singleton i = Singleton.getInstance();
            i.say();
        };
        new Thread(r).start();
        new Thread(r).start();
    }

}