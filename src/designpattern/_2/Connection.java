package tang.quanwei._2;

public abstract class Connection {
  public abstract void connect();
  public abstract void disconnect();
}
