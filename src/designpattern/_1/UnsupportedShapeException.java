package tang.quanwei._1;

public class UnsupportedShapeException extends Exception {
  public UnsupportedShapeException(String s) {
    super(s);
  }
}
