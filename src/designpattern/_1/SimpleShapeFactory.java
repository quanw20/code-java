package tang.quanwei._1;

public class SimpleShapeFactory {
  public static final String CIRCLE = "CIRCLE";
  public static final String TRIANGLE = "TRIANGLE";
  public static final String RECTANGLE = "RECTANGLE";

  public static Shape getShape(String shapeType) throws UnsupportedShapeException {
    return switch (shapeType.toUpperCase()) {
      case "CIRCLE" -> new Circle();
      case "TRIANGLE" -> new Triangle();
      case "RECTANGLE" -> new Rectangle();
      default -> throw new UnsupportedShapeException("Unsupported shape: " + shapeType);
    };
  }
}
