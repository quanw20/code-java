package tang.quanwei._5;

import java.util.Scanner;

/**
 * 在复制简历时，用户可以选择是否复制简历中的照片：
 * 如果选择“是”，则照片将一同被复制，用户对新简历中的照片进行修改不会影响到简历模板中的照片，
 * 对模板进行修改也不会影响到新简历；
 * <p>
 * 如果选择“否”，则直接引用简历模板中的照片，
 * 修改简历模板中的照片将导致新简历中的照片一同修改，
 * 反之亦然。
 * <p>
 * 现采用原型模式设计该简历复制功能并提供浅克隆和深克隆两套实现方案
 */
public class Resume implements Prototype {
  private Object photo;

  public Resume() {
    this.photo = new Object();
  }

  public Object getPhoto() {
    return photo;
  }

  public void setPhoto(Object photo) {
    this.photo = photo;
  }

  @Override
  public Resume clone()  {
    Resume resume = new Resume();
    Scanner scanner = new Scanner(System.in);
    System.out.println("是否复制照片？(Y/N)");
    String choice = scanner.next();
    scanner.close();
    if ("Y".equalsIgnoreCase(choice)) {
      resume.setPhoto(this.photo);//浅克隆
    } else {
      resume.setPhoto(new Object());//深克隆
    }
    return resume;
  }
}
