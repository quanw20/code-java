package tang.quanwei._5;

public interface Prototype {
  Prototype clone();
}
