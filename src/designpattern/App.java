package tang.quanwei;

import tang.quanwei._2.ConnectionFactory;
import tang.quanwei._2.ConnectionFactoryImpl;
import tang.quanwei._2.IMAPConnection;
import tang.quanwei._2.POP3Connection;

public class App {
  public static void main(String[] args) {
    // test factory
    ConnectionFactory factory = new ConnectionFactoryImpl();
    factory.getConnection(POP3Connection.class).connect();
    factory.getConnection(POP3Connection.class).disconnect();
    factory.getConnection(IMAPConnection.class).connect();
    factory.getConnection(IMAPConnection.class).disconnect();
  }
}
