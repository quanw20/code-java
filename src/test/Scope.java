package test;

class Singleton {
  public static final Singleton SINGLETON = new Singleton();

  void say() {
    System.out.println(this);
  }

  private Singleton() {
  }
}

public class Scope {

  public static void main(String[] args) {
    Singleton s1 = Singleton.SINGLETON;
    Singleton s2 = Singleton.SINGLETON;
    s1.say();
    s2.say();
    System.out.println(s1.equals(s2));
  }
}