package newskill.sugar;

// import static net.mindview.util.Range.*;

public class For {
    public static int[] range(int n) {
        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            ret[i] = i;
        }
        return ret;
    }
}
