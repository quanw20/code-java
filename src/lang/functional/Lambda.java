package lang.functional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Lambda {
    public static void main(String[] args) {
        ArrayList<Integer> collect = Stream
                .of(new Integer[] { 1, 2, 3 }, new Integer[] { 4, 5, 6 }, new Integer[] { 7, 8, 9 })
                .flatMap(a -> Arrays.stream(a))
                .sorted((a, b) -> b - a)
                .filter(new Predicate<Integer>() {
                    public boolean test(Integer a) {
                        return a > 2;
                    }
                }.and(new Predicate<Integer>() {
                    public boolean test(Integer a) {
                        return a < 8;
                    }
                }))
                .collect(ArrayList::new, (acc, t) -> acc.add(t), ArrayList::addAll);
        collect.forEach(a -> System.out.println(a));
        Optional<ArrayList<Integer>> oa = Optional.ofNullable(collect);
        oa.ifPresent(a -> a.forEach(System.out::println));
    }
}
