package lang.functional;

public class Closure {
  @FunctionalInterface
  interface IAdder {
    int add();
  }

  IAdder getAdder() {
    int[] a = {1};// final 指针 包装
    class Inner implements IAdder {
      @Override
      public int add() {
        return ++a[0];// 修改外部变量
      }
    }
    return new Inner();

  }

  IAdder getAdder2() {
    int[] a = {1};// final 指针 包装
    return () -> ++a[0];
  }

  public static void main(String[] args) {
    Closure c = new Closure();
    IAdder adder = c.getAdder();
    adder.add();
    adder.add();
    int add = adder.add();
    System.out.println(add);//4

    IAdder adder2 = c.getAdder2();
    adder2.add();
    adder2.add();
    int add2 = adder2.add();
    System.out.println(add2);//4
  }
}