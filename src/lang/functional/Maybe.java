package lang.functional;

import java.util.Objects;
import java.util.function.Function;

public class Maybe<A> {
  public static void main(String[] args) {
    Function<String, Maybe<Integer>> f = s -> new Maybe<>(Integer.parseInt(s));
    Function<Integer, Maybe<Double>> g = i -> new Maybe<>(i / 2.0);
    Maybe<Double> result = new Maybe<>("123")
        .flatMap(f)
        .flatMap(g);
    System.out.println(result);
  }

  private final A value;

  public Maybe(A value) {
    this.value = value;
  }

  public <B> Maybe<B> map(Function<? super A, ? extends B> f) {
    if (value != null) {
      return new Maybe<>(f.apply(value));
    } else {
      return new Maybe<>(null);
    }
  }

  public <B> Maybe<B> flatMap(Function<? super A, ? extends Maybe<? extends B>> f) {
    if (value != null) {
      return f.apply(value).map(Function.identity());
    } else {
      return new Maybe<>(null);
    }
  }

  public A get() {
    return value;
  }

  @Override
  public String toString() {
    return "Maybe " + value ;
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Maybe other = (Maybe) obj;
    return Objects.equals(value, other.value);
  }

}
