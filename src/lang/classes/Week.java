package lang.classes;

/**
 * 1）使用enum定义的枚举类
 * 默认继承了java.lang.Enum，
 * 实现了java.lang.Comparable接口，
 * 且不能继承其他类，也不可以被继承。
 * 但枚举类可以实现一个或多个接口。
 * 
 * 2）枚举类的所有实例必须放在第一行显示，
 * 不需使用new，
 * 不需显示调用构造方法，
 * 每个变量都是public static final修饰的，
 * 最终以分号结束。
 * 
 * 3）枚举类的构造方法是私有的，
 * 默认的就是private，定义的时候不加也没事。
 * 
 * 4）switch()参数可以使用enum。
 * 
 * 5）非抽象枚举类默认是final的
 * 但定义的时候加上final却编译不通过。
 * 
 * 6）枚举类可以有抽象方法，
 * 但是必须在它的实例中实现。
 */
public enum Week {
    MONDAY(0, "星期一") {
        @Override
        public Week next() {
            return TUESDAY;
        }
    },
    TUESDAY(1, "星期二") {
        @Override
        public Week next() {
            return WEDNESDAY;
        }
    },
    WEDNESDAY(2, "星期三") {
        @Override
        public Week next() {
            return THURSDAY;
        }
    },
    THURSDAY(3, "星期四") {
        @Override
        public Week next() {
            return FRIDAY;
        }
    },
    FRIDAY(4, "星期五") {
        @Override
        public Week next() {
            return SATURDAY;
        }
    },
    SATURDAY(5, "星期六") {
        @Override
        public Week next() {
            return SUNDAY;
        }
    },
    SUNDAY(6, "星期日") {
        @Override
        public Week next() {
            return MONDAY;
        }
    };// 最后一个类型必须要用分号结束

    int num;
    String desc;

    // 编译后所有实例都会成为内部类，相当于每个实例用匿名内部类的形式实现next的方法
    abstract Week next();

    /**
     * 构造方法必然是private修饰的
     * 就算不写，也是默认的
     *
     * @param num
     * @param desc
     */
    Week(int num, String desc) {
        this.num = num;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public int getNum() {
        return num;
    }

    @Override
    public String toString() {
        // switch支持Enum类型
        switch (this) {
            case MONDAY:
                return "今天星期一";
            case TUESDAY:
                return "今天星期二";
            case WEDNESDAY:
                return "今天星期三";
            case THURSDAY:
                return "今天星期四";
            case FRIDAY:
                return "今天星期五";
            case SATURDAY:
                return "今天星期六";
            case SUNDAY:
                return "今天星期日";
            default:
                return "Unknow Day";
        }
    }
}
/*
Compiled from "Week.java"
public abstract class lang.classes.Week extends java.lang.Enum<lang.classes.Week> {
  public static final lang.classes.Week MONDAY;
  public static final lang.classes.Week TUESDAY;
  public static final lang.classes.Week WEDNESDAY;
  public static final lang.classes.Week THURSDAY;
  public static final lang.classes.Week FRIDAY;
  public static final lang.classes.Week SATURDAY;
  public static final lang.classes.Week SUNDAY;
  int num;
  java.lang.String desc;
  private static final lang.classes.Week[] $VALUES;
  public static lang.classes.Week[] values();
  public static lang.classes.Week valueOf(java.lang.String);
  abstract lang.classes.Week next();
  private lang.classes.Week(int, java.lang.String);
  public java.lang.String getDesc();
  public int getNum();
  public java.lang.String toString();
  public static void main(java.lang.String[]);
  private static lang.classes.Week[] $values();
  static {};
}
 */