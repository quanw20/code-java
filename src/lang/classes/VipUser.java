package lang.classes;

public class VipUser extends User {
    private int vipNum;

    @Override
    public String toString() {
        return super.toString() + "\tvipNum: " + getVipNum();
    }

    /**
     * @return the vipNum
     */
    public int getVipNum() {
        return vipNum;
    }

    /**
     * @param vipNum the vipNum to set
     */
    public void setVipNum(int vipNum) {
        this.vipNum = vipNum;
    }

    public static void main(String[] args) {
        User user = new VipUser();
        user.setUsername("Quanwei");
        if (user instanceof VipUser vip) {
            vip.setVipNum(5432);
            System.out.println(vip.getUsername());// Quanwei
            System.out.println(vip);
        }
    }
}
