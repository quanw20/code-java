package lang.classes;

import java.lang.reflect.Method;
import java.util.Random;

import org.junit.jupiter.api.Test;

public class RandomDemo {
    @Test
    public void test() throws Exception {

        Class<Random> c = Random.class;
        Method nextInt = c.getDeclaredMethod("nextInt", int.class);
        Object invoke = nextInt.invoke(c.getConstructor().newInstance(), 10);
        System.out.println(invoke);

    }
}
