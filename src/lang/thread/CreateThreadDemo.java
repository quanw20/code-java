package lang.thread;

import org.junit.jupiter.api.Test;
/**
 * 创建线程的几种方式
 */
public class CreateThreadDemo {

    @Test
    public void testLambda() {
        new Thread(() -> {
            String name = Thread.currentThread().getName();
            System.out.println(name);// Quanwei's thread
        }, "Quanwei's thread").start();
    }

    @Test
    public void testExtends() {
        new Thread() {
            public void run() {
                String name = Thread.currentThread().getName();
                System.out.println("Extends" + name);// ExtendsThread-0 也可能不是0
            }
        }.start();
    }

    @Test
    public void testImpelements() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                String name = Thread.currentThread().getName();
                System.out.println(name);// Impelements
            }
        };
        new Thread(runnable,"Impelements").start();
    }

    public static void main(String[] args) {
        new CreateThreadDemo().testLambda();// main
        System.out.println(Thread.currentThread().getName());
    }

}
