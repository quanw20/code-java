package lang.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class AsyncCompute {

    public static void main(String[] args) {
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 1203;
            }
        };
        FutureTask<Integer> futureTask = new FutureTask<>(callable);
        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            while (true)
                if (futureTask.isDone()) {
                    Integer integer = futureTask.get();
                    System.out.println(integer);
                    break;
                }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        ExecutorService pool = Executors.newCachedThreadPool();
        Future<?> submit = pool.submit(futureTask);
        try {
            while (true)
                if (submit.isDone()) {
                    Integer integer = (Integer) submit.get();
                    System.out.println(integer);
                    break;
                }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

}
