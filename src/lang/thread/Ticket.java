package lang.thread;

import java.util.concurrent.locks.ReentrantLock;

public class Ticket {
    public static void main(String[] args) {
        var w = new Windows1();
        var w1 = new Thread(w, "w1");
        var w2 = new Thread(w, "w2");
        var w3 = new Thread(w, "w3");
        w2.setPriority(Thread.MAX_PRIORITY);
        w1.start();
        w2.start();
        w3.start();
    }


}
/**
 * 多线程操作共享数据,为保证线程安全需要给线程加上一把锁(同步监视器).
 * 
 * 操作同步代码时,只允许一个线程参与其他线程等待,相当于单线程
 * 
 * 方式一: 同步代码块
 * 
 * 方式二: 同步方法
 * 
 */
class Windows0 implements Runnable {
    private int tickets = 50;// 共享数据

    @Override
    public void run() {
        while (true) {
            if (tickets > 0) {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " : " + tickets--);
            } else
                break;
        }
    }
}

/**
 * 同步代码块
 * 
 * synchronized(同步监视器){//同步监视器可用: 类.class(保证唯一)
 * 
 * //要同步的代码
 * 
 * }
 * 
 * 
 * 不需要synchronized的操作 JVM规范定义了几种原子操作：
 * 
 * 基本类型（long和double除外）赋值，例如：int n = m； 引用类型赋值，例如：List<String> list =
 * anotherList。
 * 
 * long和double是64位数据，JVM没有明确规定64位赋值操作是不是一个原子操作，不过在x64平台的JVM是把long和double的赋值作为原子操作实现的
 * 
 * 单条原子操作不需要线程同步,多条需要
 */
class Windows1 implements Runnable {
    private int tickets = 50;
    @Override
    public void run() {
        while (true) {
            synchronized (Windows1.class) {
                if (tickets > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                    }
                    System.out.println(Thread.currentThread().getName() + " : " + tickets);
                    --tickets;
                } else
                    break;
            }
        }
    }

}

/**
 * 同步方法
 */
class Windows2 implements Runnable {
    private static int sTickets = 50;
    private int tickets = 50;
    @Override
    public void run() {
        while (true) {
            sShow();
            if (tickets <= 0)
                break;
        }
    }

    synchronized void show() {// 同步监视器this
        if (tickets > 0) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            System.out.println(Thread.currentThread().getName() + " : " + tickets);
            --tickets;
        }
    }

    static synchronized void sShow() {// 同步监视器this
        if (sTickets > 0) {
            try {
                Thread.sleep(100);
            } catch (Exception e) {
            }
            System.out.println(Thread.currentThread().getName() + " : " + sTickets--);
        }
    }
}

/**
 * Look
 */
class Windows3 implements Runnable {
    private int tickets = 50;
    private ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {
        while (true) {
            try {
                lock.lock();
                if (tickets > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + " : " + tickets--);
                } else
                    break;
            } finally {
                lock.unlock();
            }
        }

    }

}

/**
 * 多线程操作共享数据,为保证线程安全需要给线程加上一把锁(同步监视器).
 * 
 * 操作同步代码时,只允许一个线程参与其他线程等待,相当于单线程
 * 
 * 方式一: 同步代码块
 * 
 * 方式二: 同步方法
 * 
 */
class Windows4 implements Runnable {
    private volatile int tickets = 50;// 共享数据

    @Override
    public void run() {
        while (true) {
            if (tickets > 0) {
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + " : " + tickets--);
            } else
                break;
        }
    }
}
