package lang.jdbc.anno;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;

import static lang.jdbc.MySql.*;

public class Dao {
    private static Connection connection = null;
    static {
        connection = getConnection();
    }

    @Insert("insert into login values(null,?,?)")
    public static void insert(String username, String password) {
        Insert insert = null;
        try {
            Method insertMethod = Dao.class.getMethod("insert", String.class, String.class);
            insert = insertMethod.getAnnotation(Insert.class);
            String sql = insert.value();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            int rows = ps.executeUpdate();
            System.out.println(rows);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        insert("username", "password");
    }
}
