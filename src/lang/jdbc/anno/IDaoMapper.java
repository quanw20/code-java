package lang.jdbc.anno;

public interface IDaoMapper {
    @Insert("insert into login values(null,?,?)")
    void insert(String username, String password);

    @Delete("")
    void delete();

    @Update("")
    void update();

    @Select("")
    void select();
}
