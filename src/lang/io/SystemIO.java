package lang.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.stream.Stream;

public class SystemIO {
    // D:\workspaceFolder\CODE_JAVA jvm起动目录
    private static final String userDir = System.getProperty("user.dir");

    // Scanner使用分隔符模式将其输入拆分为段落，该模式默认匹配空格。然后可以使用各种next方法将产生的标记转换为不同类型的值
    public void test_Scanner() {
        Scanner in = new Scanner("3:(3) 1 4 2");
        Stream<MatchResult> fa = in.findAll("(\\d+)");
        fa.forEach(x -> System.out.println(x.start()));
        in.close();
    }

    public void test_Scanner_1() {
        Scanner in = new Scanner("32:(31) 1 2 3 4\n");
        // in.useDelimiter("\\D");
        // String findInLine = in.findInLine("(\\d+)");
        // System.out.println(findInLine);
        // MatchResult match = in.match();
        // for (int i = match.start(); i <= match.groupCount(); ++i)
        // System.out.println(match.group(i));
        // 此接口包含用于确定与正则表达式匹配的结果的查询方法。通过 MatchResult 可以看到匹配边界、组和组边界，但不能修改。
        // while (in.hasNext()) {
        // String s = in.nextLine();
        // System.out.println(s);
        // if (!s.equals(""))
        // System.out.println(Integer.parseInt(s));
        // }
        in.close();
    }

    public void test_PrintWriter() {
        var pt = new PrintWriter(System.out);
        pt.println(userDir);
        pt.close();
    }

    public void test_ScannerFileIO() {
        Scanner in = null;
        PrintWriter pt = null;
        try {
            in = new Scanner(Path.of(userDir, "TODO.md"), StandardCharsets.UTF_8);
            pt = new PrintWriter("a.txt", StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
            if (pt != null)
                pt.close();
        }
        while (in.hasNextLine()) {
            String nextLine = in.nextLine();
            System.out.println(nextLine);
            pt.write(nextLine);
            pt.println();
        }
        in.close();
        pt.close();
    }

    public void testPat() {
        String input = "1 fish 2 fish red fish blue fish";
        Scanner s = new Scanner(input);
        String findInLine = s.findInLine("(\\d+) fish (\\d+) fish (\\w+) fish (\\w+)");
        System.out.println(findInLine);
        MatchResult result = s.match();
        for (int i = 1; i <= result.groupCount(); i++)
            System.out.println(result.group(i));
        s.close();
    }

    public static void main(String[] args) {
        SystemIO io = new SystemIO();
        io.test_Scanner();

    }
}
