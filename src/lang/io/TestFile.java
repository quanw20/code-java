package lang.io;

import java.io.File;
import java.io.IOException;


/**
 * File表示一个文件或一个文件夹 在java.io包下
 */
public class TestFile {
    // 构造一个File对象，并不会导致任何磁盘操作
    // 文件夹
    File floder = new File("D:\\workspaceFolder\\CODE_JAVA");
    // 文件
    File file = new File("D:\\workspaceFolder\\CODE_JAVA\\a.txt");

    public void testGet() {
        // 返回绝对路径，
        System.out.println(file);
        // 返回构造方法传入的路径，
        System.out.println(file.getPath());
        // 返回绝对路径，
        System.out.println(file.getAbsolutePath());
        try { // 返回绝对路径，
            System.out.println(file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 输出文件夹下文件
        for (var i : floder.listFiles()) {
            System.out.println(i);
        }
    }

    public void testCreate() {
        File f = new File("test.file");
        if (!f.exists()) {
            try {
                // 创建
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void testDelete() {
        File f = new File("test.file");
        if (f.exists()) {
            // 删除
            boolean delete = f.delete();
            System.out.println(delete?"delete":"not exist");
        }
    }

}
