package lang.io.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Reactor implements Runnable {

  final Selector selector;
  final ServerSocketChannel serverSocket;

  // 初始化reactor，绑定监听端口
  public Reactor(int port) throws IOException {
    selector = Selector.open();
    serverSocket = ServerSocketChannel.open();
    serverSocket.socket().bind(new InetSocketAddress(port));
    serverSocket.configureBlocking(false);
    // 注册事件
    SelectionKey sk = serverSocket.register(selector, SelectionKey.OP_ACCEPT);
    sk.attach(new Acceptor());
  }

  @Override
  public void run() { // normally in a new Thread
    try {// 监听事件循环
      while (!Thread.interrupted()) {
        selector.select();
        Set<SelectionKey> selected = selector.selectedKeys();
        Iterator<SelectionKey> it = selected.iterator();
        while (it.hasNext())
          dispatch(it.next());
        selected.clear();
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  void dispatch(SelectionKey k) {
    Runnable r = (Runnable) (k.attachment());
    if (r != null)
      r.run();
  }

  class Acceptor implements Runnable { // inner

    @Override
    public void run() {
      try {
        SocketChannel channel = serverSocket.accept();
        if (channel != null)
          new Handler(selector, channel);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}

final class Handler implements Runnable {
  final int MAXIN = 1024 * 10;
  final int MAXOUT = 1024 * 10;
  final Selector selector;
  final ByteBuffer input = ByteBuffer.allocate(MAXIN);
  final ByteBuffer output = ByteBuffer.allocate(MAXOUT);
  final SocketChannel channel;
  SelectionKey sk;

  public Handler(Selector selector, SocketChannel c) throws IOException {
    channel = c;
    this.selector = selector;
    channel.configureBlocking(false);
    sk = channel.register(selector, 0);
    sk.attach(this);
    sk.interestOps(SelectionKey.OP_READ);
    selector.wakeup();
  }

  @Override
  public void run() {
    try {
      if (sk.isReadable())
        read();
      else if (sk.isWritable())
        send();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  synchronized void read() throws IOException {
    int len;
    input.clear();
    len = channel.read(input);
    if (len == -1) {
      sk.cancel();
      return;
    }
    sk.interestOps(SelectionKey.OP_WRITE);
  }

  synchronized void send() throws IOException {
    output.flip();
    channel.write(output);
    output.clear();
    sk.interestOps(SelectionKey.OP_READ);
  }
}
