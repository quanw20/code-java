package lang.io.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.net.InetSocketAddress;
import java.util.Iterator;

/**
 * 服务端监听来自任意客户端的连接请求，并读取/写入该客户端发送的数据
 */
public class ReactorServer {
  private final int port;
  private Selector selector;

  public ReactorServer(int port) {
    this.port = port;
  }

  public void start() throws IOException {
    ServerSocketChannel serverChannel = ServerSocketChannel.open();
    serverChannel.configureBlocking(false);
    serverChannel.bind(new InetSocketAddress(port));

    selector = Selector.open();
    serverChannel.register(selector, SelectionKey.OP_ACCEPT);

    while (true) {
      selector.select();
      Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
      while (iter.hasNext()) {
        SelectionKey key = iter.next();
        iter.remove();

        if (!key.isValid())
          continue;

        if (key.isAcceptable()) {
          accept(key);
        } else if (key.isReadable()) {
          read(key);
        } else if (key.isWritable()) {
          write(key);
        }
      }
    }
  }

  private void accept(SelectionKey key) throws IOException {
    ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
    SocketChannel channel = serverChannel.accept();

    if (channel != null) {
      System.out.println("Accepted from " + channel.getRemoteAddress());
      channel.configureBlocking(false);

      SelectionKey clientKey = channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
      ByteBuffer buffer = ByteBuffer.allocate(1024);
      clientKey.attach(buffer);
    }
  }

  private void read(SelectionKey key) throws IOException {
    SocketChannel channel = (SocketChannel) key.channel();

    ByteBuffer buffer = ByteBuffer.allocate(1024);
    int numRead = -1;
    try {
      numRead = channel.read(buffer);
    } catch (IOException e) {
      System.out.println("Client closed: " + channel.getRemoteAddress());
      key.cancel();
      return;
    }

    if (numRead == -1) {
      System.out.println("Client disconnected: " + channel.getRemoteAddress());
      key.cancel();
      return;
    }

    byte[] data = new byte[numRead];
    System.arraycopy(buffer.array(), 0, data, 0, numRead);
    System.out.println("Received " + new String(data) + " from " + channel.getRemoteAddress());

    key.attach(data);
    key.interestOps(SelectionKey.OP_WRITE);
  }

  private void write(SelectionKey key) throws IOException {
    ByteBuffer outBuf = ByteBuffer.wrap((byte[]) key.attachment());
    SocketChannel channel = (SocketChannel) key.channel();

    channel.write(outBuf);
    outBuf.flip();

    System.out.println("Sent " + new String(outBuf.array()) + " to " + channel.getRemoteAddress());

    key.interestOps(SelectionKey.OP_READ);
  }

  public static void main(String[] args) throws IOException {
    new ReactorServer(9090).start();
  }
}
