package lang.annotation;

@MyAnnotation(value = 20, name = "Quanwei")
public class MyAnnoTest {

    public static void main(String[] args) {
        MyAnnotation anno = MyAnnoTest.class.getDeclaredAnnotation(MyAnnotation.class);
        System.out.println(anno.name());// Quanwei
        System.out.println(anno.value());// 20
    }
}
