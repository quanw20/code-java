package lang.reflection;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.MethodHandles.Lookup;

/**
 * Any2Func
 */
public class MethodHandleDemo {

  public static void main(String[] args) throws Throwable {
    // [1] 创建 lookup
    Lookup lookup = MethodHandles.lookup();
    // [2] 创建 MethodType
    MethodType mt = MethodType.methodType(String.class, String.class);
    // [3] 创建 MethodHandle
    Class klass = lookup.findClass("lang.reflection._Demo");
    System.out.println("klass: " + klass.getName());

    // default
    MethodHandle mh = lookup.findVirtual(klass, "say", mt);
    mh.invoke(new _Demo(), "QWER");

    // ctor
    MethodHandle ctor = lookup.findConstructor(klass, MethodType.methodType(void.class));
    var obj = ctor.invoke();
    System.out.println("ctor.invoke: " + obj);

    var nameF = klass.getDeclaredField("name");
    nameF.setAccessible(true);
    System.out.println("reflect: " + nameF.get(obj));

    // getter public
    MethodHandle getter = lookup.findGetter(klass, "age", int.class);
    System.out.println("get age:" + getter.invoke(obj));

    // getter private
    MethodHandle getter2 = lookup.unreflectGetter(nameF);
    System.out.println("get name:" + getter2.invoke(obj));

    // 调用静态方法
    MethodHandle mhStatic = lookup.findStatic(klass, "sayStatic", mt);
    System.out.println(mhStatic.invoke("ASDF"));



  }
}

class _Demo {
  private String name;
  public int age;

  public _Demo() {
    name = "def name";
  }

  public _Demo(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  private String sayPrivate(String name) {
    var r = "hola " + name;
    System.out.println(r);
    return r;
  }

  protected String sayProtected(String name) {
    var r = "hola " + name;
    System.out.println(r);
    return r;
  }

  String say(String name) {
    var r = "hola " + name;
    System.out.println(r);
    return r;
  }

  static String sayStatic(String name) {
    var r = "hola " + name;
    System.out.println(r);
    return r;
  }

  public String sayPublic(String name) {
    var r = "hola " + name;
    System.out.println(r);
    return r;
  }

  @Override
  public String toString() {
    return "_Demo name: " + name;
  }
}
