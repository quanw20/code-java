package lang.collections;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

/**
 * void reverse(List list)：对指定 List 集合元素进行逆向排序。
 * void shuffle(List list)：对 List 集合元素进行随机排序（shuffle 方法模拟了“洗牌”动作）。
 * void sort(List list)：根据元素的自然顺序对指定 List 集合的元素按升序进行排序。
 * void sort(List list, Comparator c)：根据指定 Comparator 产生的顺序对 List 集合元素进行排序。
 * void swap(List list, int i, int j)：将指定 List 集合中的 i 处元素和 j 处元素进行交换。
 * void rotate(List list, int distance)：当 distance 为正数时，将 list 集合的后 distance
 * 个元素“整体”移到前面；当 distance 为负数时，将 list 集合的前 distance 个元素“整体”移到后面。
 */
public class TestCollections {
    public static void main(String[] args) {
        List<String> ls = new ArrayList<>();
        ls.add("reverse");
        ls.add("shuffle");
        ls.add("sort");
        ls.add("swap");
        ls.add("rotate");
        System.out.println(ls);// [reverse, shuffle, sort, swap, rotate]

        // 反转
        Collections.reverse(ls);
        System.out.println(ls);// [rotate, swap, sort, shuffle, reverse]

        // 按升序进行排序
        Collections.sort(ls);
        System.out.println(ls);// [reverse, rotate, shuffle, sort, swap]

        // 根据指定 Comparator排序
        Collections.sort(ls, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                return -o1.compareTo(o2);
            }

        });
        System.out.println(ls);// [swap, sort, shuffle, rotate, reverse]

        // 按下标交换
        Collections.swap(ls, 0, ls.size() - 1);
        System.out.println(ls);// [reverse, sort, shuffle, rotate, swap]

        // 将 list 集合的后 distance 个元素整体移到前面
        Collections.rotate(ls, 2);
        System.out.println(ls);// [rotate, swap, reverse, sort, shuffle]

        // 将 list 集合的前 distance 个元素整体移到后面
        Collections.rotate(ls, -3);
        System.out.println(ls);// [sort, shuffle, rotate, swap, reverse]

        // 返回指定集合中指定元素的出现次数
        ls.add("sort");
        int frequency = Collections.frequency(ls, "sort");
        System.out.println(frequency);// 2

        // 将指定集合中的所有元素复制到另一个集合中,目标集合的长度至少和源集合的长度相同
        List<String> l2 = new LinkedList<>();// ArrayList -> LinkedList
        l2.add("reverse1");
        l2.add("shuffle1");
        l2.add("sort1");
        l2.add("swap1");
        l2.add("rotate1");
        Collections.copy(ls, l2);
        System.out.println(ls);// [reverse1, shuffle1, sort1, swap1, rotate1, sort]

        // LinkedList -> ArrayList
        // ! IndexOutOfBoundsException Source does not fit in dest
        Collections.copy(l2, ls);
        System.out.println(ls);
    }

    /**
     * 列表
     */
    @Test
    public void testList() {
        // <> 中的类型只能是引用类型
        List<Integer> al = new ArrayList<>();
        List<Integer> ll = new LinkedList<>();
    }

    /**
     * 队列
     */
    @Test
    public void testQueue() {
        Queue<Integer> ad = new ArrayDeque<>();
        Queue<Integer> ll = new LinkedList<>();
        Queue<Integer> pq = new PriorityQueue<>();

    }

    /**
     * 集合
     */
    @Test
    public void testSet() {
        Set<Object> hs = new HashSet<>();
        Set<Object> ts = new TreeSet<>();

    }

    /**
     * 映射
     */
    @Test
    public void testMap() {
        Map<Integer, String> hm = new HashMap<>();
        Map<Integer, String> tm = new TreeMap<>();
    }

    @Test
    public void testStream() {
        // 创建
        List<Integer> number = Arrays.asList(2, 3, 4, 5);

        // 使用map将number的元素平方
        List<Integer> square = number.stream().map(x -> x * x).collect(Collectors.toList());
        System.out.println(square);// [4, 9, 16, 25]

        // 创建
        List<String> names = Arrays.asList("Reflection", "Collection", "Stream");

        // 过滤
        List<String> result = names.stream().filter(s -> s.startsWith("S")).collect(Collectors.toList());
        System.out.println(result);// [Stream]

        // 排序
        List<String> show = names.stream().sorted().collect(Collectors.toList());
        System.out.println(show);// [Collection, Reflection, Stream]

        // 创建
        List<Integer> numbers = Arrays.asList(2, 3, 4, 5, 2);

        // 收集
        Set<Integer> squareSet = numbers.stream().map(x -> x * x).collect(Collectors.toSet());
        System.out.println(squareSet);// [16, 4, 9, 25]

        // forEach
        number.stream().map(x -> x * x).forEach(y -> System.out.print(y + " "));// 4 9 16 25

        // reduce
        int even = number.stream().filter(x -> x % 2 == 0).reduce(0, (ans, i) -> ans + i);// 0+2+4

        System.out.println(even);// 6
    }
}

class Pair<T, U> {
    private T first;
    private U second;

    public Pair(T first, U second) {
        this.setFirst(first);
        this.setSecond(second);
    }

    /**
     * @return the second
     */
    public U getSecond() {
        return second;
    }

    /**
     * @param second the second to set
     */
    public void setSecond(U second) {
        this.second = second;
    }

    /**
     * @return the first
     */
    public T getFirst() {
        return first;
    }

    /**
     * @param first the first to set
     */
    public void setFirst(T first) {
        this.first = first;
    }

    @Override
    public String toString() {
        return "( " + first + ", " + second + ")";
    }
}
