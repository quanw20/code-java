package lang.gui;

import java.awt.*;

import javax.swing.JFrame;

import org.junit.jupiter.api.Test;

public class Center {
    private static final int HEIGHT = 500;
    private static final int WIDTH = 500;
    private static final int SCREEN_HEIGHT;
    private static final int SCREEN_WIDTH;
    static {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension screenSize = tk.getScreenSize();
        SCREEN_HEIGHT = screenSize.height;
        SCREEN_WIDTH = screenSize.width;
    }

    @Test
    public void testCenter() {
        JFrame f = new JFrame("CENTER");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(WIDTH, HEIGHT);
        f.setLocation(SCREEN_WIDTH / 2 - WIDTH / 2, SCREEN_HEIGHT / 2 - HEIGHT / 2);

        f.setVisible(true);
    }
    public static void main(String[] args) {
        new Center().testCenter();
    }

    @Test
    public void testSwing() {

    }
}
