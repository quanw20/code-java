package datastructure.digraph;

import java.util.HashSet;
import java.util.Set;

public class Graph {
    private final int V;
    private int E;
    private Set<Integer>[] adj;

    @SuppressWarnings("unchecked")
    public Graph(int V) {
        this.V = V;
        this.E = 0;
        adj = (Set<Integer>[]) new HashSet[V];
        for (int i = 0; i < V; ++i)
            adj[i] = new HashSet<>();
    }

    public int V() {
        return V;
    }

    public int E() {
        return E;
    }

    public void addEdge(int v, int w) {
        adj[v].add(w);
        ++E;
    }

    public Iterable<Integer> adj(int v) {
        return adj[v];
    }

    public Graph reverse() {
        Graph r = new Graph(V);
        for (int v = 0; v < V; ++v)
            for (int w : adj(v))
                r.addEdge(w, v);
        return r;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int v = 0; v < V; ++v) {
            sb.append(v);
            for (int w : adj(v))
                sb.append(" -> " + w);
            sb.append("\n");
        }
        return sb.toString();
    }
}
