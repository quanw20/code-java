package datastructure.digraph;

/**
 * 拓扑排序 条件L:有向无环图（DGA）
 * 
 * DAG的拓扑顺序即为所有顶点的逆后续排列
 */
public class Topological {
    private Iterable<Integer> order;

    public Topological(Graph G) {
        Cycle cycle = new Cycle(G);
        if (!cycle.hasCycle()) {
            DFS dfs = new DFS(G);
            order = dfs.reversePost();
        }
    }

    public boolean isDAG() {
        return order != null;
    }

    public Iterable<Integer> order() {
        return order;
    }

}
