package datastructure.digraph;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class DFS {
    private boolean[] marked;
    private Queue<Integer> pre;// 所有顶点的前序排列
    private Queue<Integer> post;// 所有顶点的后序排列
    private Stack<Integer> reversePost;// 所有顶点的逆后序排列

    public DFS(Graph G) {
        pre = new ArrayDeque<>();
        post = new ArrayDeque<>();
        reversePost = new Stack<>();
        marked = new boolean[G.V()];
        for (int v = 0; v < G.V(); ++v) {
            if (!marked[v])
                dfs(G, v);
        }
    }

    private void dfs(Graph g, int v) {
        pre.add(v);
        marked[v] = true;
        for (int w : g.adj(v))
            if (!marked[w])
                dfs(g, w);
        post.add(v);
        reversePost.push(v);
    }

    public Iterable<Integer> pre() {
        return pre;
    }

    public Iterable<Integer> post() {
        return post;
    }

    public Iterable<Integer> reversePost() {
        return reversePost;
    }
}
