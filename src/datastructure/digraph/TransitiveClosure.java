package datastructure.digraph;

/**
 * 两点间可达性
 */
public class TransitiveClosure {
    private DirectedDFS all[];

    public TransitiveClosure(Graph G) {
        all = new DirectedDFS[G.V()];
        for (int v = 0; v < G.V(); ++v) {
            all[v] = new DirectedDFS(G, v);
        }
    }

    public boolean reachable(int w, int v) {
        return all[w].marked(v);
    }
}
