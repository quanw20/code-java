package datastructure.digraph;

/**
 * 强联通分量 Strong Connected Count
 * 有向图的强连通性(顶点间的等价关系)
 * 如果顶点{@code u - v }相互可达, 则称二者强联通
 * 
 * 下面使用Kosaraju算法
 */
public class SCC {
    private boolean[] marked;
    private int[] id;
    private int count;

    public SCC(Graph G) {
        marked = new boolean[G.V()];
        id = new int[G.V()];
        count = 0;
        DFS order = new DFS(G.reverse());
        for (int s : order.reversePost())
            if (!marked[s]) {
                dfs(G, s);
                ++count;
            }
    }

    private void dfs(Graph G, int v) {
        marked[v] = true;
        id[v] = count;
        for (int w : G.adj(v))
            if (!marked[w])
                dfs(G, w);
    }

    public boolean stronglyConnected(int v, int w) {
        return id[v] == id[w];
    }

    public int id(int v) {
        return id[v];
    }

    public int count() {
        return count;
    }

}
