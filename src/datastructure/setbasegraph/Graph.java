package datastructure.setbasegraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * 邻接集实现的无向图
 */
public class Graph<VertexType extends Comparable<VertexType>> {
    private int V;
    private int E;
    private Map<VertexType, Set<VertexType>> adj;

    /**
     * 初始化
     */
    public Graph() {
        this.V = 0;
        this.E = 0;
        adj = new HashMap<>();
    }

    /**
     * 
     * @return 节点数
     */
    public int V() {
        return V;
    }

    /**
     * 
     * @return 边数
     */
    public int E() {
        return E;
    }

    /**
     * 添加一条{@code v}与{@code w}的无向边
     * 
     * @param v
     * @param w
     */
    public void addEdge(VertexType v, VertexType w) {
        adj.get(v).add(w);
        adj.get(w).add(v);
        ++E;
    }

    /**
     * 
     * @param v
     * @return {@code v}的相邻顶点
     */
    public Iterable<VertexType> adj(VertexType v) {
        return adj.get(v);
    }

    public void addVertex(VertexType v) {
        adj.put(v, new HashSet<VertexType>());
        ++V;
    }

    public Iterable<VertexType> getVertexs() {
        return adj.keySet();
    }

    // public void removeVertex(V v) {

    // }
    /**
     * 删除边
     * 
     * @param v
     * @param w
     */
    public void removeEege(VertexType v, VertexType w) {
        adj.get(v).remove(w);
        adj.get(w).remove(v);
        --E;
    }

    /**
     * 判断是否有边{@code v-w}
     * 
     * @param v
     * @param w
     * @return
     */
    public boolean containsEege(VertexType v, VertexType w) {
        return adj.get(v).contains(w) && adj.get(w).contains(v);
    }

    /**
     * 判断是否有节点{@code v}
     * 
     * @param v
     * @return
     */
    public boolean containsVertex(VertexType v) {
        return adj.get(v) != null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Set<Entry<VertexType, Set<VertexType>>> entrySet = adj.entrySet();
        for (Entry<VertexType, Set<VertexType>> entry : entrySet) {
            sb.append(entry.getKey()).append(" -> ");
            sb.append(entry.getValue().toString()).append('\n');
        }
        return sb.toString();
    }

    /**
     * 计算度
     * 
     * @param v
     * @return 度
     */
    public int degree(VertexType v) {
        int degree = 0;
        Iterator<VertexType> it = adj(v).iterator();
        while (it.hasNext()) {
            it.next();
            ++degree;
        }
        return degree;
    }

    /**
     * 计算最大度
     * 
     * @return 最大度
     */
    public int maxDegree() {
        int max = 0, cnt = 0;
        for (Set<VertexType> s : adj.values()) {
            cnt = s.size();
            max = cnt > max ? cnt : max;
        }
        return max;
    }

    /**
     * 平均度
     * 
     * @return
     */
    public double avgDegree() {
        return 2.0 * E / V;
    }

    /**
     * 自环数
     * 
     * @return
     */
    public int numberOfSelfLoops() {
        int cnt = 0;
        Iterator<Entry<VertexType, Set<VertexType>>> it = adj.entrySet().iterator();
        while (it.hasNext()) {
            Entry<VertexType, Set<VertexType>> next = it.next();
            if (next.getValue().contains(next.getKey()))
                ++cnt;
        }
        return cnt;
    }

}
