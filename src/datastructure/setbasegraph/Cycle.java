package datastructure.setbasegraph;

import java.util.HashSet;
import java.util.Set;

/**
 * 检测图g中是否有环
 */
public class Cycle<VertexType extends Comparable<VertexType>> {
    private Set<VertexType> marked;
    private boolean hasCycle;
    private VertexType start, end;
    private Graph<VertexType> g;

    Cycle(Graph<VertexType> g) {
        this.g = g;
        marked = new HashSet<>();
        hasCycle = false;
        for (VertexType v : g.getVertexs()) {
            if (!marked.contains(v)) {
                dfs(g, v, v);
            }
        }
    }

    private void dfs(Graph<VertexType> g, VertexType v, VertexType u) {
        marked.add(v);
        for (VertexType w : g.adj(v)) {
            if (!marked.contains(w)) {
                dfs(g, w, v);
            } else if (!w.equals(u)) {
                // 至少三个元素才能成环
                // 已经经过w且不是前一个,则有环
                hasCycle = true;
                start = w;
                end = u;
            }
        }
    }

    public boolean hasCycle() {
        return hasCycle;
    }

    public Iterable<VertexType> getCyclePath() {
        Paths<VertexType> paths = new Paths<>(this.g, end);
        return paths.pathTo(start);
    }

}
