package datastructure.setbasegraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Paths<VertexType extends Comparable<VertexType>> {
    private HashSet<VertexType> marked;
    private HashMap<VertexType, VertexType> edgeTo;
    private Queue<VertexType> queue;
    private VertexType start;

    /**
     * 在g中找出所有起点为v的路径
     * 
     * @param g
     * @param v
     */
    Paths(Graph<VertexType> g, VertexType v) {
        marked = new HashSet<>();
        edgeTo = new HashMap<>();
        queue = new LinkedList<>();
        start = v;
        dfs(g, v);// Cycle用到
    }

    /**
     * 使用深度优先
     * 
     * @param g
     * @param v
     */
    private void dfs(Graph<VertexType> g, VertexType v) {
        marked.add(v);
        for (VertexType w : g.adj(v)) {
            if (!marked.contains(w)) {
                edgeTo.put(w, v);
                dfs(g, w);
            }
        }
    }

    /**
     * 使用广度优先搜索
     * 
     * @param g
     * @param v
     */
    @SuppressWarnings("unused")
    private void bfs(Graph<VertexType> g, VertexType v) {
        queue.add(v);
        edgeTo.put(v, null);
        while (!queue.isEmpty()) {
            VertexType p = queue.poll();
            for (VertexType w : g.adj(p)) {
                if (!edgeTo.containsKey(w)) {
                    queue.add(w);
                    edgeTo.put(w, p);
                }
            }
        }
    }

    public boolean hasPathTo(VertexType v) {
        return edgeTo.containsKey(v);
    }

    /**
     * 路径{@code s-w}
     * 
     * @param w
     * @return
     */
    public Iterable<VertexType> pathTo(VertexType w) {
        if (!hasPathTo(w))
            return null;
        Stack<VertexType> path = new Stack<>();
        for (VertexType x = w; !x.equals(start); x = edgeTo.get(x)) {
            path.push(x);
        }
        path.push(start);
        return path;
    }

}
