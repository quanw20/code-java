package datastructure.setbasegraph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * ConnectedComponent
 * 联通分量
 */
public class ConnectedComponent<VertexType extends Comparable<VertexType>> {
    private Set<VertexType> marked;
    private Map<VertexType, Integer> id;
    private int count;

    public ConnectedComponent(Graph<VertexType> G) {
        marked = new HashSet<>();
        id = new HashMap<>();
        for (VertexType v : G.getVertexs()) {
            if (!marked.contains(v)) {
                dfs(G, v);
                ++count;
            }
        }
    }

    private void dfs(Graph<VertexType> g, VertexType v) {
        marked.add(v);
        id.put(v, count);
        for (VertexType w : g.adj(v))
            if (!marked.contains(w))
                dfs(g, w);

    }

    public boolean connected(VertexType w, VertexType v) {
        return id.get(w).equals(id.get(v));
    }

    public int id(VertexType v) {
        return id.get(v);
    }

    public int count() {
        return count;
    }
}
