package datastructure.mst;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Prim {
    /**
     * List[from]int[]{ to, weight} 表示一条边
     * 
     * @param graph 邻接表
     * @return 最小值或者-1
     */
    public int prim(List<int[]>[] graph) {
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> a[1] - b[1]);// 按照边的权重从小到大排序
        int n = graph.length;// 图中有 n 个节点
        boolean[] inMST = new boolean[n];
        inMST[0] = true;// 随便从一个点开始切分都可以，我们不妨从节点 0 开始
        for (int[] edge : graph[0]) {// 遍历 s 的邻边
            int to = edge[0];
            if (inMST[to]) // 相邻接点 to 已经在最小生成树中，跳过 否则这条边会产生环
                continue;
            pq.offer(edge);// 加入横切边队列
        }
        int weightSum = 0;
        // List<int[]> ret = new ArrayList<>();
        while (!pq.isEmpty()) {// 不断进行切分，向最小生成树中添加边
            int[] edge = pq.poll();
            int to = edge[0];
            if (inMST[to])
                continue;
            weightSum += edge[1];// 将边 edge 加入最小生成树
            // ret.add(edge);
            inMST[to] = true;
            for (int[] e : graph[to]) {// 节点 t 加入后，进行新一轮切分，会产生更多横切边
                int t = e[0];
                if (inMST[t])
                    continue;
                pq.offer(e);
            }
        }
        // ret.forEach(x -> System.out.println(x[0] + " " + x[1]));
        for (int i = 0; i < inMST.length; i++)
            if (!inMST[i])
                return -1;
        return weightSum;
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {
        List<int[]>[] g = new List[] {
                List.of(new int[] { 1, 1 }),
                List.of(new int[] { 2, 12 }),
                List.of(new int[] { 3, 23 }),
                List.of(new int[] { 4, 34 }, new int[] { 5, 35 }),
                List.of(new int[] { 5, 45 }),
                List.of(new int[] { 0, 50 })
        };
        int w = new Prim().prim(g);
        System.out.println(w);
    }
}
