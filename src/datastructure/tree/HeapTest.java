package datastructure.tree;

import org.junit.jupiter.api.Test;

import static algorithms.Util.*;
import static datastructure.tree.Heap.*;

import java.util.Arrays;
import java.util.PriorityQueue;

public class HeapTest {
    @Test
    public void testTriversal() {
        int[] a = range(10);
        System.out.println("前序遍历");
        preOrder(a, 1);
        System.out.println("\n中序遍历");
        inOrder(a, 1);
        System.out.println("\n后序遍历");
        postOrder(a, 1);
    }

    @Test
    public void test_makeMaxHeap() {
        int[] a = new int[11];
        for (int i = 1; i <= 10; i++) {
            a[i] = i - 1;
        }
        makeMaxHeap(a);
        seqOrder(a);
    }

    @Test
    public void test_minHeapify() {
        int[] a = new int[11];
        for (int i = 1; i <= 10; i++) {
            a[i] = 10 - i;
        }
        minHeapify(a);
        seqOrder(a);
    }

    @Test
    public void test_heapSort() {
        int[] a = new int[31];
        for (int i = 1; i <= 30; ++i)
            a[i] = (int) (Math.random() * 100);
        int[] b = a.clone();
        heapSort(a);
        seqOrder(a);
        System.out.println();

        heapSortLoop(b);
        seqOrder(b);
    }

}
