package datastructure.tree;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue; 

class HNode {
    int weight;
    HNode left, right, parent;

    HNode(int w) {
        weight = w;
    }
}

public class HuffmanTree {
    public static final int N = 10001;
    int[] parent = new int[N];

    void init() {
        for (int i = 1; i < N; ++i) {
            parent[i] = i;
        }
    }

    int[] find(int u) {
        int layCnt = 0;
        while (parent[u] != u) {
            u = parent[u];
            ++layCnt;
        }
        return new int[] { u, layCnt };
    }

    void union(int u, int v) {
        int pu = find(u)[0], pv = find(v)[0];
        if (pu != pv)
            parent[pu] = pv;
    }

    int huffmanTree(int[] a) {
        int weight = 0;
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (int i : a)
            pq.add(i);
        while (pq.size() != 1) {
            int x = pq.poll(), y = pq.poll(), w = x + y;
            weight += w;
            pq.add(w);
        }
        return weight;
    }

    int huffmanWeight(int[] a) {
        PriorityQueue<HNode> pq = new PriorityQueue<>((x, y) -> x.weight - y.weight);
        for (int i : a)
            pq.add(new HNode(i));
        while (pq.size() != 1) {
            HNode x = pq.poll(), y = pq.poll();
            HNode node = new HNode(x.weight + y.weight);
            node.left = x;
            node.right = y;
            x.parent = node;
            y.parent = node;
            pq.add(node);
        }
        HNode root = pq.poll();
        Queue<HNode> q = new LinkedList<>();
        q.add(root);
        int layCnt = 0;
        int weight = 0;
        while (!q.isEmpty()) {
            int s = q.size();
            while (s-- != 0) {
                HNode node = q.poll();
                if (node.left != null || node.right != null) {
                    if (node.left != null)
                        q.add(node.left);
                    if (node.right != null)
                        q.add(node.right);
                } else {
                    weight += layCnt * node.weight;
                }
            }
            ++layCnt;
        }
        return weight;
    }

    public static void main(String[] args) {
        int[] a = { 1, 2, 9, 4, 3, 6 };
        var t = new HuffmanTree();
        int huffmanTree = t.huffmanTree(a);
        System.out.println(huffmanTree);
        int huffmanWeight = t.huffmanWeight(a);
        System.out.println(huffmanWeight);
    }
}
