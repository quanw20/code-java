package datastructure.tree;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Serialize {
    public static final String SEP = ",";
    public static final String NULL = "#";

    /**
     * 使用前序遍历序列化反序列化
     */
    class Pre {
        String serialize(TreeNode root) {
            StringBuilder sb = new StringBuilder();
            serialize(root, sb);
            return sb.toString();
        }

        private void serialize(TreeNode root, StringBuilder sb) {
            if (root == null) {
                sb.append(NULL).append(SEP);
                return;
            }
            sb.append(root.val).append(SEP);
            serialize(root.left, sb);
            serialize(root.right, sb);
        }

        TreeNode deserialize(String str) {
            String[] nodes = str.split(SEP);
            return deserialize(nodes);
        }

        int i = 0;

        private TreeNode deserialize(String[] nodes) {
            if (i == nodes.length)
                return null;
            String first = nodes[i++];
            if (first.equals(NULL))
                return null;
            TreeNode root = new TreeNode(Integer.parseInt(first));
            root.left = deserialize(nodes);
            root.right = deserialize(nodes);
            return root;
        }
    }

    /**
     * 使用后续遍历序列化
     */
    class Post {
        String serialize(TreeNode root) {
            StringBuilder sb = new StringBuilder();
            serialize(root, sb);
            return sb.toString();
        }

        private void serialize(TreeNode root, StringBuilder sb) {
            if (root == null) {
                sb.append(NULL).append(SEP);
                return;
            }
            serialize(root.left, sb);
            serialize(root.right, sb);
            sb.append(root.val).append(SEP);
        }

        int i;

        TreeNode deserialize(String str) {
            String[] nodes = str.split(SEP);
            i = nodes.length - 1;
            return deserialize(nodes);
        }

        /**
         * 后续序列化结果:
         * [左子树][右子树][根]
         * |<-------------<-|
         * 
         * @param nodes
         */
        private TreeNode deserialize(String[] nodes) {
            if (i < 0)
                return null;
            String last = nodes[i--];
            if (last.equals(NULL))
                return null;
            TreeNode root = new TreeNode(Integer.parseInt(last));
            root.right = deserialize(nodes);
            root.left = deserialize(nodes);
            return root;
        }
    }

    class Sequence {
        String serialize(TreeNode root) {
            if (root == null)
                return NULL;
            StringBuilder sb = new StringBuilder();
            Queue<TreeNode> q = new LinkedList<>();
            q.add(root);
            while (!q.isEmpty()) {
                TreeNode cur = q.poll();
                if (cur != null) {
                    sb.append(cur.val).append(SEP);
                    q.add(cur.left);
                    q.add(cur.right);
                } else {
                    sb.append(NULL).append(SEP);
                }

            }
            return sb.toString();
        }

        TreeNode deserialize(String str) {
            if (str.equals(NULL))
                return null;
            String[] nodes = str.split(SEP);
            TreeNode root = new TreeNode(Integer.parseInt(nodes[0]));
            Queue<TreeNode> q = new LinkedList<>();
            q.add(root);
            for (int i = 1; i < nodes.length;) {
                TreeNode p = q.poll();
                String cur = nodes[i++];
                if (!cur.equals(NULL)) {
                    p.left = new TreeNode(Integer.parseInt(cur));
                    q.add(p.left);
                }
                cur = nodes[i++];
                if (!cur.equals(NULL)) {
                    p.right = new TreeNode(Integer.parseInt(cur));
                    q.add(p.right);
                }
            }
            return root;
        }
    }

    public static void main(String[] args) {
        final String data = "1,2,#,4,#,#,3,#,#,";
        System.out.println(Arrays.toString(data.split(SEP)));
        Serialize ser = new Serialize();
        TreeNode root;
        String str;

        // 使用前序遍历序列化反序列化
        System.out.println("使用前序遍历序列化反序列化: ");
        Pre pre = ser.new Pre();
        root = pre.deserialize(data);
        Traversal.preorder(root);
        System.out.println();
        str = pre.serialize(root);
        System.out.println(str);

        // 后序
        System.out.println("后序: ");
        Post post = ser.new Post();
        str = post.serialize(root);
        System.out.println(str);
        root = post.deserialize(str);
        Traversal.postorder(root);
        System.out.println();

        // 层序
        System.out.println("层序");
        Sequence seq = ser.new Sequence();
        str = seq.serialize(root);
        System.out.println(str);
        root = seq.deserialize(str);
        Traversal.sequenceTraversal(root);
    }

}
