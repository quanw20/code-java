package datastructure.tree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.function.Consumer;

/**
 * <pre>
 * 1. 二叉树遍历:
 * 
 *      dfs-递归
 *      dfs-非递归
 *      bfs
 * </pre>
 */
public class Traversal {
    /**
     * 中序遍历
     * 
     * @param root 根
     */
    public static void inorder(TreeNode root) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }

    /**
     * 前序遍历
     * 
     * eg. 快速排序
     * 
     * @param root 根
     */
    public static void preorder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + " ");
        preorder(root.left);
        preorder(root.right);
    }

    /**
     * 后序遍历
     * 
     * 归并排序
     * 
     * @param root 根
     */
    public static void postorder(TreeNode root) {
        if (root == null) {
            return;
        }
        postorder(root.left);
        postorder(root.right);
        System.out.print(root.val + " ");
    }

    public static void inorderR(TreeNode root, Consumer<Integer> consumer) {
        if (root == null) {
            return;
        }
        inorder(root.left);
        consumer.accept(root.val);
        inorder(root.right);
    }

    public static void preorderR(TreeNode root, Consumer<Integer> consumer) {
        if (root == null) {
            return;
        }
        consumer.accept(root.val);
        preorder(root.left);
        preorder(root.right);
    }

    public static void postorderR(TreeNode root, Consumer<Integer> consumer) {
        if (root == null) {
            return;
        }
        postorder(root.left);
        postorder(root.right);
        consumer.accept(root.val);
    }

    /**
     * 层序遍历
     * 
     * @param root
     */
    public static void sequenceTraversal(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            TreeNode node = q.poll();
            System.out.print(node.val + " ");
            if (node.left != null)
                q.add(node.left);
            if (node.right != null)
                q.add(node.right);
        }
    }

    /**
     * 非递归中序遍历
     * lift循环入栈, 弹出处理,
     * 如果right不为空, p指向right继续,
     * 否则再弹一个继续
     * 
     * @param root
     * @param consumer
     */
    public static void inorder(TreeNode root, Consumer<Integer> consumer) {
        if (root == null)
            return;
        Stack<TreeNode> s = new Stack<>();
        TreeNode p = root;
        // cur不为空, s不为空可以继续处理
        while (p != null || !s.empty()) {
            // cur不为空的情况下
            while (p != null) {
                s.push(p);
                p = p.left;
            }
            // 到达子树最左端
            TreeNode node = s.pop();
            consumer.accept(node.val);
            if (node.right != null) {
                p = node.right;
            } // 如果node.right不为空, p指向right,否则再弹一个继续
        }

    }

    /**
     * 前序遍历
     * 入栈时处理
     * 
     * @param root
     * @param consumer
     */
    public static void preorder(TreeNode root, Consumer<Integer> consumer) {
        if (root == null)
            return;

        Stack<TreeNode> s = new Stack<>();
        TreeNode cur = root;
        // cur不为空, s不为空可以继续处理
        while (cur != null || !s.empty()) {
            // cur不为空的情况下
            while (cur != null) {
                consumer.accept(cur.val);
                s.push(cur);
                cur = cur.left;
            }
            TreeNode node = s.pop();
            if (node.right != null) {
                cur = node.right;
            }
        }
    }

    /**
     * 前序遍历 换一种写法
     * 
     * @param root
     * @param consumer
     */
    public static void preorder1(TreeNode root, Consumer<Integer> consumer) {
        if (root == null)
            return;
        Stack<TreeNode> s = new Stack<>();
        TreeNode cur = root;
        // cur不为空, s不为空可以继续处理
        while (cur != null || !s.empty()) {
            if (cur != null) {// cur不为空的情况下
                consumer.accept(cur.val);
                s.push(cur);
                cur = cur.left;
            } else {// s不为空
                TreeNode node = s.pop();
                cur = node.right;
            }
        }
    }

    /**
     * 后续遍历的非递归写法
     * 单栈
     * 
     * @param root
     * @param consumer
     */
    public static void postorder(TreeNode root, Consumer<Integer> consumer) {
        if (root == null)
            return;
        Stack<TreeNode> s = new Stack<>();
        TreeNode cur = root, pre = null;
        while (cur != null || !s.empty()) {
            if (cur != null) {
                s.push(cur);
                cur = cur.left;
            } else {// s非空
                cur = s.peek();// 访问s最顶端
                if (cur.right != null && cur.right != pre) {// 没有被访问: 使得右子树是链表的情况能正常打印
                    cur = cur.right;// 右子树
                    s.push(cur);
                    cur = cur.left;// 右子树的左子树
                } else {// cur.right != null不成立
                    consumer.accept(s.pop().val);
                    pre = cur;
                    cur = null;
                }
            }
        }
    }

    /**
     * 非递归后序遍历
     * 
     * 双栈写法
     * 先右子树
     * 再左边一个
     * 循环
     * 
     * @param root
     * @param consumer
     */
    public static void postorder1(TreeNode root, Consumer<Integer> consumer) {
        if (root == null)
            return;
        Stack<TreeNode> s = new Stack<>();
        Stack<Integer> res = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !s.empty()) {
            while (cur != null) {
                res.push(cur.val);
                s.push(cur);
                cur = cur.right;//
            }
            if (!s.isEmpty()) {
                cur = s.pop();
                cur = cur.left;
            }
        }
        while (!res.isEmpty()) {
            consumer.accept(res.pop());
        }
    }

}
