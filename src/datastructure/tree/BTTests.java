package datastructure.tree;

import static datastructure.tree.BinaryTree.*;
import static datastructure.tree.Traversal.*;

import org.junit.jupiter.api.*;

import datastructure.tree.AVLTree;
import datastructure.tree.TreeNode;

public class BTTests {
    static AVLTree bst;
    private static long startTime = 0L;

    @BeforeAll
    public static void before() {
        startTime = System.nanoTime();
        System.out.println("开始");
    }

    @Test
    public void testBST() {
        bst.inOrder();
        bst.remove(60);
        System.out.println();
        bst.inOrder();
    }

    @Test
    public void testInPost() {
        post = new int[] { 2, 3, 1, 5, 7, 6, 4 };
        in = new int[] { 1, 2, 3, 4, 5, 6, 7 };
        pre = new int[] { 4, 1, 3, 2, 6, 5, 7 };
        int n = post.length - 1;
        TreeNode root = createFromInAndPost(0, n, 0, n);
        TreeNode root1 = createFromInAndPre(0, n, 0, n);
        sequenceTraversal(root);
        System.out.println();
        sequenceTraversal(root1);
    }

    @Test
    public void testMinBST() {
        int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        TreeNode root = bstWithMinHight(a);
        inorder(root);
        System.out.println();
        preorder(root);
        System.out.println();
        sequenceTraversal(root);
        System.out.println();

    }

    @Test
    public void testBalance() {
        int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        TreeNode root = bstWithMinHight(a);
        System.out.println(isBalanced(root));
        // System.out.println(isBalanced2(root));
    }

    @Test
    public void testLayar() {
        int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        TreeNode root = bstWithMinHight(a);
        printLayar2(root, 3);
        System.out.println();
        System.out.println(isBST(root));
    }

    int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    TreeNode root = bstWithMinHight(a);

    @Test
    public void testTrivals() {
        preorder(root);
        System.out.println();
        preorder1(root, x -> System.out.print(x + " "));
        System.out.println();

        inorder(root, x -> System.out.print(x + " "));
        System.out.println();

        postorder(root);
        System.out.println();
        postorder(root, x -> System.out.print(x + " "));
        System.out.println();
        postorder1(root, x -> System.out.print(x + " "));
        System.out.println();
    }

    @Test
    public void testSuPred() {
        System.out.println("successor");
        int s1 = successor(root, 1);
        System.out.println(s1);
        int s2 = successor(root, 5);
        System.out.println(s2);
        int s3 = successor(root, 9);
        System.out.println(s3);
        System.out.println("predecessor:");

        int p1 = predecessor(root, 1);
        System.out.println(p1);
        int p2 = predecessor(root, 9);
        System.out.println(p2);
        int p3 = predecessor(root, 10);
        System.out.println(p3);
        int p4 = predecessor(root, 5);
        System.out.println(p4);
    }

    @Test
    public void testLCA() {
        TreeNode lca = LCA(root, new TreeNode(3), new TreeNode(1));
        System.out.println(lca.val);
        TreeNode lca1 = LCAs(root, new TreeNode(4), new TreeNode(3));
        System.out.println(lca1.val);
    }

    @AfterAll
    public static void AfterAll() {
        long endTime = System.nanoTime() - startTime;
        System.out.println("\n结束");
        System.out.println("用时: " + endTime + " ns");
    }

}
