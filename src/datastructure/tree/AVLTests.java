package datastructure.tree;

import java.util.Random;

import org.junit.jupiter.api.*;

public class AVLTests {

    static AVLTree bst;

    @BeforeAll
    public static void before() {
        Random random = new Random(0L);
        bst = new AVLTree();
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(random.nextInt(100));
        bst.insert(20);
        System.out.println("开始");
    }

    @Test
    public void testBST() {
        bst.inOrder();
        bst.remove(60);
        System.out.println();
        bst.inOrder();
    }




    @AfterAll
    public static void AfterAll() {
        System.out.println("\n结束");
    }

}


