package datastructure.tree;

/**
 * AVL Tree 自平衡二叉查找树
 */
public class AVLTree {
    /* 根节点 */
    private TreeNode root;

    public AVLTree() {
        root = null;
    }

    public AVLTree(int val) {
        root = new TreeNode(val);
    }

    private int hight(TreeNode node) {
        return node == null ? 0 : node.hight;
    }

    private int getBalance(TreeNode node) {
        return hight(node.left) - hight(node.right);
    }

    private TreeNode leftRotate(TreeNode x) {
        TreeNode y = x.right;
        TreeNode T2 = y.left;
        x.right = T2;
        y.left = x;
        x.hight = Math.max(hight(x.left), hight(x.right)) + 1;
        y.hight = Math.max(hight(y.left), hight(y.right)) + 1;
        return y;
    }

    private TreeNode rightRotate(TreeNode y) {
        TreeNode x = y.left;
        TreeNode T2 = x.right;
        y.left = T2;
        x.right = y;
        y.hight = Math.max(hight(y.left), hight(y.right)) + 1;
        x.hight = Math.max(hight(x.left), hight(x.right)) + 1;
        return x;
    }

    private TreeNode adjust(TreeNode node) {
        int balance = getBalance(node);
        if (balance > 1) {
            if (getBalance(node.left) < 0)
                node.left = leftRotate(node.left);// LR
            rightRotate(node);// LL
        } else if (balance < -1) {
            if (getBalance(node.right) > 0)
                node.right = rightRotate(node.right);// RL
            leftRotate(node);// RR
        }
        return node;
    }

    /**
     * 
     * @param v
     * @return 是否插入
     */
    public void insert(int v) {
        insert(root, v);
    }

    private TreeNode insert(TreeNode node, int v) {
        // 1. 递归插入
        if (node == null)
            return new TreeNode(v);
        else if (node.val < v)
            node.right = insert(node.right, v);
        else if (node.val > v)
            node.left = insert(node.left, v);
        else
            return node;
        // 2. 更新高度
        node.hight = hight(node.left) + hight(node.right) + 1;
        // 3. 调整
        adjust(node);
        return node;
    }

    public void inOrder() {
        inOrder(root);
    }

    private void inOrder(TreeNode node) {
        if (node == null)
            return;
        inOrder(node.left);
        System.out.print(node.val + " ");
        inOrder(node.right);
    }

    public TreeNode find(int v) {
        return null;
    }

    public int max() {
        return maxNode(root).val;
    }

    public int min() {
        return minNode(root).val;
    }

    private TreeNode minNode(TreeNode p) {
        if (p == null)
            return p;
        while (p.left != null)
            p = p.left;
        return p;
    }

    private TreeNode maxNode(TreeNode p) {
        if (p == null)
            return p;
        while (p.right != null)
            p = p.right;
        return p;
    }

    private TreeNode remove(TreeNode node, int v) {
        if (node == null)
            return null;
        if (v < node.val)
            node.left = remove(node.left, v);
        else if (v > node.val)
            node.right = remove(node.right, v);
        else {
            if (node.left == null) {
                return node.right;
            } else if (node.right == null) {
                return node.left;
            }
            TreeNode succ = minNode(node.right);
            node.val = succ.val;
            node.right = remove(node.right, succ.val);
        }
        node.hight = Math.max(hight(node.left), hight(node.right)) + 1;
        return node;
    }
    public TreeNode remove(int v){
        return remove(root, v);
    }
}