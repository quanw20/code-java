package datastructure.tree;

/**
 * 线段树 SegmentTree
 * 
 * 线段树的每一个节点都储存着一段区间[L...R]的信息, 其中叶子节点 L == R
 * 
 * 思想:
 * 将一段大区间平均地划分成2个小区间,
 * 每一个小区间都再平均分成2个更小区间...以此类推, 直到每一个区间的L==R
 * 通过对这些区间进行修改、查询，来实现对大区间的修改、查询
 * 
 * 支持操作:
 * 1. 支持区间求和
 * 2. 区间最大值
 * 4. 单点查寻
 * 5. 区间修改
 * 6. 单点修改
 * 
 * 性质:
 * 单次插入修改的次数的最大值为层数: ⌊log(n-1)⌋+2
 * 定理: n>=3时, 一个[1,n]的线段树可以将[1,n]的任意子区间[L,R]分解为不超过 2⌊log(n-1)⌋ 个子区间
 * 这样，在查询[L,R]的统计值的时候，只需要访问不超过个节点，就可以获得[L,R]的统计信息，实现了O(log2(n))的区间查询。
 * 
 * 线段树需要的数组元素个数:
 * $ 2^⌈log(n)⌉+1 $
 */
public class SegmentTree {


    int[] a;
    int left, right;

    public SegmentTree(int left, int right) {
        this.left = left;
        this.right = right;
        a = new int[right - left << 2];// 4x
    }

    public void insert(int num) {
        insert(0, left, right, num);
    }

    public int search(int mun) {
        return search(0, left, right, mun);
    }

    public void update(int num, int addVal) {
        update(0, left, right, num, addVal);
    }

    @Override
    public String toString() {
        return toStringDfs(0, left, right);
    }

    /**
     * 插入
     * 
     * @param pos   正在访问的下标
     * @param left  当前区间的左端点(包括)
     * @param right 当前区间的右端点(包括)
     * @param num   待插入数据
     */
    private void insert(int pos, int left, int right, int num) {
        ++a[pos];
        if (num == left && num == right)// 当前区间是单位区间且正好对应num
            return;
        int mid = left + (right - left >> 1);
        int leftChild = pos * 2 + 1;
        int rightChild = pos * 2 + 2;
        if (num <= mid)
            insert(leftChild, left, mid, num);
        else
            insert(rightChild, mid + 1, right, num);
    }

    private int search(int pos, int left, int right, int num) {
        if (num == left && num == right)
            return a[pos];
        int mid = left + (right - left >> 1);
        int leftChild = pos * 2 + 1;
        int rightChild = pos * 2 + 2;
        if (num <= mid)
            return search(leftChild, left, mid, num);
        else
            return search(rightChild, mid + 1, right, num);
    }

    private void update(int pos, int left, int right, int num, int addVal) {
        if (num == left && num == right) {
            a[pos] += addVal;
            return;
        }
        int mid = left + (right - left >> 1);
        int leftChild = pos * 2 + 1;
        int rightChild = pos * 2 + 2;
        if (num <= mid)
            update(leftChild, left, mid, num, addVal);
        else
            update(rightChild, mid + 1, right, num, addVal);
        a[pos] += addVal;
    }

    private String toStringDfs(int pos, int left, int right) {
        String s = String.format("[%d, %d] a[%2d] = %d\n", left, right, pos, a[pos]);
        if (left == right)
            return s;
        int mid = (left + right) / 2;
        return toStringDfs(pos * 2 + 1, left, mid) + s +
                toStringDfs(pos * 2 + 2, mid + 1, right);
    }

    public static void main(String[] args) {
        SegmentTree st = new SegmentTree(0, 5);
        st.insert(3);
        st.insert(3);
        st.insert(5);
        st.insert(2);
        for (int i = 0; i <= 5; ++i)
            System.out.println("search " + i + " : " + st.search(i));
        st.update(5, 103);
        System.out.print(st);
    }
}