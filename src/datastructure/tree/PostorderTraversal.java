package datastructure.tree;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class PostorderTraversal {
    HashMap<String, Integer> map = new HashMap<>();
    List<TreeNode> res = new LinkedList<>();// 重复子树的根节点

    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        postorder(root);
        return res;
    }

    String postorder(TreeNode root) {
        if (root == null) {
            return " ";
        }
        String left = postorder(root.left);
        String right = postorder(root.right);
        final String ret = left + "," + right + "," + root.val;
        Integer k = map.getOrDefault(ret, 0);
        if (k == 1) {
            res.add(root);
        }
        map.put(ret, k + 1);

        return ret;
    }
}
