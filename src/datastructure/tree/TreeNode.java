package datastructure.tree;

public class TreeNode implements Cloneable {
    public int val, hight;
    public TreeNode left, right;

    public TreeNode() {
        this.val = 0;
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(TreeNode node) {
        this.val = node.val;
        this.left = node.left;
        this.right = node.right;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TreeNode n)
            if (n.val == val)
                return true;
        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public TreeNode clone() throws CloneNotSupportedException {
        return new TreeNode(this);
    }
}
