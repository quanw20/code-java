package datastructure.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph {
  public Map<Integer, Node> nodes;
  public Set<Edge> edges;

  public Graph() {
    nodes = new HashMap<>();
    edges = new HashSet<>();
  }

  @Override
  public String toString() {
    return nodes.toString() + "\n" + edges.toString();
  }

  /**
   * @param graph int[n][n] 邻接矩阵
   * @return
   */
  public static Graph fromMatrix(int[][] graph) {
    Graph g = new Graph();
    int V = graph.length;
    for (int i = 0; i < V; ++i) {
      for (int j = 0; j < V; j++) {
        int weight = graph[i][j];
        if (weight == 0)
          continue;
        Edge edge = new Edge(i, j, weight);
        // 存在返回，不存在添加值返回
        Node fromNode = g.nodes.computeIfAbsent(i, Node::new);
        Node toNode = g.nodes.computeIfAbsent(j, Node::new);
        fromNode.out++;
        fromNode.edges.add(edge);
        fromNode.nexts.add(toNode);
        toNode.in++;
        g.edges.add(edge);
      }
    }
    return g;
  }

  /**
   * 
   * @param edges int[]{from,to,weight}
   * @return
   */
  public static Graph fromEdges(int[][] edges) {
    Graph g = new Graph();
    for (int i = 0; i < edges.length; i++) {
      int from = edges[i][0];
      int to = edges[i][1];
      int weight = edges[i][2];
      Edge edge = new Edge(from, to, weight);
      Node fromNode = g.nodes.computeIfAbsent(from, Node::new);
      Node toNode = g.nodes.computeIfAbsent(to, Node::new);
      fromNode.edges.add(edge);
      fromNode.nexts.add(toNode);
      fromNode.out++;
      toNode.in++;
      g.edges.add(edge);
    }
    return g;
  }

  /**
   * 
   * @param list list[from]={{to,weight},{},...}
   * @return
   */
  public static Graph fromList(List<int[]>[] list) {
    Graph g = new Graph();
    for (int from = 0; from < list.length; from++) {
      for (int[] ed : list[from]) {
        int to = ed[0];
        int weight = ed[1];
        Edge edge = new Edge(from, to, weight);
        Node fromNode = g.nodes.computeIfAbsent(from, Node::new);
        Node toNode = g.nodes.computeIfAbsent(to, Node::new);
        fromNode.edges.add(edge);
        fromNode.nexts.add(toNode);
        fromNode.out++;
        toNode.in++;
        g.edges.add(edge);
      }
    }
    return g;
  }

  public static void main(String[] args) {
    int[][] a = {
        { 0, 0, 3 },
        { 4, 0, 6 },
        { 7, 8, 0 } };
    Graph fromMatrix = Graph.fromMatrix(a);
    System.out.println(fromMatrix);
    System.out.println();

    int[][] b = {
        { 1, 2, 3 },
        { 4, 5, 6 },
        { 7, 1, 9 } };
    Graph fromEdges = Graph.fromEdges(b);
    System.out.println(fromEdges);
    System.out.println();

    @SuppressWarnings("unchecked")
    List<int[]>[] list = new List[] {
        List.of(
            new int[] { 1, 200 },
            new int[] { 2, 300 }),
        List.of(
            new int[] { 2, 2 },
            new int[] { 3, 3 })
    };
    Graph fromList = Graph.fromList(list);
    System.out.println(fromList);
  }

}
