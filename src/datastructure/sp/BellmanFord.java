package datastructure.sp;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 可以含有负权的
 * 单源最短路
 */
public class BellmanFord {
    // List[u]{int[]{v,w}}
    static ArrayList<int[]>[] vertexs;
    static int[] dist;

    /**
     * 
     * @param n 节点数
     * @param s 起点
     * @return 是否有负环
     */
    public static boolean bellmanford(int n, int s) {
        Arrays.fill(dist, 63);
        dist[s] = 0;
        boolean flag = false;
        for (int i = 0; i < n; i++) {
            flag = false;
            for (int u = 0; u < n; u++) {
                for (int[] edge : vertexs[u]) {
                    int v = edge[0], w = edge[1];
                    if (dist[v] > dist[u] + w) {
                        dist[v] = dist[u] + w;
                        flag = true;
                    }
                }
            }
            if (!flag) // 没有可以松弛的边时就停止算法
                break;
        }
        return flag;// 第 n 轮循环仍然可以松弛时说明 s 点可以抵达一个负环
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int n = 5;
        dist = new int[n];
        vertexs = (ArrayList<int[]>[]) new ArrayList[n];
        for (int i = 0; i < n; i++)
            vertexs[i] = new ArrayList<>();
        vertexs[0].add(new int[] { 1, 1 });
        vertexs[1].add(new int[] { 3, 2 });
        vertexs[2].add(new int[] { 0, 3 });
        vertexs[3].add(new int[] { 4, 4 });
        vertexs[4].add(new int[] { 3, 2 });
        bellmanford(n, 2);
        System.out.println(Arrays.toString(dist));
    }
}
