package datastructure.sp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * 使用邻接表的dijkstra算法
 * 适用于单源正边权图
 */
public class DijkstraII {

    /**
     * 使用邻接表的dijkstra算法
     * 
     * @param graph 元素 int[]{起点,权重}
     * @param start
     * @return
     */
    static int[] dijkstra(List<int[]>[] graph, int start) {
        int V = graph.length;
        int[] dist = new int[V];// 从1开始就是V+1;
        boolean[] vis = new boolean[V];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[start] = 0;
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> dist[a] - dist[b]);
        pq.add(start);
        while (!pq.isEmpty()) {
            int u = pq.poll();
            if (vis[u])
                continue;
            vis[u] = true;
            for (int[] edge : graph[u]) {
                int v = edge[0], w = edge[1];
                if (dist[v] > dist[u] + w) {
                    dist[v] = dist[u] + w;
                    pq.add(v);
                }
            }
        }
        return dist;
    }

    public static void main(String[] args) throws IOException {
        String path = System.getProperty("user.dir");
        Scanner in = new Scanner(Path.of(path + "/ src/datastructure/sp/sp.txt"),
                StandardCharsets.UTF_8);
        int vertexs = in.nextInt();
        int edges = in.nextInt();
        List<int[]>[] graph = (List<int[]>[]) new ArrayList[vertexs];
        for (int i = 0; i < vertexs; i++)
            graph[i] = new ArrayList<>();
        for (int i = 0; i < edges; i++) {
            int u = in.nextInt() - 1;
            int v = in.nextInt() - 1;
            int w = in.nextInt();
            graph[u].add(new int[] { v, w });
        }
        int[] dist = dijkstra(graph, 0);
        System.out.println(Arrays.toString(dist));
    }
}
