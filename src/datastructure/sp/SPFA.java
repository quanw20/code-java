package datastructure.sp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

class SEdge {
    public int v, w;

    public SEdge(int a, int b) {
        v = a;
        w = b;
    }
}

/**
 * 即 Shortest Path Faster Algorithm。
 * 
 * 很多时候我们并不需要那么多无用的松弛操作。
 * 很显然，只有上一次被松弛的结点，所连接的边，才有可能引起下一次的松弛操作。
 * 那么我们用队列来维护“哪些结点可能会引起松弛操作”，就能只访问必要的边了。
 * SPFA 也可以用于判断 s 点是否能抵达一个负环，只需记录最短路经过了多少条边，当经过了至少 n 条边时，说明 点可以抵达一个负环。
 */
@SuppressWarnings("all")
public class SPFA {
    static final int N = 1001;
    static ArrayList<SEdge>[] e = new ArrayList[N];
    static int[] dis = new int[N], cnt = new int[N];
    static boolean[] vis = new boolean[N];
    static Queue<Integer> q = new LinkedList<>();
    static {
        for (int i = 0; i < N; ++i) {
            e[i] = new ArrayList<>();
        }
    }

    static boolean spfa(int n, int s) {
        Arrays.fill(dis, 63);
        dis[s] = 0;
        vis[s] = true;
        q.add(s);
        while (!q.isEmpty()) {
            int u = q.poll();
            vis[u] = false;
            for (SEdge ed : e[u]) {
                int v = ed.v, w = ed.w;
                if (dis[v] > dis[u] + w) {
                    dis[v] = dis[u] + w;
                    cnt[v] = cnt[u] + 1;
                    if (cnt[v] >= n)
                        return false;
                    if (!vis[v]) {
                        q.add(v);
                        vis[v] = true;
                    }
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        e[0].add(new SEdge(1, 1));
        e[1].add(new SEdge(3, 2));
        e[2].add(new SEdge(0, 3));
        e[3].add(new SEdge(4, 4));
        e[4].add(new SEdge(3, 2));
        spfa(5, 2);
        System.out.println(Arrays.toString(dis));
    }
}
