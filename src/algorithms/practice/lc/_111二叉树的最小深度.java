package algorithms.practice.lc;

import java.util.LinkedList;
import java.util.Queue;



/**
 * BFS 模板
 * 
 * <pre>
 *  ?计算从起点 start 到终点 target 的最近距离
 *  int BFS(Node start, Node target) {
 *      Queue<Node> q; //? 核心数据结构
 *      Set<Node> visited; //? 避免走回头路
 *      q.offer(start); //? 将起点加入队列
 *      visited.add(start);
 *      int step = 0; //? 记录扩散的步数
 *      while (q not empty) {
 *          int sz = q.size();
 *          for (int i = 0; i < sz; i++) {//?将当前队列中的所有节点向四周扩散
 *              Node cur = q.poll();
 *              if (cur is target)//?划重点：这里判断是否到达终点
 *                  return step;
 *              for (Node x : cur.adj()) {//?将 cur 的相邻节点加入队列
 *                  if (x not in visited) {
 *                      q.offer(x);
 *                      visited.add(x);
 *                  }
 *              }
 *          }
 *          step++;// ?划重点：更新步数在这里 
 *      }
 *  }
 * /pre>
 */
public class _111二叉树的最小深度 {
    public int minDepth(TreeNode root) {
        if (root == null)
            return 0;
        int depth = 1;
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            for (int i = 0, l = q.size(); i < l; ++i) {
                TreeNode poll = q.poll();
                if (poll.left == null && poll.right == null)
                    return depth;
                if (poll.left != null) {
                    q.add(poll.left);
                }
                if (poll.right != null) {
                    q.add(poll.right);
                }
            }
            ++depth;
        }
        return depth;
    }
}
