package algorithms.practice.lc;

import java.util.Arrays;
import java.util.stream.IntStream;

public class _53MaximumSubarray {
  public static void main(String[] args) {
    int[] nums = { -3, 1, 3, -1, 2, -4, 2 };
    System.out.println(new Solution().maxSubArray(nums));
    System.out.println(new Solution3().maxSubArray(nums));
    System.out.println(Arrays.toString(new Solution2().maxSubArray(nums)));
  }

  static class Solution {
    int maxSubArray(int[] nums) {
      int window = 0;
      int maxSum = 0;
      int left = 0;
      int right = 0;
      while (right < nums.length) {
        window = window + nums[right];
        right++;
        maxSum = Math.max(window, maxSum);
        while (window < 0) {
          window -= nums[left];
          left++;
        }
      }
      return maxSum;
    }
  }

  static class Solution2 {
    int[] maxSubArray(int[] nums) {
      int window = 0;
      int maxSum = 0;
      int left = 0;
      int right = 0;
      int[] ret = null;
      while (right < nums.length) {
        window = window + nums[right];
        right++;
        if (window > maxSum) {
          maxSum = window;
          ret = new int[] { maxSum, left, right - 1 };
        }
        while (window < 0) {
          window -= nums[left];
          left++;
        }
      }
      return ret;
    }
  }

  static class Solution3 {
    // dp
    int maxSubArray(int[] nums) {
      // dp[i] max end now
      int[] dp = new int[nums.length];
      dp[0] = Math.max(0, nums[0]);
      for (int i = 1; i < nums.length; ++i) {
        dp[i] = Math.max(nums[i], dp[i - 1] + nums[i]);
      }
      return IntStream.of(dp).max().getAsInt();
    }
  }
}
