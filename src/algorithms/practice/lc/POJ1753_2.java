package algorithms.practice.lc;

import java.util.Scanner;

public class POJ1753_2 {
    private static final int n = 4, m = 4;
    private static final int[] a = new int[n + 1];
    private static final int[] b = new int[n + 1];
    private static final int[] change = new int[n + 1];
    private static final int[] cur = new int[n + 1];

    public static void main(String[] args) {
        // read
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < n; ++i) {
            char[] c = in.next().toCharArray();
            for (int j = 0; j < m; ++j) {
                if (c[j] == 'b')
                    a[i] |= 1 << j;
                else
                    b[i] |= 1 << j;
            }
        }
        // solve
        int res = Math.min(solve(a), solve(b));
        if (res > m * n)
            System.out.println("Impossible");
        else
            System.out.println(res);
        in.close();
    }

    private static int solve(int[] a) {
        int ret = Integer.MAX_VALUE;
        for (change[0] = 0; change[0] < 1 << m; ++change[0]) {
            int cnt = cnt(change[0]);
            cur[0] = a[0] ^ change[0] ^ (change[0] >> 1) ^ ((change[0] << 1) & ((1 << m) - 1));
            cur[1] = a[1] ^ change[0];
            for (int i = 1; i < n; ++i) {
                change[i] = cur[i - 1];
                cnt += cnt(change[i]);
                cur[i] = cur[i] ^ change[i] ^ (change[i] >> 1) ^ ((change[i] << 1) & ((1 << m) - 1));
                cur[i + 1] = a[i + 1] ^ change[i];
            }
            if (cur[n - 1] == 0)
                ret = Math.min(ret, cnt);
        }
        return ret;
    }

    private static int cnt(int i) {
        int ret = 0;
        while (i != 0) {
            if ((i & 1) == 1)
                ++ret;
            i >>= 1;
        }
        return ret;
    }
}
