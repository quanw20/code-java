package algorithms.practice.lc;

import java.util.Arrays;

import algorithms.Util;

/**
 * 魔塔
 */
public class 魔塔 {

  public static void main(String[] args) {
    int[][] a =
        // { { -2, -3, 3 }, { -5, -10, 1 }, { 10, 30, -5 } };
        { { 0, -5 }, { 0, 0 } };
    System.out.println(new Solution().calc(a));
    System.out.println(new Solution2().calculateMinimumHP(a));
  }

  static class Solution {
    int calc(int[][] a) {
      int M = a.length, N = a[0].length;
      // i,j 点到终点需要的生命值
      int[][] dp = new int[M][N];
      dp[M - 1][N - 1] = 1 - a[M - 1][N - 1] <= 0 ? 1 : 1 - a[M - 1][N - 1];
      // 最后一列
      for (int i = M - 2; i >= 0; --i) {
        int t = dp[i + 1][N - 1] - a[i][N - 1];
        dp[i][N - 1] = t <= 0 ? 1 : t;
      }
      for (int i = N - 2; i >= 0; --i) {
        int t = dp[M - 1][i + 1] - a[M - 1][i];
        dp[M - 1][i] = t <= 0 ? 1 : t;
      }
      for (int i = M - 2; i >= 0; --i) {
        for (int j = N - 2; j >= 0; --j) {
          int t = Math.min(dp[i][j + 1], dp[i + 1][j]) - a[i][j];
          dp[i][j] = t <= 0 ? 1 : t;
        }
      }
      Util.print(a);
      Util.print(dp);
      return dp[0][0];
    }
  }

  static class Solution2 {
    public int calculateMinimumHP(int[][] dungeon) {
      int[][] mome = new int[dungeon.length][dungeon[0].length];
      for (int i = 0; i < mome.length; ++i) {
        Arrays.fill(mome[i], -1);
      }
      return dp(dungeon, 0, 0, mome);
    }

    private int dp(int[][] a, int i, int j, int[][] mome) {
      if (i == a.length || j == a[0].length)
        return Integer.MAX_VALUE;
      if (mome[i][j] != -1)
        return mome[i][j];
      if (i == a.length - 1 && j == a[0].length - 1)
        return 1 - a[i][j] <= 0 ? 1 : 1 - a[i][j];
      int t = Math.min(
          dp(a, i + 1, j, mome),
          dp(a, i, j + 1, mome)) - a[i][j];
      mome[i][j] = (t <= 0) ? 1 : (t);
      return mome[i][j];
    }
  }
}