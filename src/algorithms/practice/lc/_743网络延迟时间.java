package algorithms.practice.lc;

import java.util.LinkedList;
import java.util.PriorityQueue;

import algorithms.Util;

public class _743网络延迟时间 {
    @SuppressWarnings("unchecked")
    public int networkDelayTime(int[][] times, int n, int k) {
        LinkedList<int[]>[] graph = new LinkedList[n + 1];
        for (int i = 1; i <= n; ++i) {
            graph[i] = new LinkedList<>();
        }
        for (int[] t : times) {
            graph[t[0]].add(new int[] { t[1], t[2] });
        }
        int[] dis = dijkstra(graph, n, k);
        Util.print(dis);
        int M = -1;
        for (int i = 1; i <= n; ++i) {
            if (dis[i] == Integer.MAX_VALUE)
                return -1;
            M = Math.max(dis[i], M);
        }
        return M;
    }

    private int[] dijkstra(LinkedList<int[]>[] graph, int n, int k) {
        int[] dis = new int[n + 1];
        for (int i = 1; i <= n; ++i) {
            dis[i] = Integer.MAX_VALUE;
        }
        dis[k] = 0;
        PriorityQueue<State> pq = new PriorityQueue<>((a, b) -> a.d - b.d);
        pq.add(new State(k, 0));
        while (!pq.isEmpty()) {
            State cur = pq.poll();
            int u = cur.u;
            if (cur.d > dis[u])
                continue;
            for (int[] d : graph[u]) {
                int v = d[0];
                if (dis[v] > dis[u] + d[1]) {
                    dis[v] = dis[u] + d[1];
                    pq.add(new State(v, dis[v]));
                }
            }
        }
        return dis;
    }

    class State {
        int u, d;

        State(int a, int b) {
            u = a;
            d = b;
        }
    }

    public static void main(String[] args) {
        int[][] times = { { 2, 1, 1 }, { 2, 3, 1 }, { 3, 4, 1 } };
        int n = 4, k = 2;
        _743网络延迟时间 a = new _743网络延迟时间();
        int bv = a.networkDelayTime(times, n, k);
        System.out.println(bv);
    }

}
