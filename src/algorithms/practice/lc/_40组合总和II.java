package algorithms.practice.lc;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * _40组合总和II
 * 
 * 有重复
 * 
 * <pre>
 * bt(nums,start) {
 *  for(int i=start;i<muns.length;++i){
 *      选择
 *      bt(nums,i+1);// 无重复是 i
 *      回溯
 *  }
 * }
 * </pre>
 */
public class _40组合总和II {
    List<List<Integer>> res = new LinkedList<>();

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        if (candidates.length == 0)
            return res;
        Arrays.sort(candidates);
        bt(candidates, target, 0, new LinkedList<>());
        return res;
    }

    private void bt(int[] candidates, int target, int start, LinkedList<Integer> ls) {
        if (target == 0) {
            res.add(new LinkedList<>(ls));
            return;
        }
        for (int i = start; i < candidates.length; ++i) {
            if (target - candidates[i] < 0 || i > start && candidates[i] == candidates[i - 1])
                continue;
            ls.addLast(candidates[i]);
            bt(candidates, target - candidates[i], i + 1, ls);
            ls.removeLast();
        }
    }

    public static void main(String[] args) {
        _40组合总和II a = new _40组合总和II();
        List<List<Integer>> c2 = a.combinationSum2(new int[] { 1, 2 }, 4);
        System.out.println(c2);
    }
}