package algorithms.practice.lc;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Graph_bfs_分酒
 * 
 * 不是显示的图
 * 状态转移 bfs
 */
public class GraphBfs分酒 {
    static HashSet<State> visited = new HashSet<>();
    static int[] v = { 9, 7, 4, 2 };
    static State start = new State("9,0,0,0", 0);
    static Queue<State> q = new LinkedList<>();
    static State end;

    /**
     * 有四个红酒瓶子,容量为9,7,4,2
     * 开始的状态是[9,0,0,0],即第一个瓶子满的,其他空着
     * 允许把酒从一个瓶子倒入另一个人,但只能倒满或倒空
     * 输入最终状态求操作次数[0,7,0,2]
     * 
     * @param s
     * @return
     */
    public static int bfs() {
        while (!q.isEmpty()) {
            State cur = q.poll();
            if (cur.equals(end))
                return cur.depth;
            int[] state = cur.getArray();
            for (int i = 0; i < state.length; i++) {
                if (state[i] > 0) {
                    // 将i倒入j
                    for (int j = 0; j < state.length; j++) {
                        if (j == i)
                            continue;
                        int rest_j = v[j] - state[j];// j的剩余容量
                        // 把i倒完
                        if (rest_j >= state[i]) {
                            int temp = state[i];
                            state[j] += state[i];
                            state[i] = 0;
                            add(q, new State(state, cur.depth + 1));
                            state[i] = temp;
                            state[j] -= state[i];
                        }
                        // 把j倒满
                        if (rest_j > 0 && state[i] >= rest_j) {
                            int temp = state[i];
                            state[i] -= rest_j;
                            state[j] += rest_j;
                            add(q, new State(state, cur.depth + 1));
                            state[i] = temp;
                            state[j] -= rest_j;
                        }
                    }
                }
            }
        }
        return -1;
    }

    private static void add(Queue<State> q2, State state) {
        if (!visited.contains(state)) {
            visited.add(state);
            q.offer(state);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        String line = in.nextLine();
        String s = line.replace(' ', ',');
        end = new State(s, 0);
        q.add(start);
        int bfs = bfs();
        System.out.println(bfs);
        in.close();
    }

}

class State {
    String val;
    int depth;

    State(String v, int d) {
        val = v;
        depth = d;
    }

    State(int[] a, int d) {
        char[] c = new char[7];
        for (int i = 0; i < 7; i++) {
            if (i % 2 == 0)
                c[i] = (char) (a[i / 2] + 48);
            else
                c[i] = ',';
        }
        val=new String(c);
        depth = d;
    }

    int[] getArray() {
        return Arrays.stream(val.split(",")).mapToInt(Integer::parseInt).toArray();
    }

    @Override
    public boolean equals(Object obj) {
        return val.equals(((State) obj).val);
    }
}