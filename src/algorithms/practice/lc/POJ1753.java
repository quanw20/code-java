package algorithms.practice.lc;

/**
 * Flip Game
 * 翻转游戏是在一个 4x4 的长方形场地上进行的，在其 16 个方格中的每一个方格上都放置有两侧的棋子。
 * 每件的一面是白色的，另一面是黑色的，每件都是黑色或白色的一面朝上。
 * 每轮你翻转 3 到 5 块，从而将它们上侧的颜色从黑色变为白色，反之亦然。
 * 每轮都根据以下规则选择要翻转的棋子：
 * 1.选择 16 件中的任何一件。
 * 2.将选定的棋子以及所有相邻的棋子翻转到所选棋子的左侧、右侧、顶部和底部（如果有的话）。
 * 
 */
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class POJ1753 {
    private static final int N = 4, M = 4;
    static final int[] field = new int[N];

    public static void main(String[] args) throws IOException {
        // Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < M; ++i) {
            char[] c = in.next().toCharArray();
            for (int j = 0; j < M; ++j) {
                field[i] <<= 1;
                if (c[j] == 'b')
                    ++field[i];// 将最后一位变成1
            }
        }
        int i = 0;
        for (; i < 16; ++i)// 枚举翻转次数
            if (dfs(i, 0, -1)) {
                System.out.println(i);
                break;
            }
        if (i == 16)
            System.out.println("Impossible");

    }

    // 翻 n 个棋子
    private static boolean dfs(int n, int i, int j) {
        if (n == 0)
            return check();
        ++j;// 下一列
        if (j == 4) {
            ++i;
            j = 0;
        }
        if (i == 4) // 如果超过棋盘大小则不可能
            return false;
        for (; i < N; ++i) {
            for (; j < M; ++j) {
                flip(i, j);// 翻转
                if (dfs(n - 1, i, j))// 在当前基础上递归
                    return true;
                flip(i, j);// 回溯
            }
            j = 0;// 新一行
        }
        return false;
    }

    // 1000, 0100, 0010, 0001
    // 1100, 1110, 0111, 0011
    private final static int[][] P = { { 8, 4, 2, 1 }, { 12, 14, 7, 3 } };

    // 对(i, j)位置的棋子进行翻转
    private static void flip(int i, int j) {
        if (i > 0)
            field[i - 1] ^= P[0][j];
        field[i] ^= P[1][j];
        if (i < N - 1)
            field[i + 1] ^= P[0][j];
    }

    // 0或15: 全0或全1
    private static boolean check() {
        return (field[0] == 0 || field[3] == 15) &&
                field[0] == field[1] &&
                field[0] == field[2] &&
                field[0] == field[3];
    }
}
