package algorithms.practice.lc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 有重复, 不可重用
 * 
 * 知道怎么剪枝 : 画出树状图
 */
public class _90子集II {
    // 解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
    List<List<Integer>> res = new ArrayList<>();
    LinkedList<Integer> ls = new LinkedList<>();

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums); // 让相同元素相邻
        bt(nums, 0);
        return res;
    }

    private void bt(int[] nums, int start) {
        res.add(new LinkedList<>(ls));
        for (int i = start; i < nums.length; ++i) {
            if (i > start && nums[i] == nums[i - 1])// 加一个剪枝
                continue;
            ls.add(nums[i]);
            bt(nums, i + 1);
            ls.removeLast();
        }
    }

    public static void main(String[] args) {
        var a = new _90子集II();
        List<List<Integer>> subsets = a.subsetsWithDup(new int[] { 1, 2, 2 });
        System.out.println(subsets);
    }
}
