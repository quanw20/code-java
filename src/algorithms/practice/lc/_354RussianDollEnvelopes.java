package algorithms.practice.lc;

import java.util.Arrays;
import java.util.Comparator;

public class _354RussianDollEnvelopes {

  public static int maxEnvelopes(int[][] envelopes) {
    Comparator<int[]> cmp = (x, y) -> {
      if (x[0] > y[0])
        return 1;
      else if (x[0] < y[0])
        return -1;
      else
        return x[1] < y[1] ? 1 : x[1] == y[1] ? 0 : -1;
    };
    Arrays.sort(envelopes, cmp);
    int[]top = new int[envelopes.length];
    int piles = 0;
    for (int i = 0; i < envelopes.length; i++) {
      int l = 0, r = piles;
      while (l < r) {
        int mid = l + (r - l) / 2;
        if (top[mid] >= envelopes[i][1])
          r = mid;
        else
          l = mid + 1;
      }
      if (l == piles)
        ++piles;
      top[l] = envelopes[i][1];
    }

    return piles;
  }

  public static void main(String[] args) {
    int[][] a = {
        { 5, 4 }, { 6, 4 }, { 6, 7 }, { 2, 3 }
    };
    System.out.println(maxEnvelopes(a));
  }
}