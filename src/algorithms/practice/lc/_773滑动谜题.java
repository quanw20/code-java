package algorithms.practice.lc;

import java.util.HashSet;
import java.util.LinkedList;

public class _773滑动谜题 {
    int[][] adj = {
            { 1, 3 }, { 0, 2, 4 }, { 1, 5 }, { 0, 4 }, { 1, 3, 5 }, { 2, 4 }
    };

    public int slidingPuzzle(int[][] board) {
        int step = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 3; j++)
                sb.append(board[i][j]);
        String start = sb.toString();
        String target = "123450";
        LinkedList<String> q = new LinkedList<>();
        HashSet<String> vis = new HashSet<>();
        q.add(start);
        vis.add(start);
        while (!q.isEmpty()) {
            for (int i = 0, l = q.size(); i < l; i++) {
                String p = q.poll();
                if (p.equals(target))
                    return step;
                int index = p.indexOf('0');
                for (int d : adj[index]) {
                    String s = swap(p.toCharArray(), index, d);
                    if (!vis.contains(s)) {
                        q.add(s);
                        vis.add(s);
                    }
                }
            }
            ++step;
        }
        return -1;
    }

    private String swap(char[] ca, int i, int j) {
        char c = ca[i];
        ca[i] = ca[j];
        ca[j] = c;
        return new String(ca);
    }

    public static void main(String[] args) {
        int[][] board = { { 1, 2, 3 }, { 4,0,5 } };
        _773滑动谜题 a = new _773滑动谜题();
        int s = a.slidingPuzzle(board);
        System.out.println(s);
    }
}
