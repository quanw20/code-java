package algorithms.practice.lc;

import java.util.stream.IntStream;

import algorithms.Util;

public class LCS {
  public static void main(String[] args) {
    System.out.println(
        new Solution().longestCommonSubsequence("acb", "zabcde"));
  }

  static class Solution {
    int longestCommonSubsequence(String s1, String s2) {
      int[][] dp = new int[s1.length() + 1][s2.length() + 1];
      for (int i = 1; i < dp.length; i++) {
        for (int j = 1; j < dp[0].length; j++) {
          dp[i][j] = (s1.charAt(i - 1) == s2.charAt(j - 1) ? 1 : 0)
              + IntStream.of(dp[i - 1][j], dp[i][j - 1], dp[i - 1][j - 1]).max().getAsInt();

        }
      }
      // Util.print(dp);
      return dp[dp.length - 1][dp[0].length - 1];
    }
  }
}
