package algorithms.practice.lc;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class CrazyRows {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            char[] ca = in.next().toCharArray();
            for (int j = 0; j < n; ++j)
                if (ca[j] == '1')
                    ++a[i];
        }
        int res = 0;
        for (int i = 0; i < n; ++i) {
            int p = 0;
            for (int j = i; j < n; ++j) {
                if (a[j] - 1 <= i) {
                    p = j;
                    break;
                }
            }
            for (int j = p; j > i; j--) {
                int tp = a[j];
                a[j] = a[j - 1];
                a[j - 1] = tp;
                ++res;
            }
        }
        System.out.println(res);
        
        
    }
}
/**
 * 4
 * 1110
 * 1100
 * 1100
 * 1000
 */
