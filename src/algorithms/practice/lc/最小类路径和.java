package algorithms.practice.lc;

import java.util.stream.IntStream;
/**
 * 初值：第一行
 * 状态转移：能上一行到达的，中最小的加当前
 */
public class 最小类路径和 {
  public static void main(String[] args) {
    int[][] matrix = { { 2, 1, 3 }, { 6, 5, 4 }, { 7, 8, 9 } };
    System.out.println(minFallingPathSum(matrix));
  }

  static int minFallingPathSum(int[][] matrix) {
    int l = matrix[0].length;
    int m=matrix.length;
    for (int i = 1; i < m; ++i) {
      for (int j = 0; j < l; ++j) {
        int min = Integer.MAX_VALUE;
        if (j - 1 >= 0)
          min = Math.min(matrix[i - 1][j - 1], min);
        if (j + 1 < l)
          min = Math.min(matrix[i - 1][j + 1], min);
        min = Math.min(matrix[i - 1][j], min);
        matrix[i][j] += min;
      }
    }
    
    return IntStream.of(matrix[m-1]).min().getAsInt();
  }
}
