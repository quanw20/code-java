package algorithms.practice.lc;

import java.util.Arrays;

public class _115不同的子序列 {

  public static void main(String[] args) {
    String s = "babgbag", t = "bag";
    System.out.println(new Solution().numDistinct(s, t));
    System.out.println(new Solution2().numDistinct(s, t));
    System.out.println(new Solution3().numDistinct(s, t));
  }
}

class Solution {
  int[][] memo;

  int numDistinct(String s, String t) {
    int m = s.length();
    int n = t.length();
    memo = new int[m][n];
    for (int i = 0; i < m; ++i) {
      Arrays.fill(memo[i], -1);
    }
    return dfs(s.toCharArray(), m - 1, t.toCharArray(), n - 1);
  }

  private int dfs(char[] s, int m, char[] t, int n) {
    if (n == -1)
      return 1;
    if (s.length - m < t.length - n)
      return 0;
    if (memo[m][n] != -1)
      return memo[m][n];
    int res = 0;
    for (int i = m; i >= 0; --i) {
      if (s[i] == t[n])
        res += dfs(s, i - 1, t, n - 1);
    }
    return res;
  }
}

class Solution2 {
  int[][] memo;

  int numDistinct(String s, String t) {
    int m = s.length();
    int n = t.length();
    memo = new int[m][n];
    for (int i = 0; i < m; ++i) {
      Arrays.fill(memo[i], -1);
    }
    return dfs(s.toCharArray(), 0, t.toCharArray(), 0);
  }

  private int dfs(char[] s, int m, char[] t, int n) {
    if (n == t.length)
      return 1;
    if (s.length - m < t.length - n)
      return 0;
    if (memo[m][n] != -1)
      return memo[m][n];
    int res = 0;
    for (int i = m; i < s.length; ++i) {
      if (s[i] == t[n])
        res += dfs(s, i + 1, t, n + 1);
    }
    return res;
  }
}

class Solution3 {
  int[][] memo;

  int numDistinct(String s, String t) {
    int m = s.length();
    int n = t.length();
    memo = new int[m][n];
    for (int i = 0; i < m; ++i) {
      Arrays.fill(memo[i], -1);
    }
    return dfs(s.toCharArray(), 0, t.toCharArray(), 0);
  }

  private int dfs(char[] s, int m, char[] t, int n) {
    if (n == t.length)
      return 1;
    if (s.length - m < t.length - n)
      return 0;
    if (memo[m][n] != -1)
      return memo[m][n];
    int res = 0;
    if (s[m] == t[n]) {
      // 选当前相等的或者不选当前相等的
      res += dfs(s, m + 1, t, n + 1) + dfs(s, m + 1, t, n);
    } else {
      res += dfs(s, m + 1, t, n + 1);
    }
    return res;
  }
}