package algorithms.practice.lc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

public class _391完美矩形 {
    public boolean isRectangleCover(int[][] rectangles) {
        // 1左下,2右上
        // 1. 面积
        int X1 = Integer.MAX_VALUE, Y1 = X1, X2 = Integer.MIN_VALUE, Y2 = X2, area = 0;
        for (int[] r : rectangles) {
            if (r[0] <= X1 && r[1] <= Y1) {
                X1 = r[0];
                Y1 = r[1];
            }
            if (r[2] >= X2 && r[3] >= Y2) {
                X2 = r[2];
                Y2 = r[3];
            }
            area += (r[3] - r[1]) * (r[2] - r[0]);
        }
        int Area = (X2 - X1) * (Y2 - Y1);
        HashSet<Integer> set = new HashSet<>();
        set.add(to(X1, Y1));
        set.add(to(X1, Y2));
        set.add(to(X2, Y1));
        set.add(to(X2, Y2));
        if (area != Area)
            return false;
        // 2.顶点
        for (int[] r : rectangles) {
            int key = to(r[0], r[1]);
            map.put(key, map.getOrDefault(key, 0) + 1);
            key = to(r[2], r[3]);
            map.put(key, map.getOrDefault(key, 0) + 1);
            key = to(r[0], r[3]);
            map.put(key, map.getOrDefault(key, 0) + 1);
            key = to(r[2], r[1]);
            map.put(key, map.getOrDefault(key, 0) + 1);
        }
        int cnt = 0;
        for (Entry<Integer, Integer> e : map.entrySet()) {
            if (e.getValue() % 2 != 0) {
                if (!set.contains(e.getKey()))
                    return false;
                ++cnt;
            }
        }
        if (cnt > 4)
            return false;
        // [[0,0,1,1],[0,0,1,1],[1,1,2,2],[1,1,2,2]]
        if (rectangles.length > 1) {
            int i = 0;
            for (; i < 4; ++i) {
                if (rectangles[0][i] != rectangles[1][i])
                    break;
            }
            return !(i == 4);
        }
        return true;
    }

    final int N = 108;
    HashMap<Integer, Integer> map = new HashMap<>();

    int to(int x, int y) {
        return x * N + y;
    }

}
