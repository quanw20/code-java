package algorithms.practice.lc;

import java.util.LinkedList;
import java.util.List;

/**
 * 回溯
 * 
 * 无重复, 可重用
 * 
 * <pre>
 * void comb(int[] cand, int start, int target, LinkedList<Integer> ls) {
 *     for (int i = start; i < cand.length; ++i) {
 *         ls.add(cand[i]);
 *         comb(cand, i, target, ls);// 从i开始而不是 i+1
 *         ls.removeLast();
 *     }
 * }
 * </pre>
 */
public class _39组合总和 {
    List<List<Integer>> res = new LinkedList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        comb(candidates, 0, target, new LinkedList<>());
        return res;
    }

    void comb(int[] cand, int start, int target, LinkedList<Integer> ls) {
        if (target < 0)
            return;
        if (target == 0) {
            res.add(new LinkedList<>(ls));
            return;
        }
        for (int i = start; i < cand.length; ++i) {
            ls.add(cand[i]);
            comb(cand, i, target - cand[i], ls);// 从i开始而不是 i+1
            ls.removeLast();
        }
    }

    public static void main(String[] args) {
        int[] candidates = { 2, 3, 6, 7 };
        _39组合总和 a = new _39组合总和();
        List<List<Integer>> ls = a.combinationSum(candidates, 7);
        System.out.println(ls);
    }
}
