package algorithms.practice.pta;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class 树的同构 {
    static Scanner sc = null;
    static {
        // sc = new Scanner(new BufferedInputStream(System.in));
        try {
            sc = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        } catch (IOException e) {
        }
    }
    static boolean[] check;
    static TN1[] t1 = new TN1[31], t2 = new TN1[31];

    static boolean isomorphism(Integer r1, Integer r2) {
        if (r1 == null && r2 == null)
            return true;
        if (r1 == null && r2 != null || r1 != null && r2 == null)
            return false;
        if (t1[r1].val != t2[r2].val)
            return false;
        return isomorphism(t1[r1].left, t2[r2].left) && isomorphism(t1[r1].right, t2[r2].right) ||
                isomorphism(t1[r1].left, t2[r2].right) && isomorphism(t1[r1].left, t2[r2].right);
    }

    static Integer build(TN1[] tree) {
        int n = sc.nextInt();
        check = new boolean[n];
        for (int i = 0; i < n; i++) {
            char val = sc.next().charAt(0);
            tree[i] = new TN1(val);
            char t = sc.next().charAt(0);
            if (t != '-') {
                tree[i].left = t - '0';
                check[t - '0'] = true;
            }
            t = sc.next().charAt(0);
            if (t != '-') {
                tree[i].right = t - '0';
                check[t - '0'] = true;
            }
        }
        for (int i = 0; i < n; i++) {
            if (!check[i])
                return i;
        }
        return null;
    }

    public static void main(String[] args) {
        Integer root1 = build(t1);
        Integer root2 = build(t2);
        System.out.println(isomorphism(root1, root2) ? "Yse" : "No");
    }

}

class TN1 {
    Integer left = null, right = null;
    Character val;

    /**
     * @param val
     */
    public TN1(char val) {
        this.val = val;
    }
}