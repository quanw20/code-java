package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class 乘法口诀数列 {
    public static void main(String[] args) {
        int a, b, n;
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        a = in.nextInt();
        b = in.nextInt();
        n = in.nextInt();
        int[] q = new int[n+1];
        q[0] = a;
        q[1] = b;
        a = 0;
        b = 1;
        for (int i = 2; i < n;) {
            int p = q[a++] * q[b++];
            if (p / 10 != 0) {
                q[i++] = p / 10;
                q[i++] = p % 10;
            } else {
                q[i++] = p;
            }
        }
        System.out.print(q[0]);
        for (int i = 1; i < n; i++) {
            System.out.print(" " + q[i]);
        }
        in.close();
    }

}
