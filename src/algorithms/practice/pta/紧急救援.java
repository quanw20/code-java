package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;
// todo 没做完
public class 紧急救援 {
    static final int N = 501;
    static int[][] a = new int[N][N];
    static int[] p = new int[N];
    static int[] cnt = new int[N];
    static boolean[] vis;
    static int[] dis;
    static Scanner in = new Scanner(new BufferedInputStream(System.in));


    public static void main(String[] args) {
        int n, m, s, d, u, v, w;
        n = in.nextInt();
        m = in.nextInt();
        s = in.nextInt();
        d = in.nextInt();
        for (int i = 0; i < n; i++)
            cnt[i] = in.nextInt();

        while (m-- != 0) {
            u = in.nextInt();
            v = in.nextInt();
            w = in.nextInt();
            a[u][v] = w;
        }

        dijkstra(a, s);
        System.out.println(Arrays.toString(dis) + " : " + dis[d]);
        System.out.println(Arrays.toString(p));
    }

    private static void dijkstra(int[][] a, int s) {
        int L = a.length;
        vis = new boolean[L];
        dis = new int[L];
        Arrays.fill(dis, Integer.MAX_VALUE);
        dis[s] = 0;
        PriorityQueue<Integer> pq = new PriorityQueue<>((x, y) -> dis[x] - dis[y]);
        pq.add(s);
        for (int i = 0; i < L - 1; ++i) {
            int u = pq.poll();
            vis[u] = true;
            for (int v = 0; v < L; ++v) {
                if (!vis[v] && a[u][v] > 0 && dis[v] > dis[u] + a[u][v]) {
                    dis[v] = dis[u] + a[u][v];
                    p[u] = v;
                    pq.add(v);
                }
            }
        }
    }

}
