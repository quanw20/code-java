package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

/**
 * 二叉堆的后序遍历
 */
public class 完全二叉树的层序遍历 {
    static int[] res;
    static int n;
    static Scanner sc = null;
    static {
        sc = new Scanner(new BufferedInputStream(System.in));
        // try {
        // sc = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        // } catch (IOException e) {
        // }
    }

    public static void main(String[] args) {
        n = sc.nextInt();
        res = new int[n + 1];
        build(1);
        System.out.print(res[1]);
        for (int i = 2; i <= n; i++)
            System.out.print(" " + res[i]);
    }

    private static void build(int i) {
        if (i > n)
            return;
        build(i << 1);
        build((i << 1) + 1);
        res[i] = sc.nextInt();
    }
}
