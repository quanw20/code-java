package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class 家庭房产 {
    static final int SIZE = 10001;// 4位
    static int[] parent = new int[SIZE];
    static int[] number = new int[SIZE];
    static int[] houseNum = new int[SIZE];
    static int[] houseCov = new int[SIZE];
    static Info[] ans = new Info[1001];// <=1000
    static Input[] in = new Input[1001];
    static {
        for (int i = 1; i < SIZE; i++) {
            parent[i] = i;
            number[i] = 1;
        }
        for (int i = 1; i < 1001; ++i) {
            in[i] = new Input();
        }
    }

    static int find(int v) {
        while (parent[v] != v) {
            parent[v] = parent[parent[v]];
            v = parent[v];
        }
        return v;
    }

    public static void main(String[] args) throws IOException {
        // Scanner sc = new Scanner(Path.of("a.txt"),StandardCharsets.UTF_8);
        Scanner sc = new Scanner(new BufferedInputStream(System.in));
        int N = sc.nextInt();
        for (int i = 1; i <= N; i++) {
            in[i].id[0] = sc.nextInt();
            in[i].id[1] = sc.nextInt();
            in[i].id[2] = sc.nextInt();
            in[i].num = sc.nextInt();
            for (int j = 3; j < in[i].num + 3; j++)
                in[i].id[j] = sc.nextInt();
            in[i].houseN = sc.nextInt();
            in[i].houseC = sc.nextInt();
            houseNum[in[i].id[0]] += in[i].houseN;
            houseCov[in[i].id[0]] += in[i].houseC;
        }
        for (int i = 1; i <= N; i++) {// 使用并查集合并
            int id1 = find(in[i].id[0]);// 根
            for (int j = 1; j < in[i].num + 3; j++) {
                if (in[i].id[j] == -1)
                    continue;
                int id2 = find(in[i].id[j]); // 父母或孩子
                if (id1 < id2) {
                    parent[id2] = id1;// 将每个家庭的最小id作为根
                    houseNum[id1] += houseNum[id2];
                    houseCov[id1] += houseCov[id2];
                    number[id1] += number[id2];
                } else if (id1 > id2) {
                    parent[id1] = id2;
                    houseNum[id2] += houseNum[id1];
                    houseCov[id2] += houseCov[id1];
                    number[id2] += number[id1];
                    id1 = id2;// 还是同一个家庭,只是更新了根
                }
            }
        }
        int cnt = 0;
        for (int i = 0; i < SIZE; i++)
            if (parent[i] == i && houseNum[i] != 0)
                ans[cnt++] = new Info(i, number[i], 1.0 * houseNum[i] / number[i], 1.0 * houseCov[i] / number[i]);
        Arrays.sort(ans, 0, cnt, (x, y) -> {
            if (x.aveC != y.aveC)
                return (int) (y.aveC - x.aveC);
            return x.id - y.id;
        });
        System.out.println(cnt);
        for (int i = 0; i < cnt; i++)
            System.out.printf("%04d %d %.3f %.3f\n", ans[i].id, ans[i].number, ans[i].aveH, ans[i].aveC);
    }
}

class Info {
    int id, number;
    double aveH, aveC;

    public Info(int id, int number, double aveH, double aveC) {
        this.id = id;
        this.number = number;
        this.aveH = aveH;
        this.aveC = aveC;
    }
}

class Input {
    int[] id = new int[9];
    int num, houseN, houseC;
}