package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Scanner;

class Cake {
    public double a, b;

    Cake(double x) {
        a = x;
    }

    @Override
    public String toString() {
        return a + "|" + b;
    }
}

public class 月饼 {
    static Scanner in = new Scanner(new BufferedInputStream(System.in));
    static int n, m;
    static ArrayList<Cake> al = new ArrayList<>();

    public static void main(String[] args) {
        n = in.nextInt();
        m = in.nextInt();
        for (int i = 0; i < n; i++) {
            al.add(new Cake(in.nextDouble()));
        }
        for (int i = 0; i < n; i++) {
            al.get(i).b = in.nextDouble();
        }
        al.sort((x, y) -> (y.b / y.a - x.b / x.a) > 0 ? 1 : -1);
        // System.out.println(al);
        double ret = 0;
        for (Cake c : al) {
            if (c.a >= m) {
                ret += m * 1.0 * c.b / c.a;
                break;
            } else {
                ret += c.b;
                m -= c.a;
            }
        }
        System.out.printf("%.2f", ret);
    }
}
