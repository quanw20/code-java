package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class 查验身份证 {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        int[] w = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
        char[] m = "10X98765432".toCharArray();
        boolean has = false;
        while (n-- != 0) {
            String id = in.next();
            // 首先对前17位数字加权求和
            int sum = 0;
            for (int i = 0; i < 17; ++i) {
                sum += (id.charAt(i) - 48) * w[i];
            }
            // 然后将计算的和对11取模得到值Z
            int z = sum % 11;
            // 对应Z值与校验码M的值
            if (m[z] != id.charAt(17)) {
                System.out.println(id);
                has=true;
            }
        }
        if(!has)
        System.out.println("All passed");
        in.close();
    }

}
