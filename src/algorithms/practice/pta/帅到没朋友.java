package algorithms.practice.pta;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class 帅到没朋友 {
    public static void main(String[] args) {
        boolean has = false;
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        while (n-- != 0) {
            int k = in.nextInt();
            int v = in.nextInt();
            while (--k != 0) {
                int w = in.nextInt();
                union(w, v);
                h[v] = true;
            }
        }
        int m = in.nextInt();
        while (m-- != 0) {
            int k = in.nextInt();
            if (p[k] == k && !h[k]) {
                if (!has) {
                    System.out.print(k);
                    has=true;
                } else
                    System.out.print(" " + k);
                p[k] = k;
                h[k] = true;
            }
        }
        if (!has)
            System.out.println("No one is handsome");

        in.close();
    }

    static int[] p = new int[1000000];
    static {
        for (int i = 99999; i > 0; i--) {
            p[i] = i;
        }
    }
    static boolean[] h = new boolean[1000000];

    static void union(int v, int w) {
        int r = find(v);
        int q = find(w);
        if (r != q)
            p[r] = q;
    }

    static int find(int v) {
        while (v != p[v]) {
            p[v] = p[p[v]];
            v = p[v];
        }
        return v;
    }
}
