package algorithms.practice.t2014;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.jupiter.api.extension.ExtensionContext.Store;

/**
 * 
 * 斐波那契数列大家都非常熟悉。它的定义是：
 * f(x) = 1 … (x=1,2)
 * f(x) = f(x-1) + f(x-2) … (x>2)
 * 对于给定的整数 n 和 m，我们希望求出：
 * f(1) + f(2) + … + f(n) 的值。但这个值可能非常大，所以我们把它对 f(m) 取模。
 * 公式参见【图1.png】
 * 但这个数字依然很大，所以需要再对 p 求模。
 * 【数据格式】
 * 输入为一行用空格分开的整数 n m p (0 < n, m, p < 10^18)
 * 输出为1个整数
 * 
 * 例如，如果输入：
 * 2 3 5
 * 程序应该输出：
 * 0
 * 
 * 再例如，输入：
 * 15 11 29
 * 程序应该输出：
 * 25
 * 
 * 分析：
 * f(n)=f(n-1)+f(n-2)
 * f(n+1)=f(n)+f(n-1)
 * f(n)=f(n+1)-f(n-1)
 * 
 * 求和1->n(f(i))=f(n+2)-1
 * 
 * double sqr = Math.sqrt(5);
 * double ret = (Math.pow((1 + sqr) / 2, n) - Math.pow((1 - sqr) / 2, n)) / sqr;
 */
public class _09斐波那契 {

  /**
   * 矩阵乘法 A(m*n)xB(n*p)=C(m*p)
   * 
   * @param a
   * @param b
   * @return
   */
  public static long[][] matrixMutiply(long[][] a, long[][] b) {
    long[][] c = new long[a.length][b[0].length];
    for (int i = 0; i < a.length; i++)
      for (int j = 0; j < b[0].length; j++)
        for (int k = 0; k < b.length; k++)
          c[i][j] += a[i][k] * b[k][j];
    return c;
  }

  /**
   * 使用矩阵计算fib
   * 
   * @param n
   * @return
   */
  public static long fibMatrix(long n) {
    if (n <= 2)
      return 1;
    long[][] a = { { 1, 1 }, { 1, 0 } };
    long[][] pow = matrixPow(a, n);
    return pow[1][0];
  }

  /**
   * 矩阵 快速幂
   * 
   * 计算矩阵A的b次幂
   * 
   * @param a
   * @param b
   * @return
   */
  public static long[][] matrixPow(long[][] a, long b) {
    long[][] ret = new long[a.length][a.length];
    // 单位阵
    for (int i = 0; i < a.length; i++)
      ret[i][i] = 1;
    while (b > 0) {
      if ((b & 1) != 0) {
        ret = matrixMutiply(a, ret);
      }
      a = matrixMutiply(a, a);
      b >>= 1;
    }
    return ret;
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int m = in.nextInt();
    int n = in.nextInt();
    int p = in.nextInt();
    long ret = (fibMatrix(n + 2) - 1) % fibMatrix(n) % p;
    System.out.println(ret);
    System.out.println(calc(m, n, p));

  }

  private static BigInteger calc(int m, int n, int p) {
    if (m + 2 <= n)
      return BigInteger.ZERO;
    BigDecimal fm = cal(m + 2).subtract(BigDecimal.ONE);
    BigDecimal fn = cal(n);

    return fm.toBigInteger()
        .mod(fn.toBigInteger())
        .mod(BigInteger.valueOf(p));
  }

  private static BigDecimal cal(int m) {
    BigDecimal sqrt = BigDecimal.valueOf(5).sqrt(MathContext.DECIMAL128);
    return BigDecimal.ONE.add(sqrt).divide(BigDecimal.valueOf(2), 64, RoundingMode.DOWN).pow(m)
        .subtract(BigDecimal.ONE.subtract(sqrt).divide(BigDecimal.valueOf(2), 64, RoundingMode.DOWN).pow(m))
        .divide(sqrt, 64, RoundingMode.DOWN);
  }

}
