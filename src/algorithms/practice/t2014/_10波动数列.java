package algorithms.practice.t2014;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * 
 * 观察这个数列：
 * 1 3 0 2 -1 1 -2 …
 * 这个数列中后一项总是比前一项增加2或者减少3。
 * 栋栋对这种数列很好奇，他想知道长度为 n 和为 s 而且后一项总是比前一项增加a或者减少b的整数数列可能有多少种呢？
 * 
 * 【数据格式】
 * 输入的第一行包含四个整数 n s a b，含义如前面说述。
 * 输出一行，包含一个整数，表示满足条件的方案数。由于这个数很大，请输出方案数除以100000007的余数。
 * 
 * 例如，输入：
 * 4 10 2 3
 * 程序应该输出：
 * 2
 * 
 * => 2 4 1 3 => 7 4 1 -2
 * 
 */
public class _10波动数列 {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int s = in.nextInt();
    int a = in.nextInt();
    int b = in.nextInt();
    solve1(n, s, a, b);
    solve2(n, s, a, b);
    
  }

  private static void solve2(int n, int s, int a, int b) {
  }

  private static void solve1(int n, int s, int a, int b) {
    int x = 0;
    int cnt = 0;
    int[] sum = new int[n];
    for (int i = 0, l = (int) Math.pow(2, n - 1); i < l; ++i) {
      x = i;
      for (int j = 1; j < n; ++j) {
        sum[j] = sum[j - 1] + ((x & 1) == 1 ? a : -b);
        x >>= 1;
      }
      int r = IntStream.of(sum).reduce(0, Integer::sum);
      if (Math.abs(s - r) > n && (s - r) % n == 0) {
        System.out.println(Arrays.toString(sum));
        cnt = (cnt + 1) % 100000007;
      }
    }
    System.out.println(cnt);
  }

}
