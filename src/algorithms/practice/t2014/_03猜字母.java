package algorithms.practice.t2014;

import java.util.Arrays;

/**
 * 把abcd…s共19个字母组成的序列重复拼接106次，得到长度为2014的串。
 * 接下来删除第1个字母（即开头的字母a），以及第3个，第5个等所有奇数位置的字母。
 * 得到的新串再进行删除奇数位置字母的动作。如此下去，最后只剩下一个字母，请写出该字母。
 * 答案是一个小写字母，请通过浏览器提交答案。不要填写任何多余的内容。
 * 
 * 1. 用后面的数字覆盖
 * 2. 两个数组
 */
public class _03猜字母 {
  public static void main(String[] args) {
    char[] ca = new char[2014];
    for(int i=0;i<ca.length;){
      for(char j='a';j<='s';++j,++i){
        ca[i]=j;
      }
    }
    // System.out.println(Arrays.toString(ca));
    solve1(ca,ca.length);
  }

  private static void solve1(char[] ca, int l) {
    if(l==1){
      System.out.println(ca[0]);
      System.out.println(Arrays.toString(ca));
      return;
    }
    int p = 0;
    for (int i = 0; i < l; ++i) {
      if ((i & 1) == 1) {// 留下
        ca[p] = ca[i];
        p++;
      }
    }
    solve1(ca, l/2);
  }
}
