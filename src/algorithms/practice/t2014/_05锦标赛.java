package algorithms.practice.t2014;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * 找出一个数组里面前两位最大的,返回下标和数值
 */
public class _05锦标赛 {
  public static void main(String[] args) {
    int[] a = { 1, 8, 22, 15, 3, 6, 12, 13 };
    int[] b = IntStream.range(0, a.length).toArray();
    System.out.println(Arrays.toString(getMax(a, b, 0, b.length - 1)));
    System.out.println(Arrays.toString(getMax(a, b, 1, b.length - 1)));
  }

  private static int[] getMax(int[] a, int[] b, int l, int r) {
    for (int i = r; i >= l; --i) {
      if (a[b[i]] > a[b[i / 2]]) {
        swap(b, i, i / 2);
      }
    }
    return new int[] { b[l], a[b[l]] };
  }

  private static void swap(int[] a, int i, int j) {
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;
  }
}
