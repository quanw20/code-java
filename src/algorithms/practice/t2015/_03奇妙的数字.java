package algorithms.practice.t2015;

import java.util.Arrays;

/**
 * 小明发现了一个奇妙的数字。
 * 它的平方和立方正好把0~9的10个数字每个用且只用了一次。
 * 你能猜出这个数字是多少吗？
 */
public class _03奇妙的数字 {
  public static void main(String[] args) {
    for (int i = 50; i < 100; ++i) {
      String s1 = String.valueOf(i * i);
      String s2 = String.valueOf(i * i * i);
      if (check(s1 + s2))
        System.out.println(i);
    }
  }

  private static boolean check(String string) {
    char[] ca = string.toCharArray();
    Arrays.sort(ca);
    char[] cb = "0123456789".toCharArray();
    return Arrays.equals(ca, cb);
  }
}
