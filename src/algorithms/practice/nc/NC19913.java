package algorithms.practice.nc;

import java.util.*;

public class NC19913 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int b = in.nextInt();
        int[] a = new int[n];
        int[] c = new int[n << 1];
        int idx = -1;
        for (int i = 0; i < n; ++i) {
            int k = in.nextInt();
            a[i] = k > b ? 1 : -1;
            if (k == b) {
                a[i] = 0;
                idx = i;
            }
        }
        int cnt = 1, sum = 0;
        for (int i = idx - 1; i >= 0; --i) {
            sum += a[i];
            ++c[n + sum];
            if (sum == 0)
                ++cnt;
        }
        sum = 0;
        for (int i = idx + 1; i < n; ++i) {
            sum += a[i];
            cnt += c[n - sum];
            if (sum == 0)
                cnt++;
        }
        System.out.println(cnt);
    }
}
