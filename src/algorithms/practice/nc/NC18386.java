package algorithms.practice.nc;

import java.util.Scanner;

public class NC18386 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        char[] s = in.next().toCharArray();
        int min = Integer.MAX_VALUE, ss = 26;
        int[] a = new int[129];
        int l = 0, r = 0, n = 0;
        while (r < s.length) {
            while (r < s.length && n < ss) {
                ++a[s[r++]];
                if (a[s[r - 1]] == 1)
                    ++n;
            }
            if (n == ss)
                min = Math.min(min, r - l);
            --a[s[l++]];
            if (a[s[l - 1]] == 0)
                --n;
        }
        System.out.println(min);
    }
}