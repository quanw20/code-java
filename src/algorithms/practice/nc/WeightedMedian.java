package algorithms.practice.nc;

import java.util.Scanner;

/**
 * 带权中位数
 * 
 * 问题:
 * 有若干个排列在一条直线上的点pi，每个点上有ai个人，找出一个人使得所有人移动到这个人的位置上的总距离最小
 * 
 * 结论:
 * 人数刚刚过半(前缀和)的那个点就是目标点
 * 
 * test case:
 * 6
 * 1 3 4 7 9 11
 * 8 2 15 3 4 21
 * output:
 * 7
 */
public class WeightedMedian {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] p = new int[n];
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            p[i] = in.nextInt();
        }
        int sum = 0;
        for (int i = 0; i < n; ++i) {
            a[i] = in.nextInt();
            sum += a[i];
        }
        for (int i = 0; i < n; ++i)
            System.out.println("i: " + p[i] + "dist: " + clac(p, a, i));
        // sum /= 2;
        // for (int i = 1; i < n; ++i) {
        // a[i] += a[i - 1];
        // if (a[i] > sum) {
        // System.out.println(p[i]);
        // break;
        // }
        // }

    }

    static int clac(int[] p, int[] a, int i) {
        int ret = 0;
        for (int j = 0; j < p.length; ++j)
            ret += a[j] * Math.pow(p[j] - p[i], 2);
        return ret;
    }
}
