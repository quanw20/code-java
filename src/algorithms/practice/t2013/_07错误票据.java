package algorithms.practice.t2013;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

/**
 * 某涉密单位下发了某种票据，并要在年终全部收回。
 * 
 * 每张票据有唯一的ID号。全年所有票据的ID号是连续的，但ID的开始数码是随机选定的。
 * 
 * 因为工作人员疏忽，在录入ID号的时候发生了一处错误，造成了某个ID断号，另外一个ID重号。
 * 
 * 你的任务是通过编程，找出断号的ID和重号的ID。
 * 
 * 假设断号不可能发生在最大和最小号。
 * 
 * 输入格式
 * 
 * 要求程序首先输入一个整数N(N<100)表示后面数据行数。
 * 
 * 接着读入N行数据。
 * 
 * 每行数据长度不等，是用空格分开的若干个（不大于100个）正整数（不大于100000），请注意行内和行末可能有多余的空格，你的程序需要能处理这些空格。
 * 
 * 每个整数代表一个ID号。
 */
// 6

// 164 178 108 109 180 155 141 159 104 182 179 118 137 184 115 124 125 129 168
// 196
// 172 189 127 107 112 192 103 131 133 169 158
// 128 102 110 148 139 157 140 195 197
// 185 152 135 106 123 173 122 136 174 191 145 116 151 143 175 120 161 134 162
// 190
// 149 138 142 146 199 126 165 156 153 193 144 166 170 121 171 132 101 194 187
// 188
// 113 130 176 154 177 120 117 150 114 183 186 181 100 163 160 167 147 198 111
// 119

// 105 120
/**
 * 输入->排序->两个两个比较
 */
public class _07错误票据 {
    static String i, j;

    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        int n = in.nextInt();
        in.nextLine();
        ArrayList<String> al = new ArrayList<>();
        while (n-- != 0) {
            String line = in.nextLine();
            String[] sp = line.split("\\s");
            Arrays.stream(sp).forEach(e -> al.add(e));
        }
        al.sort(Comparator.naturalOrder());
        al.stream().reduce((a, b) -> {
            if (b.compareTo(a) == 0) {
                i = a;
            } else if (b.compareTo(a) == 2) {
                j = a;
            }
            return b;
        });

        System.out.println((Integer.parseInt(j) + 1) + " " + i);
        in.close();
    }
}
