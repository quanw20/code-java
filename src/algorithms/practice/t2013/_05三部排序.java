package algorithms.practice.t2013;

import java.util.Arrays;

/**
 * 题目描述
 * 一般的排序有许多经典算法，如快速排序、希尔排序等。
 * 但实际应用时，经常会或多或少有一些特殊的要求。
 * 我们没必要套用那些经典算法，可以根据实际情况建立更好的解法。
 * 比如，对一个整型数组中的数字进行分类排序：
 * 使得负数都靠左端，正数都靠右端，0在中部。
 * 注意问题的特点是：负数区域和正数区域内并不要求有序。
 * 可以利用这个特点通过1次线性扫描就结束战斗!!
 * 以下的程序实现了该目标。
 */
public class _05三部排序 {

    private static void sort(int[] a, int i, int j) {
        if (i >= j)
            return;
        int[] p = partition (a, i, j);
        sort(a, i, p[0] - 1);
        sort(a, p[1] + 1, j);
    }

    static int[] partition2(int[] a, int l, int r) {
        int p = a[l];
        while (l < r) {
            while (l < r && a[r] >= p)
                --r;
            a[l] = a[r];
            while (l < r && a[l] <= p)
                ++l;
            a[r] = a[l];
            a[l] = p;
        }
        return new int[] { l, r };
    }

    static int[] partition(int[] a, int i, int j) {
        int p = i, left = i, right = j;
        int pivot = a[left];
        while (p <= right) {
            if (a[p] < pivot) {
                int t = a[left];
                a[left] = a[p];
                a[p] = t;
                ++left;
                ++p;
            } else if (a[p] < pivot) {
                int t = a[right];
                a[right] = a[p];
                a[p] = t;
                --right;
            } else {
                ++p;
            }
        }
        System.out.println(left+" "+right);
        return new int[] { left, right };
    }

    public static void main(String[] args) {
        int[] a = { 4, 0, 2, 5, 1, 1, 5 - 4, -1, -5, -4, -1, -5, 0, -2, 0 };
        sort(a, 0, a.length - 1);

        System.out.println(Arrays.toString(a));
    }

    private static int[] partition1(int[] arr, int begin, int end) {
        int pivot = arr[begin];
        int left = begin + 1;
        int mid = begin + 1;// 相等指针
        int right = end;
        while (left <= right) {
            if (arr[left] < pivot) {
                left++;
            } else if (arr[left] > pivot) {
                swap(arr, left, right);
                right--;
            } else {
                mid = left;
                left++;
                break;
            }
        }
        while (left <= right) {
            if (arr[left] < pivot) {// 小于主元时，交换位置
                swap(arr, left, mid);
                mid++;
                left++;
            } else if (arr[left] > pivot) {
                swap(arr, left, right);
                right--;
            } else {// 等于主元时，left++
                left++;
            }
        }
        if (arr[mid] != pivot) {// 假如不存在等于主元的元素
            swap(arr, begin, right);
            int[] position = { right, right };
            return position;
        } else { // 若存在与主元相等的元素，主元依旧在begin位,此时e指向第一个等于主元位，bigger指向最后一个等于主元位
            swap(arr, begin, mid - 1);
            mid--;
            int[] position = { mid, right };// 记录两个端点位置
            return position;
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
