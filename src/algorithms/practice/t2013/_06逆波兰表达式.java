package algorithms.practice.t2013;

import java.util.Stack;

/**
 * 前缀表达式 -> 波兰表达式
 * 后缀表达式 -> 逆波兰表达式
 */
/**
 * 
 * 正常的表达式称为中缀表达式，运算符在中间，主要是给人阅读的，机器求解并不方便。例如：3 + 5 * (2 + 6) -
 * 1而且，常常需要用括号来改变运算次序。相反，如果使用逆波兰表达式（前缀表达式）表示，上面的算式则表示为：- + 3 * 5 + 2 6 1
 * 不再需要括号，机器可以用递归的方法很方便地求解。
 * 为了简便，我们假设：
 * 1.只有 + - * 三种运算符
 * 2. 每个运算数都是一个小于10的非负整数
 * 下面的程序对一个逆波兰表示串进行求值。
 * 其返回值为一个数组：其中第一元素表示求值结果，第二个元素表示它已解析的字符数。
 * static int[] evaluate(String x)
 * {
 * if(x.length()==0) return new int[] {0,0};
 * 
 * char c = x.charAt(0);
 * if(c>='0' && c<='9') return new int[] {c-'0',1};
 * 
 * int[] v1 = evaluate(x.substring(1));
 * int[] v2 = __________________________________________; //填空位置
 * 
 * int v = Integer.MAX_VALUE;
 * if(c=='+') v = v1[0] + v2[0];
 * if(c=='*') v = v1[0] * v2[0];
 * if(c=='-') v = v1[0] - v2[0];
 * 
 * return new int[] {v,1+v1[1]+v2[1]};
 * }
 * 
 */
public class _06逆波兰表达式 {
    /**
     * 将中缀表达式转换为前缀表达式
     * 
     * <pre>
     * 转换步骤如下:
     * 1. 初始化两个栈:运算符栈s1，储存中间结果的栈s2
     * 2. 从右至左扫描中缀表达式
     * 3. 遇到操作数时，将其压入s2
     * 4. 遇到运算符时，比较其与s1栈顶运算符的优先级
     *      4.1 如果s1为空，或栈顶运算符为右括号“)”，则直接将此运算符入栈
     *      4.2 否则，若优先级比栈顶运算符的较高或相等，也将运算符压入s1
     *      4.3 否则，将s1栈顶的运算符弹出并压入到s2中，再次转到(4-1)与s1中新的栈顶运算符相比较
     * 5. 遇到括号时
     *      5.1 如果是右括号“)”，则直接压入s1
     *      5.2 如果是左括号“(”，则依次弹出S1栈顶的运算符，并压入S2，直到遇到右括号为止，此时将这一对括号丢弃
     * 6. 重复步骤2至5，直到表达式的最左边
     * 7. 将s1中剩余的运算符依次弹出并压入s2
     * 8. 依次弹出s2中的元素并输出，结果即为中缀表达式对应的前缀表达式
     * </pre>
     * 
     * @param infix 中缀表达式
     * @return 前缀表达式(波兰表达式)
     */
    public static String inToPre(String infix) {
        Stack<Character> s1 = new Stack<>();
        Stack<Character> s2 = new Stack<>();
        for (int i = infix.length() - 1; i >= 0; --i) {
            char c = infix.charAt(i);
            if ('0' <= c && c <= '9') {
                s2.push(c);
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                if (s1.empty() || s1.peek() == ')'
                        || c == '*' || c == '/'
                        || (c == '+' || c == '-') && (s1.peek() == '+' || s1.peek() == '-')) {
                    s1.push(c);
                } else {
                    Character c1 = s1.pop();
                    s2.push(c1);
                    continue;
                }
            } else if (c == ')') {
                s1.push(c);
            } else if (c == '(') {
                Character pop = null;
                while ((pop = s1.pop()) != ')') {
                    s2.push(pop);
                }
            }
        }
        while (!s1.isEmpty()) {
            s2.push(s1.pop());
        }
        StringBuilder sb = new StringBuilder();
        while (!s2.isEmpty()) {
            sb.append(s2.pop()).append(' ');
        }
        return sb.toString();
    }

    /**
     * 从右至左扫描表达式，遇到数字时，将数字压入堆栈，遇到运算符时，弹出栈顶的两个数，用运算符对它们做相应的计算（栈顶元素 op
     * 次顶元素），并将结果入栈；重复上述过程直到表达式最左端，最后运算得出的值即为表达式的结果
     * 
     * @param prefix
     * @return
     */
    public static int computePrefix(String prefix) {
        Stack<Integer> s = new Stack<>();
        for (int i = prefix.length() - 1; i >= 0; --i) {
            char c = prefix.charAt(i);
            if ('0' <= c && c <= '9') {
                s.push(c - 48);
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                int a, b;
                switch (c) {
                    case '+':
                        a = s.pop();
                        b = s.pop();
                        s.push(a + b);
                        break;
                    case '-':
                        a = s.pop();
                        b = s.pop();
                        s.push(a - b);
                        break;
                    case '*':
                        a = s.pop();
                        b = s.pop();
                        s.push(a * b);
                        break;
                    case '/':
                        a = s.pop();
                        b = s.pop();
                        if (b == 0) {
                            System.out.println("devide by 0");
                            return -1;
                        }
                        s.push(a / b);
                        break;

                    default:
                        break;
                }
            }
        }
        return s.pop();
    }

    static int priority(char c) {
        switch (c) {
            case '*':
                return 2;
            case '/':
                return 2;
            case '+':
                return 1;
            case '-':
                return 1;
        }
        return 3;
    }

    /**
     * 将中缀表达式转换为后缀表达式
     * 
     * <pre>
     * 与转换为前缀表达式相似，步骤如下：
     * 
     * 1. 初始化两个栈：运算符栈s1和储存中间结果的栈s2；
     * 2. 从左至右扫描中缀表达式；
     * 3. 遇到操作数时，将其压s2；
     * 4. 遇到运算符时，比较其与s1栈顶运算符的优先级：
     *      4.1 如果s1为空，或栈顶运算符为左括号“(”，则直接将此运算符入栈；
     *      4.2 否则，若优先级比栈顶运算符的高，也将运算符压入s1（注意转换为前缀表达式时是优先级较高或相同，而这里则不包括相同的情况）；
     *      4.3 否则，将s1栈顶的运算符弹出并压入到s2中，再次转到(4-1)与s1中新的栈顶运算符相比较；
     * 5. 遇到括号时：
     *      5.1 如果是左括号“(”，则直接压入s1；
     *      5.2 如果是右括号“)”，则依次弹出s1栈顶的运算符，并压入s2，直到遇到左括号为止，此时将这一对括号丢弃；
     * 6. 重复步骤2至5，直到表达式的最右边；
     * 7. 将s1中剩余的运算符依次弹出并压入s2；
     * 8. 依次弹出s2中的元素并输出，结果的逆序即为中缀表达式对应的后缀表达式（转换为前缀表达式时不用逆序）
     * </pre>
     * 
     * @param infix 中缀表达式
     * @return 后缀表达式
     */
    public static String inToSurffix(String infix) {
        Stack<Character> s1 = new Stack<>();
        Stack<Character> s2 = new Stack<>();
        for (int i = 0; i < infix.length(); i++) {
            char c = infix.charAt(i);
            if ('0' <= c && c <= '9') {
                s2.push(c);
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                if (s1.empty() || s1.peek() == '('
                        || (c == '*' || c == '/') && (s1.peek() == '+' || s1.peek() == '-')) {
                    s1.push(c);

                } else {
                    i--;// 下一个也是infix[i]
                    s2.push(s1.pop());
                    continue;
                }
            } else if (c == '(') {
                s1.push(c);
            } else if (c == ')') {
                Character pop = null;
                while ((pop = s1.pop()) != '(') {
                    s2.push(pop);
                }
            }
        }
        while (!s1.empty()) {
            s2.push(s1.pop());
        }
        StringBuilder sb = new StringBuilder();
        s2.forEach(e -> sb.append(e).append(' '));
        return sb.toString();
    }

    /**
     * 
     * 从左至右扫描表达式，遇到数字时，将数字压入堆栈，遇到运算符时，弹出栈顶的两个数，用运算符对它们做相应的计算（次顶元素 op
     * 栈顶元素），并将结果入栈；重复上述过程直到表达式最右端，最后运算得出的值即为表达式的结果
     * 
     * @param surffix 后缀表达式
     * @return 值
     */
    public static int compuateSuffix(String surffix) {
        Stack<Integer> s = new Stack<>();
        for (int i = 0, l = surffix.length(); i < l; i++) {
            char c = surffix.charAt(i);
            if ('0' <= c && c <= '9') {
                s.push(c - 48);
            } else if (c == '+' || c == '-' || c == '*' || c == '/') {
                Integer a = s.pop();
                Integer b = s.pop();// b在前
                switch (c) {
                    case '+':
                        s.push(b + a);
                        break;
                    case '-':
                        s.push(b - a);
                        break;
                    case '*':
                        s.push(b * a);
                        break;
                    case '/':
                        s.push(b / a);
                        break;
                    default:
                        break;
                }
            }
        }
        return s.pop();
    }

    public static void main(String[] args) {
        String infix = "(1+((2+3)*4))/3-5";
        String inToPre = inToPre(infix);
        String surffix = inToSurffix(infix);
        System.out.println(inToPre);
        System.out.println(surffix);
        int a = computePrefix(inToPre);
        int b = compuateSuffix(surffix);
        System.out.println(a);
        System.out.println(b);
    }
}
