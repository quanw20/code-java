package algorithms.practice.t2013;

/**
 * 4位数字
 * 1958->8561
 * 倒了两个,赔200多,赚800多
 * 净赚558
 * 求赔钱那个的正确价格
 */
public class _04颠倒的价格牌 {
    public static void main(String[] args) {
        for (int j = 1001; j < 10000; j++) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(j);
            String s1 = sb2.reverse().toString().replace('9', '-').replace('6', '9').replace('-', '6');
            int i1 = Integer.parseInt(s1);
            int a = i1 - j;
            if (-300 < a && a < -200)
                for (int i = 1001; i < 10000; i++) {
                    StringBuilder sb1 = new StringBuilder();
                    sb1.append(i);
                    String s = sb1.reverse().toString().replace('9', '-').replace('6', '9').replace('-', '6');
                    int i2 = Integer.parseInt(s);
                    int b = i2 - i;
                    if (800 < b && b < 900 && a + b == 558)
                        System.out.println(j + ":" + i1 + "\t" + i + ":" + i2);
                }
        }
    }
}
// 9088 -> 8806