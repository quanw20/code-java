package algorithms.practice.t2013;

import java.io.BufferedInputStream;
import java.util.Scanner;

/**
 * 剪格子
 * 问题描述
 * 如下图所示，3 x 3 的格子中填写了一些整数。
 * 
 * 我们沿着图中的星号线剪开，得到两个部分，每个部分的数字和都是60。
 * 本题的要求就是请你编程判定：对给定的m x n 的格子中的整数，是否可以分割为两个部分，使得这两个区域的数字和相等。
 * 如果存在多种解答，请输出包含左上角格子的那个区域包含的格子的最小数目。
 * 如果无法分割，则输出 0。
 * 
 * 输入格式
 * 程序先读入两个整数 m n 用空格分割 (m,n<10)。
 * 表示表格的宽度和高度。
 * 接下来是n行，每行m个正整数，用空格分开。每个整数不大于10000。
 * 输出格式
 * 输出一个整数，表示在所有解中，包含左上角的分割区可能包含的最小的格子数目。
 * 样例1.in
 * 3 3
 * 10 1  52
 * 20 30 1
 * 1  2  3
 * 样例1.out
 * 3
 * 样例2.in
 * 4 3
 * 1 1  1  1
 * 1 30 80 2
 * 1 1  1  100
 * 样例2.out
 * 10
 * 
 * 题目分析
 * 题目概述：将输入的矩阵分成两半，使两部分的累和相同，
 * 无解时输出0，如果有多组解，输出左边元素个数最少的一组解
 * 
 * 思路: 先计算一半的大小->dfs+回溯 找到==一半 ->和min比较
 */
public class _09剪格子 {
    static int sum = 0;
    static int[][] a;// graph
    static boolean[][] b; // visited
    static int m;
    static int n;
    static int min = (1 << 31) - 1;

    /**
     * 
     * @param i 0~m-1
     * @param j 0~n-1
     * @param k
     * @param l
     */
    private static void dfs(int i, int j, int k, int l) {
        if (k == sum) {
            min = l < min ? l : min;
            return;
        }
        if (k > sum)
            return;
        if (i < m - 1 && !b[i + 1][j]) {
            b[i + 1][j] = true;
            dfs(i + 1, j, k + a[i][j], l + 1);
            b[i + 1][j] = false;
        }
        if (j < n - 1 && !b[i][j + 1]) {
            b[i][j + 1] = true;
            dfs(i, j + 1, k + a[i][j], l + 1);
            b[i][j + 1] = false;
        }
        if (i - 1 >= 0 && !b[i - 1][j]) {
            b[i - 1][j] = true;
            dfs(i - 1, j, k + a[i][j], l + 1);
            b[i - 1][j] = false;
        }
        if (j - 1 >= 0 && !b[i][j - 1]) {
            b[i][j - 1] = true;
            dfs(i, j - 1, k + a[i][j], l + 1);
            b[i][j - 1] = false;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        m = in.nextInt();
        n = in.nextInt();
        a = new int[m][n];
        b = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = in.nextInt();
                sum += a[i][j];
            }
        }
        sum /= 2;

        dfs(0, 0, 0, 0);
        System.out.println(min);

        in.close();
    }

}
