package algorithms.practice.t2013;

import java.io.BufferedInputStream;
import java.util.Scanner;

/**
 * _10大臣的旅费
 * 
 * 任何一个大城市都能从首都直接或者通过其他大城市间接到达。
 * 同时，如果不重复经过大城市，从首都到达每个大城市的方案都是唯一的。
 * J是T国重要大臣，他巡查于各大城市之间，体察民情。所以，
 * 从一个城市马不停蹄地到另一个城市成了J最常做的事情。他有一个钱袋，用于存放往来城市间的路费。
 * 聪明的J发现，如果不在某个城市停下来修整，在连续行进过程中，他所花的路费与他已走过的距离有关，
 * 在走第x千米到第x+1千米这一千米中（x是整数），他花费的路费是x+10这么多。也就是说走1千米花费11，走2千米要花费23。
 * J大臣想知道：他从某一个城市出发，中间不休息，到达另一个城市，所有可能花费的路费中最多是多少呢？
 * 
 * 输入格式
 * 输入的第一行包含一个整数n，表示包括首都在内的T王国的城市数
 * 城市从1开始依次编号，1号城市为首都。
 * 接下来n-1行，描述T国的高速路（T国的高速路一定是n-1条）
 * 每行三个整数Pi, Qi, Di，表示城市Pi和城市Qi之间有一条高速路，长度为Di千米。
 * 
 * 输出格式
 * 输出一个整数，表示大臣J最多花费的路费是多少。
 * 
 * 样例输入1
 * 5
 * 1 2 2
 * 1 3 1
 * 2 4 5
 * 2 5 4
 * 样例输出1
 * 135
 * 输出格式
 * 大臣J从城市4->2->5要花费135的路费。
 * 
 * 树的直径
 */
public class _10大臣的旅费 {
    static int n;
    static int[][] a;// 邻接矩阵
    static int[] d;
    static int[] t;
    static int l = 0;// 最远距离

    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedInputStream(System.in));
        n = in.nextInt();
        a = new int[n + 1][n + 1];// 从1开始
        d = new int[n + 1];
        t = new int[n + 1];
        for (int k = 0; k < n - 1; ++k) {
            int i = 0, j = 0;
            i = in.nextInt();
            j = in.nextInt();
            a[i][j] = a[j][i] = in.nextInt();
        }
        // dfs(1, 0);
        for (int i = 1; i <= n; i++) {
            d(i, new boolean[n + 1], 0);

        }
        int dis = dis(l);
        System.out.println(dis);
        in.close();
    }

    private static int dis(int n) {
        return 11 * n + (n * n - n) / 2;
    }

    static void dfs(int u, int fa/* 父节点 */) {
        d[u] = t[u] = 0;
        for (int v : a[u]) {
            if (v == fa)
                continue;
            dfs(v, u);
            int g = d[v] + 1;
            if (g > d[u]) {
                t[u] = d[u];
                d[u] = g;
            } else if (g > t[u])
                t[u] = g;
        }
        l = Math.max(l, d[u] + t[u]);
    }

    static void d(int u, boolean[] vis, int cost) {
        vis[u] = true;
        for (int v = 0; v < a[u].length; v++) {
            if (a[u][v] != 0 && !vis[v]) {
                d(v, vis, cost + a[u][v]);
                vis[v] = false;
            }
        }
        l = Math.max(l, cost);
        System.out.println(l);
    }

}