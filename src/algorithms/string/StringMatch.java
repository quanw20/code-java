package algorithms.string;

/**
 * StringMatch
 */
public class StringMatch {
    /**
     * 朴素匹配法
     * O(m*n)
     * 
     * @param a
     * @param b
     * @return
     */
    public static int simpleMatch(String a, String b) {
        char[] c1 = a.toCharArray();
        char[] c2 = b.toCharArray();
        for (int i = 0; i < c1.length; i++) {
            if (c1[i] == c2[0]) {
                int j = i, k = 0;
                for (; j < c1.length && k < c2.length; j++, k++) {
                    if (c1[j] != c2[k])
                        break;
                }
                if (k == c2.length)
                    return i;
            }
        }
        return -1;
    }

    /**
     * 
     * String -> HashCode -> Compare
     * 
     * @param a
     * @param b
     * @return
     */
    public static int hashMatch(String a, String b) {
        long hb = HashFunc.strHash(b);
        char[] c = a.toCharArray();
        char[] t = new char[b.length()];
        for (int i = 0; i < c.length - b.length(); i++) {
            System.arraycopy(c, i, t, 0, b.length());
            long ha = HashFunc.strHash(new String(t));
            if (ha == hb)
                return i;
        }
        return -1;
    }

    /**
     * /**
     * 
     * <pre>
     * babab
     * 
     * 后缀:    b   ba  bab    baba
     * 前缀:    b   ab  bab    abab
     * 最长匹配: 1   1    3      3
     * 
     * </pre>
     * 
     * next数组:
     * next[j]=k -> j失陪,j回溯到k
     * 
     * s i 不回溯
     * p j 回溯
     * 
     * 周期性 j%(j-k)==0
     * 周期T=j/(j-k)
     * 
     * @param str
     * @param pat
     * @return
     */
    public static int kmp(String str, String pat) {
        // 初始胡next数组
        int[] next = new int[pat.length()];
        next[0] = -1;

        for (int k = 0, j = 1; j < pat.length() - 1; k++, j++) {
            // 相同: 后移一步,给next数组赋值
            // 不同: k回溯, 都后移一步, 给next数组赋值
            if (pat.charAt(k) != pat.charAt(j)) {
                k = next[k];
            }
            next[j + 1] = k + 1;
        }

        // System.out.println(Arrays.toString(next));
        int i = 0, j = 0;
        for (; i < str.length() && j < pat.length(); i++, j++) {
            // 如果不相等,回溯
            if (str.charAt(i) != pat.charAt(j)) {
                j = next[j];
            }
        }
        if (pat.length() == j)
            return i - j;

        return -1;
    }
}
