package algorithms.string;

/**
 * kmp算法
 * 
 * <pre>
 * 
 * babab
 * 
 * 后缀:    b   ba  bab    baba
 * 前缀:    b   ab  bab    abab
 * 最长匹配: 1   1    3      3
 * 
 * 
 * </pre>
 * 
 * next数组:
 * next[j]=k -> j失陪,j回溯到k
 * 
 * s i 不回溯
 * p j 回溯
 */
public class KMP {
    int[] getNext(String t) {
        int[] next = new int[t.length()];
        next[0] = -1;
        for (int i = 1, j = -1; i < t.length(); ++i) {
            while (j > -1 && t.charAt(j + 1) != t.charAt(i))
                j = next[j];
            if (t.charAt(j + 1) == t.charAt(i))
                next[i] = ++j;
        }
        return next;
    }

    int kmp(String s, String t) {
        int[] next = new int[t.length()];
        next[0] = -1;
        for (int i = 1, j = -1; i < t.length(); ++i) {
            while (j > -1 && t.charAt(j + 1) != t.charAt(i))
                j = next[j];
            if (t.charAt(j + 1) == t.charAt(i))
                next[i] = ++j;
        }
        for (int i = 0, j = -1; i < s.length(); ++i) {
            while (j > -1 && s.charAt(i) != t.charAt(j + 1))
                j = next[j];
            if (s.charAt(i) == t.charAt(j + 1))
                ++j;
            if (j + 1 == t.length())
                return i - j;
        }
        return -1;
    }

    public static void main(String[] args) {
        String s = "acababc";
        String t = "ababc";
        int kmp = new KMP().kmp(s, t);
        System.out.println(kmp);
    }
}
