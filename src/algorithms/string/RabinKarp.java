package algorithms.string;

import java.math.BigInteger;
import java.util.Random;

/**
 * 后缀数组(suffix array): 排名和原下标的映射 sa[1]=2;
 * rank数组: 给定后缀的下标,返回字典序 rk[2]=1; rk[sa[i]]=i
 * 字串: 一定是某个后缀的前缀
 * 
 * <pre>
 * 
 * 源字符串: ASDFGH
 * 
 * 字符串  下标
 * ASDFGH 0
 *  SDFGH 1
 *   DFGH 2
 *    FGH 3
 *     GH 4
 *      H 5
 *
 * 得到元素 0 1 2 3 4 5
 * 
 * 按字典序排列 0 2 3 4 5 1
 * 
 * 将这些元素到数组(后缀数组sa)中 :
 * 
 * element: 0 2 3 4 5 1
 *   index: 0 1 2 3 4 5
 * 
 * </pre>
 */
class RabinKarp {
    private String pat; // 模式
    private long patHash;// 模式字符串的哈希值
    private int m; // pat.length()
    private long q; // 一个素数
    private int R; // 基数
    private long RM; // 最高位 R^m

    public RabinKarp(String pat) {
        this.pat = pat;
        R = 256;
        m = pat.length();
        q = longRandomPrime();

        RM = 1;
        for (int i = 1; i <= m - 1; i++)
            RM = (R * RM) % q;
        patHash = hash(pat, m);
    }

    /**
     * 计算一个字符串的哈希值
     * 
     * @param key 字符串
     * @param m   长度
     * @return
     */
    private long hash(String key, int m) {
        long h = 0;
        for (int j = 0; j < m; j++)
            h = (R * h + key.charAt(j)) % q;
        return h;
    }

    /**
     * 朴素匹配法验证
     * 
     * @param txt
     * @param i
     * @return
     */
    private boolean check(String txt, int i) {
        for (int j = 0; j < m; j++)
            if (pat.charAt(j) != txt.charAt(i + j))
                return false;
        return true;
    }

    public int search(String txt) {
        int n = txt.length();
        if (n < m)
            return n;
        long txtHash = hash(txt, m);

        if ((patHash == txtHash) && check(txt, 0))
            return 0;

        // 滚动Hash
        for (int i = m; i < n; i++) {// i为用来匹配的最后一个字符的下标
            // 去掉最高位
            txtHash = (txtHash + q - RM * txt.charAt(i - m) % q) % q;
            txtHash = (txtHash * R + txt.charAt(i)) % q;

            int offset = i - m + 1;
            if ((patHash == txtHash) && check(txt, offset))
                return offset;
        }
        return n;
    }

    private static long longRandomPrime() {
        BigInteger prime = BigInteger.probablePrime(31, new Random());
        return prime.longValue();
    }
}
