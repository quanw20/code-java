package algorithms.array;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * 数组/列表 基本操作
 */

import org.junit.jupiter.api.Test;

public class ArrayListBase {
  @Test
  void testArray() {
    int[] intArr = new int[Integer.MAX_VALUE / 10];
    int[] intArr2 = new int[] { 1, 2, 3, 4 };
    Random random = new Random(1L);
    for (int i = 0; i < intArr.length; ++i) {
      intArr[i] = random.nextInt(10);
    }
    int[] clone = intArr.clone();

    long t1 = System.nanoTime();
    int sum = IntStream.of(clone).sum();
    System.out.println(sum + " : " + (System.nanoTime() - t1));

    t1 = System.nanoTime();
    int sum2 = IntStream.of(clone).parallel().sum();
    System.out.println(sum2 + " : " + (System.nanoTime() - t1));

    t1 = System.nanoTime();
    int sum3 = 0;
    for (int i = 0, l = clone.length; i < l; ++i) {
      sum3 += clone[i];
    }
    System.out.println(sum3 + " : " + (System.nanoTime() - t1));

    t1 = System.nanoTime();
    int sum4 = 0;
    for (int i : clone) {
      sum4 += i;
    }
    System.out.println(sum4 + " : " + (System.nanoTime() - t1));
  }

  @Test
  void testArrayList(){
    ArrayList<Integer> al = new ArrayList<>();
    Random random = new Random(1L);
    for (int i = 0; i < 10000000; i++) {
      al.add(random.nextInt(10));
    }
  }
}
