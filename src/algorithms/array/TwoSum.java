package algorithms.array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.IntStream;
/**
 * 
 */
public class TwoSum {
  public int[] twoSum(int[] nums, int target) {
    HashMap<Integer, Integer> map = new HashMap<>();// 值-位置
    for (int i = 0; i < nums.length; ++i) {
      int sub = target - nums[i];// 另一个
      if(map.containsKey(sub)){
        return new int[]{i,map.get(sub)};
      }
      map.put(nums[i],i);
    }
    return null;
  }

  public static void main(String[] args) {

  }
}
