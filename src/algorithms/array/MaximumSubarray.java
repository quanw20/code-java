package algorithms.array;

import java.util.stream.IntStream;

/**
 * Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
 * Output: 6
 * Explanation: The subarray [4,-1,2,1] has the largest sum 6.
 * 
 * 
 * 
 */
public class MaximumSubarray {
  public int maxSubArray(int[] nums) {
    if (nums.length == 1)
      return nums[0];
    int[] a = new int[nums.length];
    a[0] = nums[0];
    for (int i = 1; i < nums.length; ++i) {
      a[i] = a[i - 1] > 0 ? a[i - 1] + nums[i] : nums[i];
    }
    return IntStream.of(a).parallel().max().getAsInt();
  }
/**
 * 分治法
 * 最大子数组=max（左边，右边，以中间向两边扩散的数组）
 * @param nums
 * @return
 */
  public int maxSubArray2(int[] nums) {
    int len = nums.length;
    if (len == 0)
      return 0;
    return maxSubArraySum(nums, 0, len - 1);
  }

  private int maxSubArraySum(int[] nums, int l, int r) {
    if (l == r)
      return nums[l];
    int mid = l + (r - l) / 2;
    return IntStream.of(
        maxSubArraySum(nums, l, mid),
        maxSubArraySum(nums, mid + 1, r),
        maxCrossingSum(nums, l, mid, r))
        .max().getAsInt();
  }

  private int maxCrossingSum(int[] nums, int l, int mid, int r) {
    int leftSum, rightSum, tmp = 0;
    leftSum = rightSum = Integer.MIN_VALUE;
    for (int i = mid; i >= l; --i) {
      tmp += nums[i];
      if (tmp > leftSum)
        leftSum = tmp;
    }
    tmp = 0;
    for (int i = mid + 1; i <= r; ++i) {
      tmp += nums[i];
      if (tmp > rightSum)
        rightSum = tmp;
    }
    System.out.println(leftSum + rightSum);
    return leftSum + rightSum;
  }

  public static void main(String[] args) {
    int[] nums = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
    System.out.println(new MaximumSubarray().maxSubArray2(nums));
  }
}
