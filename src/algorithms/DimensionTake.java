package algorithms;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 尺取/双指针/追逐法
 * 
 * 连续的区间
 */
public class DimensionTake {
    /**
     * test case
     * 5
     * 11
     * 1 2 3 4 5
     * 
     * @throws IOException
     */
    static void subsequence() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int s = in.nextInt();
        int a[] = new int[n];
        for (int i = 0; i < n; ++i)
            a[i] = in.nextInt();

        int res = n + 1, sum = 0;
        int i = 0, j = 0;
        for (;;) {
            // 找到sum>=s的区间[i,j]
            while (j < n && sum < s)
                sum += a[j++];
            if (sum < s)// 找不到
                break;
            res = Math.min(j - i, res);
            sum -= a[i++];// 左端点右移
        }
        System.out.println(res == n + 1 ? 0 : res);
    }

    /**
     * Jessica's Reading Problem 3320
     * If she wants to pass it, she has to master all ideas included in a very thick
     * text book. thus some ideas are covered more than once. Jessica
     * think if she managed to read each idea at least once, she can pass the exam.
     * She decides to read only one contiguous part of the book which contains all
     * ideas covered by the entire book. And of course, the sub-book should be as
     * thin as possible.
     * 
     * in:
     * 5
     * 1 8 8 8 1
     * out:
     * 2
     * 
     * @throws IOException
     */
    static void JessicasReadingProblem() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int[] a = new int[n];
        int[] marked = new int[1001];
        for (int i = 0; i < n; ++i) {
            a[i] = in.nextInt();
        }

        n = (int) Arrays.stream(a).distinct().count();

        int s = 0, t = 0, num = 0, res = n + 1;
        for (;;) {
            while (t < a.length && num < n)
                if (marked[a[t++]]++ == 0) {// 有新页
                    ++num;
                }
            if (num < n)// 页数再也不够
                break;
            res = Math.min(res, t - s);
            if (--marked[a[s++]] == 0) {
                --num;
            }
        }
        System.out.println(res);

    }

    static void dualptr_temp() {
        int n = 0;
        for (int i = 0, j = 0; i < n; ++i) {
            while (j == i && check(i, j))
                ++j;
        }
    }

    private static boolean check(int i, int j) {
        return false;
    }

    public static void main(String[] args) throws IOException {
        // subsequence();
        JessicasReadingProblem();
    }
}