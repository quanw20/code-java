package algorithms;

import java.util.*;

import algorithms.math.MathProblems;

/**
 * 插入排序(将元素插入到有序序列中)
 * 
 * 希尔排序(缩小增量排序)
 * 
 * 汉诺塔
 * 
 * 二分查找
 * 
 * 上楼梯 1 2 3
 * 
 * 快速排序
 * 
 * 归并排序
 * 
 * 将乱序数组中奇数放前边偶数放后边, 并排序
 * 
 * 在乱序数组中找第K小的数值
 * 
 * 从数组中找出个数超过一半的数
 * 
 * 从乱序数组(从0开始)中找到最小可用id
 * 
 * 将两有序数组合并并排序
 * 
 * 用coins凑出n
 * 
 * 子集生成:
 * 
 * 全排列 -- 回溯
 * 
 * 数独
 * 
 * 若干个元素的和等于a
 * 
 * 水洼
 * 
 * n皇后
 * 
 * 素数环
 * 
 * 困难的串:
 * 
 */
/**
 * # 递归
 * 
 * 找重复<br/>
 * 1. 找到一种划分方法<br/>
 * 2. 找到递推公式或等价转换<br/>
 * 
 * 找变化量<br/>
 * 1. 变化量通常要作为参数<br/>
 * 
 * 找递归出口<br/>
 * 
 * # 分治法
 * 
 * 分解devide: 将原问题划分为子问题<br/>
 * 解决conquer: 递归的解决子问题,若问题足够小,则直接解决<br/>
 * 合并combine:将子问题的结果合并成原问题的解
 */
public final class Recursive {
	/**
	 * 输入底数和指数计算幂
	 * 
	 * @param m 底数
	 * @param n 指数
	 * @return m^n
	 */
	public static int factorial(int m, int n) {
		if (n == 1) // 递归出口
			return m;
		return m * factorial(m, n - 1); // 递推关系
	}

	/**
	 * 插入排序(将元素插入到有序序列中)
	 * 
	 * 平均效率低,但在元素基本有序时,它很快
	 * 
	 * O(n^2)
	 * 
	 * @param a 待排数组
	 * @param s 开始下标
	 * @param k 数组长度
	 */
	public static void insertSort(int[] a, int s, int k) {
		if (k == 0)
			return;
		insertSort(a, s, k - 1);
		int x = a[k];// 待插入的元素
		int i = k - 1;// 有序元素的下标
		while (i > s - 1 && a[i] > x) {// 将大于x的元素后移
			a[i + 1] = a[i];
			--i;
		}
		a[i + 1] = x;
	}

	/**
	 * 希尔排序(缩小增量排序)
	 * O(n^1.3)
	 * 
	 * @param a 待排数组
	 */
	public static void shellSort(int[] a) {
		// 不断缩小增量interval
		for (int interval = a.length >> 1; interval > 0; interval >>= 1)
			// 增量为1的插入排序
			for (int i = interval; i < a.length; i++) {
				int x = a[i];
				int j = i - interval;
				while (j > -1 && x < a[j]) {
					a[j + interval] = a[j];
					j -= interval;
				}
				a[j + interval] = x;
			}
	}

	/**
	 * 汉诺塔
	 * 
	 * @param a 起始柱
	 * @param b 辅助柱
	 * @param c 目标柱
	 * @param n 移动的次数
	 */
	public static void hannoi(char a, char b, char c, int n) {
		if (n == 1) {// 只有一个盘的情况
			System.out.println(a + " -> " + c);
			return;
		}
		hannoi(a, c, b, n - 1);// 把前n-1个从a->b
		System.out.println(a + " -> " + c);// 最后一个从a->c
		hannoi(b, a, c, n - 1);// 最后把前n-1个从b-c
	}

	/**
	 * 二分查找
	 * 
	 * @param a
	 * @param lo
	 * @param hi
	 * @param key
	 * @return
	 */
	public static int binarySearchR(int[] a, int lo, int hi, int key) {
		if (lo > hi)
			return -1;
		int mid = lo + ((hi - lo) >> 1);// (lo+hi)>>>1 //防溢出
		if (a[mid] > key) { // 剪枝
			return binarySearchR(a, lo, mid - 1, key);
		} else if (a[mid] < key) {
			return binarySearchR(a, mid + 1, hi, key);
		} else {
			return mid;
		}
	}

	/**
	 * 上楼梯 1 2 3
	 * 
	 * @param n
	 * @return
	 */
	public static int go(int n) {
		if (n == 0)
			return 1;
		if (n < 0)
			return 0;
		return go(n - 1) + go(n - 2) + go(n - 3);
	}

	/**
	 * 
	 * @param a
	 * @param lo
	 * @param hi
	 * @return
	 */
	public static int findMinByBinary(int[] a, int lo, int hi) {
		// 没有旋转
		if (a[lo] < a[hi])
			return a[lo];
		// 最后剩两个
		if (hi - lo == 1)
			return a[hi];
		int mid = lo + ((hi - lo) >> 1);// 注意运算符优先级
		if (a[lo] < a[mid]) {// 左边有序,右边无序,在右边
			return findMinByBinary(a, mid, hi);
		} else if (a[lo] > a[mid]) {
			return findMinByBinary(a, lo, mid);
		}
		return a[hi];
	}

	/**
	 * 快速排序
	 * 
	 * O(nlogn)
	 * 
	 * @param a
	 * @param left
	 * @param right
	 */
	public static void quickSort(int[] a, int left, int right) {
		if (left >= right)
			return;
		int mid = partition(a, left, right);
		quickSort(a, left, mid - 1);
		quickSort(a, mid + 1, right);
	}

	/**
	 * 分区算法
	 * 
	 * @param a
	 * @param left
	 * @param right
	 * @return 下标
	 */
	private static int partition(int[] a, int left, int right) {
		int pivot = a[left];// 取区间一(left)为pivot,left空
		while (left < right) {
			while (left < right && a[right] >= pivot)
				right--;
			a[left] = a[right];// 取right填left,right空
			while (left < right && a[left] <= pivot)
				left++;
			a[right] = a[left];// 取left填right,left空
			a[left] = pivot;// pivot回填
		}
		return left;// pivot在分区后的下标
	}

	/**
	 * 将乱序数组中奇数放前边偶数放后边, 并排序
	 * 
	 * @param a
	 * @return
	 */
	public static int[] sortOddAndEven(int[] a) {
		int l = 0, r = a.length - 1;
		while (l < r) {
			while (l < r && (a[l] & 1) == 1)// 奇数
				l++;
			while (l < r && (a[r] & 1) == 0)// 偶数
				r--;
			if (l < r) {
				int t = a[l];
				a[l] = a[r];
				a[r] = t;
			}
		}
		insertSort(a, 0, l - 1);
		insertSort(a, l, a.length - 1);

		return a;
	}

	/**
	 * 在乱序数组中找第K小的数值
	 * 
	 * @param a
	 * @param lo
	 * @param hi
	 * @param k
	 * @return 乱序数组中找第K小的数值, -1表示没找到
	 */
	public static int findK(int[] a, int lo, int hi, int k) {
		if (k > a.length)
			return -1;
		int mid = partition(a, lo, hi); // O(n)
		int p = mid - lo + 1;// pos指的是第几个元素
		if (p == k)
			return a[mid];
		else if (p > k) // k在p左边
			return findK(a, lo, mid - 1, k);
		else
			return findK(a, mid + 1, hi, k - p);// k在右边的排名

	}

	public static int findK(int[] a, int k) {
		return findK(a, 0, a.length - 1, k);
	}

	/**
	 * 从数组中找出个数超过一半的数
	 * 
	 * @param a
	 * @return
	 */
	public static int findOverAHalf(int[] a) {
		partition(a, 0, a.length - 1);
		return a[a.length / 2];
	}

	public static int findOverAHalf2(int[] a) {
		int n = a[0];
		int cnt = 1;
		for (int i = 1; i < a.length; i++) {
			if (cnt == 0) {
				n = a[i];
				cnt = 1;
				continue;
			}
			if (a[i] != n)
				cnt--;
			else {
				cnt++;
			}

		}
		return n;
	}

	/**
	 * 从乱序数组(从0开始)中找到最小可用id
	 * 先排序再和下标进行比较<br/>
	 * O(nlogn)<br/>
	 * 
	 * @param a
	 * @return
	 */
	public static int findMinId1(int[] a) {
		Arrays.sort(a);
		int i = 0;
		while (i < a.length) {
			if (i != a[i]) {
				return i;
			}
			++i;
		}
		return i;
	}

	/**
	 * 从乱序数组中找到最小可用id<br/>
	 * 数组b的元素表示该下标是否被访问过<br/>
	 * O(n)<br/>
	 * 
	 * @param a
	 * @return
	 */
	public static int findMinId2(int[] a) {
		boolean[] b = new boolean[a.length];
		for (int i = 0; i < a.length; i++) {
			if (a[i] >= a.length)
				continue;
			b[a[i]] = true;
		}
		for (int i = 0; i < b.length; i++) {
			if (b[i] == false)
				return i;
		}
		return a.length + 1;
	}

	/**
	 * 从乱序数组中找到最小可用id<br/>
	 * 使用partition<br/>
	 * 
	 * <pre>
	 * 
	 * 0			100
	 * +------+------+
	 * ^lo    ^mid   ^hi
	 * 
	 * </pre>
	 * 
	 * if a[mid]=mid : 左边稠密,找右边 <br/>
	 * else 右边稠密,找左边
	 * 
	 * @param a
	 * @return
	 */
	public static int findMinId3(int[] a, int lo, int hi) {
		if (lo > hi)
			return hi + 1;
		int mid = partition(a, lo, hi);
		if (a[mid] == mid)
			return findMinId3(a, mid + 1, hi);// 需要+1,否则lo==hi
		else
			return findMinId3(a, lo, mid + 1);

	}

	/**
	 * 将两有序数组合并并排序<br/>
	 * 其中a数组后留有b数组的空间<br/>
	 * 不使用其他空间<br/>
	 * 
	 * @param a
	 * @param aLength a数组的元素个数
	 * @param b
	 * @return
	 */
	public static int[] mergeArray(int[] a, int aLength, int[] b) {
		int cur = a.length - 1; // 指向a数组最后一个位置
		int p1 = aLength - 1;// 指向a数组最后一个有元素的位置
		int p2 = b.length - 1;// 指向b数组最后一个位置
		while (p2 > -1) {
			if (b[p2] > a[p1]) {
				a[cur--] = b[p2--];
			} else {// b[p2]<=a[1];
				a[cur--] = a[p1--];
			}
		}
		return a;
	}

	/**
	 * 递归
	 * 
	 * 写是自顶向下
	 * 想是自底向上
	 * 
	 * 解决简单情况下的问题
	 * 推广到稍微复杂的情况下的问题
	 * 
	 * 递推次数明确,用迭代
	 * 有封闭形式,直接求解
	 */

	public static int countW(int n) {
		if (n <= 0)
			return 1;
		return collectCoins(n, new int[] { 1, 5, 10, 25 }, 3);
	}

	/**
	 * 用coins凑出n 有多少种凑法
	 * 
	 * <pre>
	 * +--+--+--+--+--+--+--+--+--+--+--+--+
	 * |  | 0| 1| 2| 3| 4| 5| 6|10|11|15|25|
	 * +--+--+--+--+--+--+--+--+--+--+--+--+
	 * | 1| 1  1  1  1  1  1  1  1  1  1  1|
	 * | 5| 1  1  1  1  1  2  2  3  3  4  6|
	 * |10| 1  1  1  1  1  2  2  4  4  6 12|
	 * |25| 1  1  1  1  1  2  2  4  4  4 13|
	 * +--+--+--+--+--+--+--+--+--+--+--+--+
	 * </pre>
	 * 
	 * @param n
	 * @param coins
	 * @param cur
	 * @return
	 */
	public static int collectCoins(int n, int[] coins, int cur) {
		if (cur <= 0)
			return 1;
		int res = 0;
		for (int i = 0; i * coins[cur] <= n; i++) {
			int remain = n - i * coins[cur];
			res += collectCoins(remain, coins, cur - 1);
		}
		return res;
	}

	public static int collectCoinsLoop(int n, int[] coins) {
		int[][] helper = new int[coins.length][n + 1];
		Arrays.fill(helper[0], 1);
		for (int row = 1; row < coins.length; row++)
			for (int col = 0; col <= n; col++)
				// 状态转移方程 helper[row][col]=helper[row - 1][col] + helper[row -
				// 1][col-coins[row]]+...+ helper[row - 1][col-n*coins[row]]
				for (int cnt = col; cnt >= 0; cnt -= coins[row])
					helper[row][col] += helper[row - 1][cnt];
		return helper[coins.length - 1][n];
	}

	/**
	 * 子集生成:
	 * 
	 * 空集开始->加a[0]或不加->加a[1]或不加
	 * 
	 * @param a
	 */
	public static Set<Set<Integer>> subsetGenerate(int[] a) {
		return subsetGenerate(a, a.length, a.length - 1);
	}

	@SuppressWarnings("unchecked")
	private static Set<Set<Integer>> subsetGenerate(int[] a, int n, int cur) {
		Set<Set<Integer>> newSet = new HashSet<>();
		if (cur == 0) {
			// 空集
			Set<Integer> nil = new HashSet<>();
			Set<Integer> first = new HashSet<>();
			first.add(a[0]);
			newSet.add(nil);
			newSet.add(first);
			return newSet;
		}

		Set<Set<Integer>> oldSet = subsetGenerate(a, n, cur - 1);
		for (Set<Integer> set : oldSet) {
			newSet.add(set);
			HashSet<Integer> clone = (HashSet<Integer>) ((HashSet<Integer>) set).clone();
			clone.add(a[cur]);
			newSet.add(clone);
		}
		return newSet;
	}

	/**
	 * 子集生成 loop
	 * 
	 * @param a
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<Set<Integer>> subsetGenerateLoop(int[] a) {
		Set<Set<Integer>> res = new HashSet<>();
		res.add(new HashSet<>());
		for (int i = 0; i < a.length; i++) {
			Set<Set<Integer>> newSet = new HashSet<>();
			newSet.addAll(res);
			for (Set<Integer> e : res) {
				HashSet<Integer> clone = (HashSet<Integer>) ((HashSet<Integer>) e).clone();
				clone.add(a[i]);
				newSet.add(clone);
			}
			res = newSet;
		}
		return res;
	}

	/**
	 * 子集生成-二进制
	 * 
	 * 对于集合中的某个元素, 在子集生成过程中: 有选取(1)和不选取(0), 两种选择
	 * 
	 * @param a
	 * @return
	 */
	public static ArrayList<ArrayList<Integer>> subsetGenerateBinary(int[] a) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<>();// 放所有结果的容器
		int n = (int) Math.pow(2, a.length);// x个元素就有2^x种选择
		for (int i = 0; i < n; i++) {
			ArrayList<Integer> arr = new ArrayList<>();
			for (int j = a.length - 1; j >= 0; j--) {
				if (((i >> j) & 1) == 1) {
					arr.add(a[j]);
				}
			}
			res.add(arr);
		}
		return res;
	}

	/**
	 * 全排列 -- 回溯
	 * 
	 * @param a
	 * @return
	 */
	public static ArrayList<String> permutation(int[] a) {
		class Perm {
			private static ArrayList<String> res = new ArrayList<>();

			private static void perm(int[] a, int start) {
				if (start == a.length)// 排好了
					res.add(Arrays.toString(a));
				for (int i = start; i < a.length; i++) {
					Util.swap(a, i, start);
					perm(a, start + 1);// 排后面
					Util.swap(a, i, start);// 回溯
				}
			}

			public static ArrayList<String> perm(int[] a) {
				perm(a, 0);
				return res;
			}
		}
		return Perm.perm(a);
	}

	/**
	 * 数独
	 * 
	 * @param p
	 * 
	 */
	public static void sudoku(int[][] table, int x, int y) {
		if (x == 9) {
			Util.print(table);
			// 数独的解唯一,填完之后直接结束,而不是return
			System.exit(0);
		}
		if (table[x][y] == 0) {
			for (int k = 1; k < 10; k++) {
				// k是否能填入
				if (check(table, x, y, k)) {
					// 填入
					table[x][y] = k;
					// 从右到左, 从上到下
					sudoku(table, x + (y + 1) / 9, (y + 1) % 9);
				}
			}
			// 如果1~10都不能填入就回溯
			table[x][y] = 0;// 回溯
		} else {// 直接去填下一个
			sudoku(table, x + (y + 1) / 9, (y + 1) % 9);
		}
	}

	private static boolean check(int[][] table, int x, int y, int k) {
		// 检查行列
		for (int i = 0; i < 9; i++) {
			if (table[x][i] == k)
				return false;
			if (table[i][y] == k)
				return false;
		}
		// 检查3*3方格
		for (int i = (x / 3) * 3; i < (x / 3 + 1) * 3; i++)
			for (int j = (y / 3) * 3; j < (y / 3 + 1) * 3; j++)
				if ((table[i][j] == k))
					return false;
		return true;
	}

	/**
	 * 若干个元素的和等于k (二进制)
	 * 
	 * @param a
	 * @param k
	 */
	public static void sumEqualsToKLoop(int[] a, int k) {
		int n = Util.pow(2, a.length);
		ArrayList<Integer> ls = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			int sum = 0;
			ls.clear();
			for (int j = a.length - 1; j >= 0; j--) {
				if (((i >> j) & 1) == 1) {
					sum += a[j];
					ls.add(a[j]);
				}
			}
			if (sum == k) {
				System.out.println(k + " = " + ls);
			}
		}
	}

	/**
	 * 若干个元素的和等于a
	 * 
	 * @param a
	 * @param k
	 */
	public static void sumEqualsToK(int[] a, int k) {
		class Dfs {
			static int[] a;
			static ArrayList<Integer> ints = new ArrayList<Integer>();
			static int n;

			public static void dfs(int k, int cur) {
				if (k == 0) {
					System.out.println(n + " = " + ints);
					System.exit(0);// 剪支
				}
				if (k < 0 || cur == a.length)
					return;
				// 不要当前元素
				dfs(k, cur + 1);
				// 要当前元素
				ints.add(a[cur]);
				dfs(k - a[cur], cur + 1);
				// 回溯
				ints.remove(ints.size() - 1);
			}
		}
		Dfs.a = a;
		Dfs.n = k;
		Dfs.dfs(k, 0);
	}

	/**
	 * 水洼
	 * 
	 * W 是一个水洼,W和其八个方向的W也是水洼
	 * 
	 * 求水洼数
	 * 
	 * W.....WW..
	 * .WW....WW.
	 * .W......W.
	 * ........W.
	 * ........W.
	 * ..W.....W.
	 * .W.W...W..
	 * W.W.W.W...
	 * .W.W...W..
	 * ..W.......
	 * 
	 * @param a
	 * @return
	 */
	public static int paddle(char[][] a) {
		class Dfs {
			public static int paddle(char[][] a) {
				int cnt = 0;
				for (int i = 0; i < a.length; i++) {
					for (int j = 0; j < a[i].length; j++) {
						if (a[i][j] == 'W') {
							dfs(a, i, j);
							cnt++;
						}
					}
				}
				return cnt;
			}

			/**
			 * 八联通检测
			 * 
			 * 将W变为.
			 * 
			 * @param a
			 * @param i
			 * @param j
			 */
			private static void dfs(char[][] a, int i, int j) {
				a[i][j] = '.';
				// 遍历八方
				for (int k = -1; k < 2; k++) {
					for (int l = -1; l < 2; l++) {
						// 跳过自己
						if (k == 0 && l == 0)
							continue;
						if (i + k >= 0 && i + k <= a.length - 1 && j + l >= 0 && j + l <= a[i].length - 1) {
							if (a[i + k][j + l] == 'W')
								dfs(a, i + k, j + l);
						}
					}
				}
			}
		}
		return Dfs.paddle(a);
	}

	/**
	 * n皇后
	 * 
	 * @param n
	 */
	public static void nQueen(int n) {
		class Dfs {
			static int[] rec;
			static int cnt;
			static int n;

			public static void dfs(int row) {
				if (row == n) {
					cnt++;
					print();
					return;
				}
				for (int col = 0; col < n; col++) {
					boolean can = true;
					for (int i = 0; i < row; i++) {
						// 主对角线 x-y相同
						// 副对角线 x+y相同
						if (rec[i] == col || i + rec[i] == row + col || rec[i] - i == col - row) {
							can = false;
							break;
						}
					}
					if (can) {
						rec[row] = col;
						dfs(row + 1);
						// 这里可以不用恢复,因为检查只检查row上面的
						// rec[row]=0;
					}
				}
			}

			public static void print() {
				char[][] a = new char[n][n];
				for (int i = 0; i < n; i++)
					Arrays.fill(a[i], '.');
				for (int i = 0; i < n; i++) {
					a[i][rec[i]] = 'Q';
				}
				Util.print(a);
			}
		}
		Dfs.n = n;
		Dfs.rec = new int[n];
		Dfs.dfs(0);
		System.out.println(Dfs.cnt);

	}


	/**
	 * 素数环
	 * 用1~n,组成一个环其中两相邻数之和为素数
	 * 
	 * @param n
	 */
	public static void primeCircle(int n) {

		class Dfs {
			static int n;
			static int[] rec;

			public static void dfs(int cur) {
				if (cur == n && isPrime(rec[0] + rec[n - 1])) {
					Util.print(rec);
					return;
				}
				for (int i = 2; i <= n; i++) {
					if (check(i, cur)) {
						rec[cur] = i;
						dfs(cur + 1);
						rec[cur] = 0;// 回溯
					}
				}
			}

			private static boolean check(int k, int cur) {
				for (int i = 1; i < n; i++)
					if (rec[i] == k)
						return false;
				if (!isPrime(rec[cur - 1] + k))
					return false;
				return true;
			}

			private static boolean isPrime(int i) {
				return MathProblems.isPrime(i);
			}
		}
		Dfs.n = n;
		Dfs.rec = new int[n];
		Dfs.rec[0] = 1;
		Dfs.dfs(1);
	}

	/**
	 * 困难的串:
	 * 
	 * 容易的串: 一个字符串包含两个相邻的重复字串<br/>
	 * 困难的串: 不是容易的串
	 * 
	 * 输入 n,l
	 * 输出由前l个字符组成的,字典序第n小的困难的串
	 * 
	 * eg: l=3 n=4
	 * A, AB, ABA, ABAC, ABACA, ABACAB, ABACABA
	 * 输出: ABAC
	 * 
	 * @param n
	 * @param l
	 */
	public static void hardString(int n, int l) {

		class Dfs {
			static char[] rec;
			static int l;

			public static void dfs(int cur) {
				if (cur == rec.length) {
					System.out.println(Arrays.toString(rec));
					// return;
					System.exit(0);
				}
				for (int i = 0; i < l; i++) {
					if (check((char) ('A' + i), cur)) {
						rec[cur] = (char) ('A' + i);
						dfs(cur + 1);
					}
				}
			}

			private static boolean check(char c, int cur) {
				int width = 0;
				if (rec[cur - 1] == c)
					return false;
				for (int i = rec.length - 2; i >= 0; i -= 2, width++) {
					String s1 = new String(rec, i, width + 1);
					String s2 = new String(rec, i + width + 1, width) + c;
					if (s1.equals(s2))
						return false;
				}
				return true;
			}
		}
		Dfs.rec = new char[n];
		Dfs.rec[0] = 'A';
		Dfs.l = l;
		Dfs.dfs(1);
	}
}
