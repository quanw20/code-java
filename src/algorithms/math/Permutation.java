package algorithms.math;

import java.util.*;
import java.util.function.Consumer;

public class Permutation {
    static String s = "我是唐权威";
    public static Consumer<Stack<Integer>> f = stack -> {
        stack.forEach(x -> System.out.print(s.charAt(x)));
        System.out.println();
    };

    public static void main(String[] args) {
        // 建立{下标->符号}映射
        int[] a = new int[s.length()];
        for (int i = 1; i < a.length; ++i)
            a[i] = i;
        // 排列
        perm(a, new Stack<Integer>());
    }

    /**
     * 基于栈的全排列(字典序)
     * 
     * @param array
     * @param stack
     */
    public static void perm(int[] array, Stack<Integer> stack) {
        if (array.length == 0) {// 进入了叶子节点，输出栈中内容
            // f.accept(stack);
            System.out.println(stack);
            return;
        }
        for (int i = 0; i < array.length; i++) {
            int[] tempArray = new int[array.length - 1];
            System.arraycopy(array, 0, tempArray, 0, i);
            System.arraycopy(array, i + 1, tempArray, i, array.length - i - 1);
            stack.push(array[i]);
            perm(tempArray, stack);
            stack.pop();
        }
    }

    /**
     * 基于交换的全排列
     * 
     * 通过交换实现的全排列不是字典序的全排列
     * 
     * @param array 参与排列的数组
     * @param start 起始下标(包括)
     * @param end   结束下标(包括)
     */
    public static void perm(int[] array, int start, int end) {
        if (start == end) {
            System.out.println(Arrays.toString(array));
        } else {
            for (int i = start; i <= end; i++) {
                swap(array, start, i);
                perm(array, start + 1, end);
                swap(array, start, i);
            }
        }
    }

    public static final void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

}
