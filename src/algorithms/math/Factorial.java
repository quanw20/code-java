package algorithms.math;

public class Factorial {
    /**
     * 阶乘末尾0的个数
     * 
     * 末尾0的个数就是指这个数总共有几个10因子，而10又能表示成2和5的乘积。
     * 假设m=n!，那么m中2的因子个数肯定大于5的因子个数，所以m中5的因子个数即是所要求结果；
     * 显然n除以5可得到1~n中包含有一个因子5的个数，但是，1~n中有的数可以被5整除好几次，
     * 所以必须将这个数再除以5，得到1~n中包含有两个因子5的个数，依次循环进行累加即可得到全部5的因子个数；
     * 
     * 计算n!中有多少个质因子p
     * 
     * 
     * <pre>
     * n=51,p=5
     * 
     * 1~n 中有
     * 
     *  5 10
     * 25 30
     * 35 40
     * 45 50
     * 
     * 共10个
     * 
     * n!中有   50 45 35 ... 5
     * p的个数有 10 9   7 ... 1
     * </pre>
     * 
     * @param n
     * @param p
     */
    public int countFactor(int n, int p) {
        int ans = 0;
        while (n != 0) {
            ans += n / p;
            n /= p;
        }
        return ans;
    }

    public int preimageSizeFZF(int K) {
        // 左边界和右边界之差 + 1 就是答案
        return (int) (right_bound(K) - left_bound(K) + 1);
    }

    private long left_bound(int k) {
        long left = 0, right = Long.MAX_VALUE;
        while (left < right) {
            long mid = left + (right - left >> 1);
            if (f(mid) >= k) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    private int f(long mid) {
        int ans = 0;
        for (long i = mid; i/5 > 0; i /= 5)
            ans += mid / 5;
        return ans;
    }

    private long right_bound(int k) {
        long left = 0, right = Long.MAX_VALUE;
        while (left < right) {
            long mid = left + (right - left >> 1);
            if (f(mid) <= k) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return right - 1;
    }

    public static void main(String[] args) {
        Factorial f = new Factorial();
        int preimageSizeFZF = f.preimageSizeFZF(0);
        System.out.println(preimageSizeFZF);
    }
}
