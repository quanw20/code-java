package algorithms.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

import org.junit.jupiter.api.Test;

/**
 * 1. Prime 素数
 * 
 * <pre>
 * 素数与合数的简单性质:
 * 
 * 大于 1 的整数 a 是合数,等价于 a 可以表示为整数 d 和 e（1<d, e<a）的乘积
 * 如果素数 p 有大于 1 的约数 d ,那么 d=p
 * 大于 1 的整数 a 一定可以表示为素数的乘积
 * 对于合数 a ,一定存在素数 p<sqrt(a) 使得 p//a
 * 素数有无穷多个
 * 所有大于 3 的素数都可以表示为 6n+1 的形式
 * 
 * <pre>
 */
public class NumberTheory {
    private boolean isPrime(int n) {
        int sqr = (int) (Math.sqrt(n) + 0.5);
        for (int i = 2; i < sqr; ++i) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    /**
     * 使用(埃氏)筛法求 2-n(包括)之间有多少个素数
     * 1. 将[2, n]初始化为true
     * 2. 枚举[2, sqrt(n)], 计数器++, 并筛除素数的倍数
     * 3. 继续枚举(sqrt(n), n], 如果仍然为true -> 计数器++;
     * 
     * @param n 右端点
     */
    public static void seive(int n) {
        BitSet isPrime = new BitSet(n + 1);
        int cnt = 0, i = 0;
        for (i = 2; i <= n; ++i)
            isPrime.set(i);

        for (i = 2; i * i <= n; ++i) {
            if (isPrime.get(i)) {
                ++cnt;
                for (int k = i << 1; k <= n; k += i)
                    isPrime.clear(k);
            }
        }
        for (; i <= n; ++i) {
            if (isPrime.get(i))
                ++cnt;
        }
        System.out.println(cnt + " primes");
    }

    public static void segment_sieve(long a, long b) {
        int qb = (int) Math.sqrt(b);
        BitSet isPrime = new BitSet(Integer.MAX_VALUE);
        BitSet isPrimeSmall = new BitSet(qb);

        isPrime.set(0, isPrime.length());
        isPrimeSmall.set(0, isPrimeSmall.length());

        for (int i = 2; (long) i * i < b; ++i) {
            if (isPrimeSmall.get(i)) {
                for (int j = i + i; (long) j * j < b; j += i)// [2,sqrt(b))
                    isPrimeSmall.clear(j);
                for (long j = Math.max(2, (a + i - 1) / i) * i; j < b; j += i)// [a,b)
                    isPrime.clear((int) (j - a));
            }
        }

    }

    /**
     * 输入n将n质因数分解
     * 
     * @param n
     */
    void primeFactorization(int n) {
        boolean first = true;
        for (int i = 2; i <= n; ++i) {
            if (isPrime(i) && n % i == 0) {
                int s = 0;
                while (n % i == 0) {
                    ++s;
                    n /= i;
                }
                if (first) {
                    System.out.print(i + "^" + s + " ");
                    first = false;
                } else {
                    System.out.print("* " + i + "^" + s + " ");
                }
            }
        }
    }

    void getPrime(int m, int n) {
        for (int i = m; i < n; ++i) {
            if (isPrimeII(i))
                System.out.print(i + " ");
        }
    }

    boolean[] isPrime = new boolean[10001];
    boolean init = false;

    boolean isPrimeII(int n) {
        if (!init) {
            Arrays.fill(isPrime, true);
            isPrime[0] = isPrime[1] = false;
            for (int i = 2; i < isPrime.length; i++) {
                if (isPrime[i])
                    for (int j = i * i; j < isPrime.length; j += i) {
                        isPrime[j] = false;
                    }
            }
            init = true;
        }
        return isPrime[n];
    }

    /**
     * 欧拉函数
     * 已知正整数x，求1~x-1中，有多少与x互质的数。（互质是指两个数最大公约数为1）
     * 
     * @param n
     */
    void EulerFunc(int n) {
        int res = n, i;
        for (i = 2; i * i <= n; i++)
            if (n % i == 0) { // 第一次找到的必为素因子
                n /= i;
                res = res - res / i; // x(1-1/p1)
                while (n % i == 0)
                    n /= i; // 将该素因子的倍数也全部筛掉
            }
        if (n > 1)
            res = res - res / n;
        System.out.println(res);
    }

    /**
     * 约数个数
     * 
     * @param n
     */
    void divisorCount(int n) {
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 1; i <= n / i; ++i) {
            if (n % i == 0) {
                al.add(i);
                if (i != n / i)
                    al.add(n / i);
            }
        }
        System.out.println(al.size());
        System.out.println(al);
    }

    int gcd(int m, int n) {
        if (n == 0)
            return m;
        return gcd(n, m / n);
    }

    int lcm(int m, int n) {
        return m * n / gcd(m, n);
    }

    @Test
    public void test_02() {
        primeFactorization(135);
    }

    @Test
    public void test_03() {
        EulerFunc(12412);
    }

    @Test
    public void test_04() {
        divisorCount(12412);
    }

    @Test
    public void testGetPrime() {
        getPrime(2, 100);
    }

    @Test
    public void test_00() {
        System.out.println(gcd(36, 6));
        System.out.println(lcm(36, 6));
    }
}
