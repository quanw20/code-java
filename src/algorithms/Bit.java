package algorithms;

import java.util.BitSet;

/**
 * 一些常用的位运算(一般和整数类型运算)
 * 
 * <pre>
 * 基本运算:
 * 
 * ~    按位取反
 * &    与
 * |    或
 * ^    异或
 * <<   左移
 * >>   右移        左边是补上符号位
 * >>>  无符号右移   左边是补上0
 * 
 * 复合运算:
 * 
 * &= , |= , ^= , <<= , >>=
 * 
 * 注意:
 * 
 * int类型移位大于等于32位时, long类型大于等于64位时, 会先做求余处理再位移处理
 * byte, short移位前会先转换为int类型（32位）再进行移位
 * 
 * c++则是未定义行为
 * </pre>
 */
public class Bit {
    /**
     * 汉明权重
     * 计算整数x的二进制中'1'的个数
     * 
     * @param x 整数x
     * @return '1'的个数
     */
    int countOnes(int x) {
        int ones = 0;
        if (x < 0) {
            x = x & Integer.MAX_VALUE;
            ones = 1;
        }
        while (x > 0) {
            x &= (x - 1);// 把右边第一个1变成0
            ones++;
        }
        return ones;
    }

    /**
     * 交换整数的奇偶位
     * 
     * @param n
     * @return
     */
    int exchangeOddAndEven(int n) {
        int even = n & 0xaaaaaaaa;// 1010 ... 1010 取出偶数位
        int odd = n & 0x55555555;// 0101 ... 0101 取出奇数位
        return (even >> 1) ^ (odd << 1); // 异或--不进位加法
    }

    /**
     * 是否是二的幂
     * 
     * @param n n
     * @return 是否是二的幂
     */
    boolean isPowerOfTwo(int n) {
        return n > 0 && (n & (n - 1)) == 0;
    }

    /**
     * 获取 a 的第 b 位，最低位编号为 0
     * 
     * @param a a
     * @param b b
     * @return a 的第 b 位 0 or 1
     */
    int getBit(int a, int b) {
        return (a >> b) & 1;
    }

    /**
     * 对二的非负数次幂取模
     * 
     * @param x   数
     * @param mod 模
     * @return
     */
    int modPowerOfTwo(int x, int mod) {
        return x & (mod - 1);
    }

    /**
     * n>>31 取得 n 的符号，若 n 为正数，n>>31 等于 0，若 n 为负数，n>>31 等于 -1
     * 若 n 为正数 n^0=n, 数不变，若 n 为负数有 n^(-1)
     * 需要计算 n 和 -1 的补码，然后进行异或运算，
     * 结果 n 变号并且为 n 的绝对值减 1，再减去 -1 就是绝对值
     * 
     * @param n
     * @return
     */
    int abs(int n) {
        // 右操作数（即移位数）为负值；
        // 右操作数大于等于左操作数的位数；
        return (n ^ (n >> 31)) - (n >> 31);
    }

    /**
     * 如果 a>=b,(a-b)>>31 为 0，否则为 -1
     * 
     * @param a
     * @param b
     * @return max
     */
    int max(int a, int b) {
        return b & ((a - b) >> 31) | a & (~(a - b) >> 31);
    }

    /**
     * min
     * 
     * @param a
     * @param b
     * @return min
     */
    int min(int a, int b) {
        return a & ((a - b) >> 31) | b & (~(a - b) >> 31);
    }

    /**
     * 判断符号
     * 
     * @param x
     * @param y
     * @return
     */
    boolean isSameSign(int x, int y) { // 有 0 的情况例外
        return (x ^ y) >= 0;
    }

    /**
     * 将 a 的第 b 位设置为 0 ，最低位编号为 0
     * 
     * @param a
     * @param b
     * @return 将 a 的第 b 位设置为 0,后的 a
     */
    int unsetBit(int a, int b) {
        return a & ~(1 << b);
    }

    /**
     * 将 a 的第 b 位设置为 1 ，最低位编号为 0
     * 
     * @param a
     * @param b
     * @return
     */
    int setBit(int a, int b) {
        return a | (1 << b);
    }

    /**
     * 将 a 的第 b 位取反 ，最低位编号为 0
     * 
     * @param a
     * @param b
     * @return
     */
    int flapBit(int a, int b) {
        return a ^ (1 << b);
    }

    /**
     * 交换两个整数
     * 
     * @param a
     * @param b
     */
    void swap(int[] a, int[] b) {
        a[0] ^= b[0] ^= a[0] ^= b[0];
    }

    /**
     * 只出现一次的数字
     * 
     * a^0=a
     * a^a=0
     * a^b^a=b;
     * 
     * @param nums 数组
     * @return 只出现一次的数字
     */
    int singleNumber(int[] nums) {
        int res = 0;
        for (int i : nums)
            res ^= i;
        return res;
    }

    /**
     * 给定一个包含 [0, n] 中 n 个数的数组 nums ，找出 [0, n] 这个范围内没有出现在数组中的那个数
     * 
     * <pre>
     * 
     * [3,0,1]
     * 
     * 0 1 2 3 :index 
     * 3 0 1 x :nums
     * 
     * </pre>
     * 
     * @param nums 包含 [0, n] 中 n 个数的数组
     * @return 没有出现在数组中的那个数
     */
    int missingNumber(int[] nums) {
        if (nums.length == 1)
            return 1;
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            res = res ^ i ^ nums[i];
        }
        return res ^ nums.length;
    }

    boolean isOdd(int n) {
        return (n & 1) == 1;
    }

    /**
     * 用位运算模拟集合操作
     * 交集 a交b a&b
     * 并集 a并b a|b
     * 补集 a的补 ~a(全集为二进制都是一)
     * 差集 a\b a&~b
     * 对称差 a-b a^b
     */
    @SuppressWarnings("all")
    private class MyBitSet {
        // 代替boolean[]
        BitSet bs = new BitSet();

        void test() {
            int u = 0;
            // 遍历 u 的非空子集
            for (int s = u; s != 0; s = (s - 1) & u) {
                // s 是 u 的一个非空子集
            }
        }

    }

    public static void main(String[] args) {
        Bit bit = new Bit();
        int m = bit.modPowerOfTwo(128, 5);
        System.out.println(m);
        int x = 10;
        System.out.println(Integer.bitCount(x));
        System.out.println(Integer.toHexString(x));
        System.out.println(Integer.highestOneBit(x));
        System.out.println(Integer.lowestOneBit(x));

    }
}
