package algorithms.sort;

@SuppressWarnings("all")
public class ShellSort {
    public static void shellSort(Comparable[] a) {
        int h = 1, l = a.length;
        for (; h < l / 3; h = 3 * h + 1)
            ;
        for (; h >= 1; h /= 3) {
            for (int i = h; i < l; ++i) {
                for (int j = i; j > h && a[j - h].compareTo(a[j]) > 0; j -= h)
                    swap(a, j, j - h);
            }
        }
    }

    private static void swap(Comparable[] a, int j, int i) {
        Comparable tmp = a[j - 1];
        a[j - 1] = a[j];
        a[j] = tmp;
    }

    public static void main(String[] args) {
        Integer[] a = new Integer[] { 1, 43, 2, 123, 1412, 25, 2341, 12, 41, 24, 12, 4, 1, 24, 123412 };
        shellSort(a);
        for (Integer integer : a) {
            System.out.print(integer + " ");
        }
    }
}
