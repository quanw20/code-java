package algorithms.sort;

import org.junit.jupiter.api.*;

import algorithms.Util;

import static algorithms.Util.*;
import static algorithms.sort.SortProblems.*;
import static algorithms.sort.CountingSort.*;
import static algorithms.sort.RadixSort.*;
import static algorithms.sort.BucketSort.*;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class SortTest {

    static int[] a;

    @BeforeAll
    public static void ba() {
        a = new int[10];
        Random random = new Random(0);
        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(1, 10);
        }
    }

    @Test
    public void testCountingSort() {
        int[] c = a.clone();
        int[] v = a.clone();
        long start = System.nanoTime();
        CountingSort.countingSort(c);
        System.out.println(System.nanoTime() - start);
        Arrays.sort(c);
        Assertions.assertArrayEquals(v, c, "不相等");
        System.out.println(Arrays.toString(c));
    }

    @Test
    public void testRadixSort() {
        int[] c = a.clone();
        int[] v = a.clone();
        long start = System.nanoTime();
        RadixSort.radixSort(c);
        System.out.println(System.nanoTime() - start);
        Arrays.sort(v);
        Assertions.assertArrayEquals(v, c, "不相等");
        System.out.println(Arrays.toString(c));
    }

    @Test
    public void testShellSort() {
        // int[] c = a.clone();
        Integer[] v = (Integer[]) IntStream.of(a).boxed().toArray();
        Integer[] c = (Integer[]) IntStream.of(a).boxed().toArray();
        long start = System.nanoTime();
        ShellSort.shellSort(c);
        System.out.println(System.nanoTime() - start);
        Arrays.sort(c);
        Assertions.assertArrayEquals(v, c, "不相等");
        System.out.println(Arrays.toString(c));
    }

    @Test
    public void testTime() {
        System.out.println("+----耗时测试----+");
        long startTime = System.currentTimeMillis();
        int times = 1000_000;
        while (times-- != 0) {
            int[] a = { 3, 2, 1, 4, 5, 3, 1, 10, 32 };
            countingSort(a);
        }
        System.out.println("用时: " + (System.currentTimeMillis() - startTime) + "毫秒");

        // System.out.println("+----耗时测试----+");
        // long startTime1 = System.currentTimeMillis();
        // int times1 = 1000_000;
        // while (times-- != 0) {
        // int[] a = { 3, 2, 1, 4, 5, 3, 1, 10, 32 };
        // insertSort(a, 0, a.length - 1);
        // }
        // System.out.println("用时: " + (System.currentTimeMillis() - startTime) + "毫秒");
    }

    @Test
    public void testNum() {
        int n = 987654321;
        System.out.println(n % 10000 / (1000));

    }

    @Test
    public void testRadix() {
        int[] a = { 3, 2, 1, 4, 5, 3, 1, 10, 32 };
        radixSort(a);
        System.out.println(Arrays.toString(a));

    }

    @Test
    public void testFactorSum() {
        //
        int[] a = range(-8, 20);
        System.out.println(Arrays.toString(a));
        // int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
        int[][] r = findFactorOfSum(a, 12);
        System.out.println(Arrays.deepToString(r));
    }

    @Test
    public void testNeedSort() {
        int[] a = { 1, 2, 3, 7, 6, 4, 5, 8, 9 };
        int[] res = findSegmentNeedSort(a);
        System.out.println(Arrays.toString(res));

    }

    @Test
    public void testfindSegmentNeedSort() {
        int[] a = { 1, 2, 3, 7, 8, 5, 9 };
        print(findSegmentNeedSort(a));

        int[] b = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        print(findSegmentNeedSort(b));

    }

    @Test
    public void testTopK() {
        // int findTopK = findTopK();
        // System.out.println(findTopK);
    }

    public static void main(String[] args) {
        new SortTest().testTopK();
    }

    @Test
    public void testMinComb() {
        int[] a = { 321, 32, 3 };
        int min = findMinCombination(a);
        System.out.println(min);
    }

    @Test
    public void testBucket() {
        int[] a = { 4, 1, 3, 8, 6, 5, 34, 50, 9, 0 };
        bucketSort(a);
        Util.print(a);
    }

}
