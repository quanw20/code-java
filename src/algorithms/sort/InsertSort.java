package algorithms.sort;

@SuppressWarnings("all")
public class InsertSort {
    public static void sort(Comparable[] a) {
        for (int i = 1, l = a.length; i < l; ++i) {
            // 后小于前->往前插入
            for (int j = i; j > 0 && a[j].compareTo(a[j - 1]) < 0; --j) {
                swap(a, j - 1, j);
            }
        }
    }

    private static void swap(Comparable[] a, int i, int j) {
        Comparable tmp = a[j - 1];
        a[j - 1] = a[j];
        a[j] = tmp;
    }

    /**
     * 使用链表的插入排序
     * 
     * @param a
     */
    public static void insertSort(int[] a) {
        final class Node {
            public int val;
            public Node next;

            public Node(int val) {
                this.val = val;
            }

            public Node(int val, Node next) {
                this.val = val;
                this.next = next;
            }
        }
        // 初始化
        Node head = new Node(a[0]);
        for (int i = 1, l = a.length; i < l; i++) {
            if (head.val >= a[i]) {// 小于头节点放在头上
                head = new Node(a[i], head);
            } else {// 放在第一个较大的之前
                Node p = head, pre = p;
                while (p != null && p.val < a[i]) {
                    pre = p;
                    p = p.next;
                }
                pre.next = new Node(a[i], p);
            }

        }
        int k = 0;
        for (Node p = head; p != null; p = p.next)
            a[k++] = p.val;

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[] { 1, 43, 2, 123, 1412, 25, 2341, 12, 41, 24, 12, 4, 1, 24, 123412 };
        sort(a);
        for (Integer integer : a) {
            System.out.print(integer + " ");
        }
    }
}
