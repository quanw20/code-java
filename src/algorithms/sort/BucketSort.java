package algorithms.sort;

final class Node {
    public int val;
    public Node next;

    public Node(int val) {
        this.val = val;
    }

    public Node(int val, Node next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return "val= " + val;
    }
}

public class BucketSort {
    /**
     * 桶排序
     * 
     * 类似计数排序,但一个桶中可以放多个数据(用链表串起来)
     * 
     * 先分桶再用其他方法排序
     * 
     * 适合均匀分布的
     * 
     * @param a
     */
    public static void bucketSort(int[] a) {

        // a->length, max
        int l = a.length;
        int max = 0;
        for (int i = 0; i < l; i++) {
            if (a[i] > max)
                max = a[i];
        }

        // node[]
        Node[] buckets = new Node[l];
        // 扫描->hash->下标->元素入桶
        for (int i = 0; i < l; i++) {
            int index = a[i] * l / (max + 1);
            if (buckets[index] == null) {
                buckets[index] = new Node(a[i]);
            } else {
                if (buckets[index].val >= a[i]) {// 小于头节点放在头上
                    buckets[index] = new Node(a[i], buckets[index]);
                } else {// 放在第一个较大的之前
                    Node p = buckets[index], pre = p;
                    while (p != null && p.val < a[i]) {
                        pre = p;
                        p = p.next;
                    }
                    pre.next = new Node(a[i], p);
                }
            }
        }
        // 出桶
        int k = 0;
        for (int i = 0; i < l; i++)
            for (Node p = buckets[i]; p != null; p = p.next)
                a[k++] = p.val;
    }

}
