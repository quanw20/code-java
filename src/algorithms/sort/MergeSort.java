package algorithms.sort;

import java.util.Arrays;

public class MergeSort {
    /**
     * 归并排序
     * 
     * @param a 数组
     */
    public static int[] margeSort(int[] a) {
        sort(a, 0, a.length - 1);
        return a;
    }

    /**
     * 分解随意<br/>
     * 重在合并<br/>
     * 
     * O(nlogn)
     * 
     * @param a  数组
     * @param lo 起点(包括)
     * @param hi 终点(包括)
     */
    private static void sort(int[] a, int lo, int hi) {
        if (lo >= hi)
            return;
        int mid = lo + (hi - lo >> 1);
        sort(a, lo, mid);
        sort(a, mid + 1, hi);
        merge(a, lo, mid, hi);
    }

    /**
     * 合并
     * 
     * @param a   数组
     * @param lo  起点(包括)
     * @param mid 中点
     * @param hi  终点(包括)
     */
    private static void merge(int[] a, int lo, int mid, int hi) {
        int[] ret = a.clone();
        int cnt = lo;// 从低下标开始
        int l = lo, r = mid + 1;
        while (l <= mid && r <= hi) {// 合并两个有序数组
            if (ret[l] < ret[r])
                a[cnt++] = ret[l++];
            else
                a[cnt++] = ret[r++];
        }
        while (l <= mid)// 复制剩余的
            a[cnt++] = ret[l++];
        while (r <= hi)
            a[cnt++] = ret[r++];
    }

    public static void main(String[] args) {
        int[] a = margeSort(new int[] { 2, 14, 1245, 1, 51235, 214634, 6325, 6, 34, 521334 });
        System.out.println(Arrays.toString(a));
    }
}
