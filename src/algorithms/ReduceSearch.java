package algorithms;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

/**
 * ReduceSearch
 */
public class ReduceSearch {

    int n;
    int[] A;
    int[] B;
    int[] C;
    int[] D;
    int[] CD;

    int lower(int[] a, int v) {
        int left = 0, right = a.length;
        while (left < right) {
            int mid = (left + right) / 2;
            if (a[mid] >= v) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

    int upper(int[] a, int v) {
        int left = 0, right = a.length;
        while (left < right) {
            int mid = (left + right) / 2;
            if (a[mid] <= v) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return right - 1;
    }

    void solve() {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                CD[i * n + j] = D[i] + D[i];
            }
        }
        Arrays.sort(D);
        int res = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                int cd = -(A[i] + B[j]);
                res += upper(CD, cd) - lower(CD, cd);
            }
        }
        System.out.println(res);
    }

    void init() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        n = in.nextInt();
        A = new int[n];
        B = new int[n];
        C = new int[n];
        D = new int[n];
        CD = new int[n * n];
        for (int i = 0; i < n; i++) {
            A[i] = in.nextInt();
            B[i] = in.nextInt();
            C[i] = in.nextInt();
            D[i] = in.nextInt();
        }
    }

    public static void main(String[] args) throws IOException {
        ReduceSearch r = new ReduceSearch();
        r.init();
        r.solve();
    }
}