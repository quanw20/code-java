package algorithms.onoff;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class Fliptile {
    int[] dx = { -1, 0, 0, 0, 1 };
    int[] dy = { 0, -1, 0, 1, 0 };
    int M, N;
    final int MAX_M = 1001;
    final int MAX_N = 1001;
    int[][] tile = new int[MAX_M][MAX_N];
    int[][] opt = new int[MAX_M][MAX_N];
    int[][] flip = new int[MAX_M][MAX_N];

    int get(int x, int y) {
        int c = tile[x][y];
        for (int d = 0; d < 5; ++d) {
            int nx = x + dx[d], ny = y + dy[d];
            if (0 <= nx && nx < M && 0 < ny && ny < N)
                c += flip[nx][ny];
        }
        return c % 2;
    }

    int calc() {
        for (int i = 1; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                if (get(i - 1, j) != 0)
                    flip[i][j] = 1;
            }
        }
        for (int j = 0; j < N; ++j) {
            if (get(M - 1, j) != 0)
                return -1; 
        }
        int res = 0;
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                res += flip[i][j];
            }
        }
        return res;
    }

    void solve() throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        M = in.nextInt();
        N = in.nextInt();
        for (int i = 0; i < M; ++i) {
            for (int j = 0; j < N; ++j) {
                tile[i][j] = in.nextInt();
            }
        }
        int res = -1;
        for (int i = 0; i < 1 << N; i++) {
            flip = new int[MAX_M][MAX_N];
            for (int j = 0; j < N; ++j) {
                flip[0][N - j - 1] = 1 >> j & 1;
            }
            int num = calc();
            if (num >= 0 && (res < 0 || res > num)) {
                res = num;
                opt = flip.clone();
            }
        }
        if (res < 0) {
            System.out.println("IMPOSSIBLE");
        } else {
            for (int i = 0; i < M; ++i) {
                for (int j = 0; j < N; ++j) {
                    System.out.printf("%d%c", opt[i][j], j + 1 == N ? '\n' : ' ');
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new Fliptile().solve();
    }
}
