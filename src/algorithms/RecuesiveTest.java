package algorithms;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static algorithms.Recursive.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;

public class RecuesiveTest {
    @Test
    public void testMyQuickSort() {
        final int MAX_NUM = 100000;
        int[] a1 = new int[MAX_NUM];
        int[] a2 = new int[MAX_NUM];
        Random random = new Random();
        for (int i = 0; i < MAX_NUM; i++) {
            int num = (random.nextInt(1000)) * random.nextInt(1000) % 10003;
            a1[i] = num;
            a2[i] = num;
        }
        Arrays.sort(a2);
        for (int i = 0; i < MAX_NUM; i++) {
            assert (a1[i] == a2[i]);
        }
    }

    @Test
    public void testHannoi() {
        hannoi('a', 'b', 'c', 6);
    }

    @Test
    public void testBSearch() {
        int[] a = new int[] { 1, 2, 3, 4, 5, 6, 6, 7, 7, 7, 7, 8 };
        int index = binarySearchR(a, 0, a.length, 7);
        System.out.println(index);

    }

    @Test
    public void testRecursive() {
        // System.out.println(go(9));
        int[] a = { 3, 4, 5, 1, 2 };
        int[] b = { 3, 4, 1, 2 };
        System.out.println(findMinByBinary(a, 0, a.length - 1));
        System.out.println(findMinByBinary(b, 0, b.length - 1));
        Assertions.assertEquals(findMinByBinary(a, 0, a.length - 1), 1);
        Assertions.assertEquals(findMinByBinary(b, 0, b.length - 1), 1);
        Arrays.sort(a);
        Arrays.parallelSort(a);
    }

    @Test
    public void testSort() {
        int[] a = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        int[] sa = sortOddAndEven(a);
        System.out.println(Arrays.toString(sa));
    }

    @Test
    public void testInsertSort() {
        int[] a = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        insertSort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }

    @Test
    public void testFindK() {
        int[] a = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        System.out.println(findK(a, 2));
        Assertions.assertEquals(findK(a, 1), 0);
        Assertions.assertEquals(findK(a, 2), 1);
        Assertions.assertEquals(findK(a, 3), 2);
        Assertions.assertEquals(findK(a, 12), 11);
    }

    @Test
    public void testAhalf() {
        int[] a = { 6, 6, 6, 6, 6, 6, 4, 3, 2, 1, 0 };
        System.out.println(findOverAHalf(a));
        System.out.println(findOverAHalf2(a));
    }

    @Test
    public void testFindMinId() {
        int[] a = { 11, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
        Assertions.assertEquals(findMinId1(a), 10);
        Assertions.assertEquals(findMinId2(a), 10);
        Assertions.assertEquals(findMinId3(a, 0, a.length - 1), 10);
    }

    @Test
    public void testMerge() {
        int[] a = new int[10];
        a[0] = 1;
        a[1] = 3;
        a[2] = 5;
        a[3] = 7;
        a[4] = 9;
        int[] b = { 2, 4, 6, 8, 10 };
        int[] c = mergeArray(a, 5, b);
        System.out.println(Arrays.toString(c));
    }

    @Test
    public void testCoins() {
        for (int i = 0; i < 53; i += 2) {
            System.out.println(i + " -- " + collectCoinsLoop(i, new int[] { 1, 5, 10, 25 }));
        }
        int i = 10;
        int res = collectCoinsLoop(i, new int[] { 1, 5, 10, 25 });
        System.out.println(i + " -- " + res);
    }

    @Test
    public void testSubset() {
        int[] a = { 1, 2, 3, 4};
        Set<Set<Integer>> subsetGenerate = subsetGenerate(a);
        System.out.println(subsetGenerate);
        Set<Set<Integer>> subsetGenerateLoop = subsetGenerateLoop(a);
        System.out.println(subsetGenerateLoop);
        ArrayList<ArrayList<Integer>> subsetGenerateBinary = subsetGenerateBinary(a);
        System.out.println(subsetGenerateBinary);

    }

    @Test
    public void testPrem() {
        int[] a = { 1, 2, 3, 4 };
        ArrayList<String> permutation = permutation(a);
        System.out.println(permutation);
    }

    @Test
    public void testSudoku() {
        int[][] table = {
                { 0, 0, 5, 3, 0, 0, 0, 0, 0 },
                { 8, 0, 0, 0, 0, 0, 0, 2, 0 },
                { 0, 7, 0, 0, 1, 0, 5, 0, 0 },
                { 4, 0, 0, 0, 0, 5, 3, 0, 0 },
                { 0, 1, 0, 0, 7, 0, 0, 0, 6 },
                { 0, 0, 3, 2, 0, 0, 0, 8, 0 },
                { 0, 6, 0, 5, 0, 0, 0, 0, 9 },
                { 0, 0, 4, 0, 0, 0, 0, 3, 0 },
                { 0, 0, 0, 0, 0, 9, 7, 0, 0 },
        };
        sudoku(table, 0, 0);
    }

    @Test
    public void testSum() {
        int[] a = { 1, 2, 4, 3, 7 };
        sumEqualsToKLoop(a, 13);
        sumEqualsToK(a, 13);

    }

    @Test
    public void testPaddle() {
        char[][] a = {
                "W.....WW..".toCharArray(),
                ".WW....WW.".toCharArray(),
                ".W......W.".toCharArray(),
                "........W.".toCharArray(),
                "........W.".toCharArray(),
                "..W.....W.".toCharArray(),
                ".W.W...W..".toCharArray(),
                "W.W.W.W...".toCharArray(),
                ".W.W...W..".toCharArray(),
                "..W......W".toCharArray()
        };
        int paddle = paddle(a);
        System.out.println(paddle);
    }

    @Test
    public void testNQueen() {
        // nQueen(8);
        

    }

    @Test
    public void testPrime() {
        primeCircle(6);
    }

    @Test
    public void testAll() {
        char[] a = { 'a', 'b', 'c' };
        String string = new String(a, 1, 1);
        System.out.println(string);
    }

    @Test
    public void testHard() {
        hardString(4, 3);
    }
}
