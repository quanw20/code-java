package algorithms.dp;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。
 * 
 * 在「杨辉三角」中，每个数是它左上方和右上方的数的和。
 * 
 * 
 */
public class _118杨辉三角 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ret = new ArrayList<>();
        ret.add(new ArrayList<Integer>() {
            {
                add(1);
            }
        });
        for (int i = 2; i <= numRows; ++i) {
            ArrayList<Integer> al = new ArrayList<>();
            for (int j = 1; j <= i; ++j) {
                if (j == 1 || j == i)
                    al.add(1);
                else {
                    al.add(ret.get(i - 2).get(j - 2) + ret.get(i - 2).get(j-1));
                }
            }
            ret.add(al);
        }
        return ret;
    }
    // 给定一个非负索引 rowIndex，返回「杨辉三角」的第 rowIndex 行。
    public List<Integer> getRow(int rowIndex) {
        List<Integer> ret = new ArrayList<>();
        ret.add(1);
        for (int i = 1; i <= rowIndex; ++i)
            ret.add((int)((long)ret.get(i - 1) * (rowIndex - i + 1) / i));
        return ret;
    }
}
