package algorithms.dp;

import org.junit.jupiter.api.Test;

import static algorithms.dp.DynamicProgroming.*;
import static algorithms.dp.Fib.*;

public class DynamicTest {
    @Test
    public void testFib() {
        int n = 30;

        System.out.println(fibLoop(n));
        System.out.println(fibRecursive(n));
        System.out.println(fibRecord(n));
        System.out.println(fibFormula(n));
        System.out.println(fibMatrix(n));
    }

    @Test
    public void testSteel() {
        int[] steels = { 1, 5, 8, 16, 10, 17, 17, 20, 24, 300 };
        int length = 11;
        while (length-- != 0) {
            int cutSteel = cutSteel(steels, length);
            System.out.println(cutSteel);

            cutSteel = cutSteelRecord(steels, length);
            System.out.println(cutSteel);
        }
        cutSteelRecord(steels, 10);

    }

    @Test
    public void testTriangle() {
        int[][] tri = {
                { 7 },
                { 3, 8 },
                { 8, 1, 0 },
                { 2, 7, 4, 4 },
                { 4, 5, 2, 6, 5 },
                { 4, 5, 2, 6, 5, 7 },
                { 4, 5, 2, 6, 5, 7, 8 },
                { 4, 5, 2, 6, 5, 7, 8, 9 },
                { 4, 5, 2, 6, 5, 7, 8, 9, 10 },
                { 4, 5, 2, 6, 5, 7, 8, 9, 11, 13 },
        };

        int digitalTriangle = digitalTriangle(tri);
        System.out.println(digitalTriangle);

        int dt = digitalTriangleDp(tri);
        System.out.println(dt);

        int dta = digitalTriangleDpA(tri);
        System.out.println(dta);
    }

    @Test
    public void testLCS() {
        String s1 = "BA34Cdqfwasfalkdsvsi";
        String s2 = "A1BC2adiqwhdaodhf8qw";
        int lsc = LSC(s1, s2);
        System.out.println(lsc);
    }

    @Test
    public void testCSS() {
        int[] a = { -2, 11, -4, 13, -5, 2 };
        System.out.println(maxCSS(a));
    }

    @Test
    public void testLIS() {
        int[] a = { 1, 2, 3, -1, -2, 7, 9, 7, 7 };
        System.out.println(LIS(a));
        System.out.println(LISi(a));
    }

    @Test
    public void testLPS() {
        // String lps = LPSExpand("qwertyasdfghgfdsaqwerty");
        // System.out.println(lps);
        // System.out.println(LPS("asdsa"));
        // System.out.println(LPS("assa"));
        // System.out.println(LPS("qwq"));
        // System.out.println(LPS("ww"));
        // System.out.println(LPS("q"));
        // System.out.println(LPSExpand("asdsa"));
        // System.out.println(LPSExpand("assa"));
        // System.out.println(LPSExpand("qwq"));
        // System.out.println(LPSExpand("ww"));
        // System.out.println(LPSExpand("q"));
        // System.out.println(longestPalindrome("asdsa"));
        // System.out.println(LPSExpand("asdsa"));

        System.out.println(LPSManacher("aasasdsadadqdaugfasgfahdgasfdasaafaffafaffaff"));
    }

}
