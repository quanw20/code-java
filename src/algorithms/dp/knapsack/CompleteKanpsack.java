package algorithms.dp.knapsack;

/**
 * 有一个背包，最大容量为 amount，有一系列物品 coins，每个物品的重量为
 * coins[i]，每个物品的数量无限。请问有多少种方法，能够把背包恰好装满？
 * 
 * 1. 确定状态
 * f[i][j]依然表示前i种物品放入容量为v的背包的最大权值
 * 2.确定转移方程
 * f[i,j]=max(f[i-1,j-k*c[i]]+k*w[i])
 */
public class CompleteKanpsack {
    public static void main(String[] args) {

    }

    int change(int[] coins, int amount) {
        int n = coins.length;
        int[][] dp = new int[n + 1][amount + 1];
        // base case
        for (int i = 0; i <= n; i++)
            dp[i][0] = 1;

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= amount; j++)
                if (j >= coins[i - 1])
                    dp[i][j] = dp[i - 1][j] + dp[i][j - coins[i - 1]];
                else
                    dp[i][j] = dp[i - 1][j];
        }
        return dp[n][amount];
    }
}
