package algorithms.dp.knapsack;

/**
 * 子集背包
 */
public class SubsetKnapsack {
    boolean canPartition(int[] nums) {
        int sum = 0;
        int n = nums.length;
        for (int i = 0; i < n; ++i)
            sum += nums[i];
        // 和为奇数时，不可能划分成两个和相等的集合
        if (sum % 2 != 0)
            return false;
        sum /= 2;
        boolean[][] dp = new boolean[n + 1][sum + 1];
        // base case
        for (int i = 0; i <= n; i++)
            dp[i][0] = true;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= sum; j++) {
                if (j - nums[i - 1] < 0) {
                    dp[i][j] = dp[i - 1][j];// 背包容量不足，不能装入第 i 个物品
                } else {
                    dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]];// 装入或不装入背包
                }
            }
        }
        return dp[n][sum];
    }
}
