package algorithms.dp;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class _22括号生成 {
    ArrayList<String>[] cache = new ArrayList[100];

    public List<String> generateParenthesis(int n) {
        if (cache[n] != null) {
            return cache[n];
        }
        ArrayList<String> ans = new ArrayList<String>();
        if (n == 0) {
            ans.add("");
        } else {
            for (int c = 0; c < n; ++c) {
                for (String left : generateParenthesis(c)) {
                    for (String right : generateParenthesis(n - 1 - c)) {
                        ans.add("(" + left + ")" + right);
                    }
                }
            }
        }
        cache[n] = ans;
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(new _22括号生成().generateParenthesis(4).size());
    }
}
