package algorithms.dp;

public class _53最大子数组和 {
    public int maxSubArray(int[] nums) {
        // dp[i]: 以nums[i-1]为结尾的具有最大和的连续子数组
        int[] dp = new int[nums.length + 1];
        int m = nums[0] = -65535;
        for (int i = 0; i < nums.length; ++i) {
            dp[i + 1] = Math.max(dp[i] + nums[i], nums[i]);
            m = Math.max(dp[i + 1], m);
        }
        return m;
    }
}
