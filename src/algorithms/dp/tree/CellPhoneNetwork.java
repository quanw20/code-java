package algorithms.dp.tree;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * https://ac.nowcoder.com/acm/contest/25022/1010
 * 树的最小支配集
 * 
 * 给定一颗无向树，问最少用多少个点可以覆盖掉其他的所有点
 * （覆盖：他自己和与他相邻的点都算被覆盖）
 * 
 * 1. 状态：
 * 选他，选他儿子，选他父亲
 * dp[i][0]: 选点i，并且以点i为根的子树都被覆盖
 * dp[i][1]: 不选点i，i被其儿子覆盖（儿子至少选一个）
 * dp[i][2]: 不选点i，i被其父亲覆盖（儿子可选可不选）
 * 
 * <pre>
 * 2. 状态转移方程
 * dp[i][0]=1+∑min(dp[u][0],dp[u][1],dp[u][2]) (u是i的儿子)
 * dp[i][2]=∑min(dp[u][1],dp[u][0])
 * dp[i][1]= if(i没有子节点) : INF
 *           else ∑min(dp[u][0],dp[u][1])+inc
 *           其中inc= min(dp[u][0]-dp[u][1]>0?dp[u][0]-dp[u][1]:0);
 * </pre>
 * 
 */
public class CellPhoneNetwork {
    static final int N = 10001, INF = 0xffffff;
    static List<Integer>[] son = new ArrayList[N];
    static int[][] dp = new int[N][3];
    private static int n;

    static {
        for (int i = 0; i < N; i++) {
            son[i] = new ArrayList<>();
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"), StandardCharsets.UTF_8);
        n = in.nextInt();
        int x, y;
        for (int i = 1; i < n; ++i) {
            x = in.nextInt();
            y = in.nextInt();
            son[x].add(y);
            son[y].add(x);
        }
        dfs(1, 0);
        System.out.println(Math.min(dp[1][1], dp[1][0]));
        in.close();
    }

    private static void dfs(int i, int fa) {
        dp[i][0] = 1;
        dp[i][1] = 0;
        dp[i][2] = 0;
        int inc = INF;
        for (int u : son[i]) {
            if (u == fa)
                continue;
            dfs(u, i);
            dp[i][0] += Math.min(dp[u][0], Math.min(dp[u][1], dp[u][2]));
            dp[i][2] += Math.min(dp[u][1], dp[u][0]);
            dp[i][1] += Math.min(dp[u][1], dp[u][0]);
            inc = Math.min(inc, dp[u][0] - dp[u][1]);
        }
        inc = inc > 0 ? inc : 0;
        dp[i][1] += inc;
    }
}
