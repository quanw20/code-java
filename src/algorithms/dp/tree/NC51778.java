package algorithms.dp.tree;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 题目描述
 * 有N名职员，编号为1~N。他们的关系就像一棵树，父节点就是子节点的直接上司。
 * 每个职员有一个快乐指数，用整数 Hi 给出，其中 1≤i≤N,父子不能同时选择,求快乐指数最大值
 * 
 * 输入描述:
 * 第一行一个整数N。
 * 接下来N行，第 i 行表示 i 号职员的快乐指数Hi。
 * 接下来N-1行，每行输入一对整数L, K,表示K是L的直接上司。
 * 最后一行输入0,0。
 * 输出描述:
 * 输出最大的快乐指数。
 */
public class NC51778 {
    static int N;
    static int[] H = new int[6001];
    static int[][] f = new int[6001][2];// dp数组

    static List<Integer>[] son = new ArrayList[6001];// 父子关系
    static {
        for (int i = 0; i < 6001; ++i)
            son[i] = new ArrayList<>();
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("a.txt"));
        N = in.nextInt();
        for (int i = 1; i <= N; ++i) {
            H[i] = in.nextInt();
        }
        int root = N * (N + 1) / 2;// 根
        int x, y;
        for (int i = 1; i < N; ++i) {
            x = in.nextInt();
            y = in.nextInt();
            son[y].add(x);
            root -= x;//计算根
        }
        dfs(root);
        System.out.println(Math.max(f[root][0], f[root][1]));
    }

    private static void dfs(int i) {
        f[i][0] = 0;
        f[i][1] = H[i];
        for (int j : son[i]) {
            dfs(j);
            f[i][0] += Math.max(f[j][0], f[j][1]);
            f[i][1] += f[j][0];
        }
    }
}
