package algorithms.dp.greedy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class 字典序最小问题 {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("src\\algorithms\\dp\\greedy\\字典序最小问题.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        char[] s = in.next().toCharArray();
        char[] t = new char[n];
        int i = 0, j = n - 1, idx = 0;
        boolean left = false;
        while (i <= j) {
            // 相同只判断前一个
            // if (s[i] < s[j])
            // t[idx++] = s[i++];
            // else if (s[i] > s[j])
            // t[idx++] = s[j--];
            // else {
            // if (i == j || s[i + 1] <= s[j - 1]) {
            // t[idx++] = s[i++];
            // } else {
            // t[idx++] = s[j--];
            // }
            // }
 
            // 相同判断前 直到不相同个
            for (int k = 0; i + k <= j; ++k) {
                if (s[i + k] < s[j - k]) {
                    left = true;
                    break;
                } else if (s[i + k] < s[j - k]) {
                    left = false;
                    break;
                }
            }
            if (left)
                t[idx++] = s[i++];
            else
                t[idx++] = s[j--];
            // 也可以直接输出 
        }
        System.out.println(new String(t));
    }
}
