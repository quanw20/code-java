package algorithms.dp.greedy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class Army {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(Path.of("src\\algorithms\\dp\\greedy\\Army.txt"), StandardCharsets.UTF_8);
        int n = in.nextInt();
        int r = in.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; ++i) {
            a[i] = in.nextInt();
        }
        int res = 0, i = 0;
        while (i < n) {
            int s = a[i];
            while (i < n && a[i] <= s + r)
                ++i;
            int p = a[i - 1];
            while (i < n && a[i] <= p + r)
                ++i;
            ++res;
        }
        System.out.println(res);
    }
}
