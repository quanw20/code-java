package algorithms.dp;


public class Fib {
    /**
     * 使用循环计算fibonacci
     * 
     * @param n
     * @return
     */
    public static int fibLoop(int n) {
        if (n <= 2)
            return 1;
        int a = 1, b = 1;
        while (n-- != 2) {
            b = a + b;
            a = b - a;
        }
        return b;
    }

    /**
     * 使用简单递归
     * 
     * @param n
     * @return
     */
    public static int fibRecursive(int n) {
        if (n == 1 || n == 2)
            return 1;
        return fibRecursive(n - 1) + fibRecursive(n - 2);
    }

    /**
     * 带备忘录的fib
     * 
     * @param n
     * @return
     */

    public static int fibRecord(int n) {
        int[] rec = new int[n + 1];
        int fib = fib(rec, n);
        return fib;
    }

    /**
     * 带备忘录的fib计算核心函数
     * 
     * @param rec
     * @param n
     * @return
     */
    private static int fib(int[] rec, int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        if (rec[n] > 0)
            return rec[n];
        rec[n] = fib(rec, n - 1) + fib(rec, n - 2);
        return rec[n];
    }

    /**
     * 使用矩阵计算fib
     * 
     * @param n
     * @return
     */
    public static int fibMatrix(int n) {
        if (n <= 2)
            return 1;
        int[][] a = { { 1, 1 }, { 1, 0 } };
        int[][] pow = matrixPow(a, n);
        return pow[1][0];
    }

    /**
     * 矩阵乘法 A(m*n)xB(n*p)=C(m*p)
     * 
     * @param a
     * @param b
     * @return
     */
    public static int[][] matrixMutiply(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b[0].length];
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < b[0].length; j++)
                for (int k = 0; k < b.length; k++)
                    c[i][j] += a[i][k] * b[k][j];
        return c;
    }

    /**
     * 矩阵 快速幂
     * 
     * 计算矩阵A的b次幂
     * 
     * @param a
     * @param b
     * @return
     */
    public static int[][] matrixPow(int[][] a, int b) {
        int[][] c = new int[a.length][a.length];
        // 单位阵
        for (int i = 0; i < a.length; i++)
            for (int j = 0; j < a[i].length; j++)
                if (i == j)
                    c[i][j] = 1;
        while (b > 0) {
            if ((b & 1) != 0) {
                c = matrixMutiply(a, c);
            }
            a = matrixMutiply(a, a);
            b >>= 1;
        }
        return c;
    }

    /**
     * 公式
     * 
     * @param n
     * @return
     */
    public static int fibFormula(int n) {
        double sqr = Math.sqrt(5);
        double ret = (Math.pow((1 + sqr) / 2, n) - Math.pow((1 - sqr) / 2, n)) / sqr;
        return (int) ret;
    }

}
