package algorithms.dp;

import java.util.Arrays;

public class Coins {
    /**
     * 递归
     * 
     * 写是自顶向下
     * 想是自底向上
     * 
     * 解决简单情况下的问题
     * 推广到稍微复杂的情况下的问题
     * 
     * 递推次数明确,用迭代
     * 有封闭形式,直接求解
     */

    public static int countW(int n) {
        if (n <= 0)
            return 1;
        return collectCoins(n, new int[] { 1, 5, 10, 25 }, 3);
    }

    /**
     * 用coins凑出n 有多少种凑法
     * 
     * <pre>
     * +--+--+--+--+--+--+--+--+--+--+--+--+
     * |  | 0| 1| 2| 3| 4| 5| 6|10|11|15|25|
     * +--+--+--+--+--+--+--+--+--+--+--+--+
     * | 1| 1  1  1  1  1  1  1  1  1  1  1|
     * | 5| 1  1  1  1  1  2  2  3  3  4  6|
     * |10| 1  1  1  1  1  2  2  4  4  6 12|
     * |25| 1  1  1  1  1  2  2  4  4  4 13|
     * +--+--+--+--+--+--+--+--+--+--+--+--+
     * </pre>
     * 
     * @param n
     * @param coins
     * @param cur
     * @return
     */
    public static int collectCoins(int n, int[] coins, int cur) {
        if (cur <= 0)
            return 1;
        int res = 0;
        for (int i = 0; i * coins[cur] <= n; i++) {
            int remain = n - i * coins[cur];
            res += collectCoins(remain, coins, cur - 1);
        }
        return res;
    }

    public static int collectCoinsLoop(int n, int[] coins) {
        int[][] helper = new int[coins.length][n + 1];
        Arrays.fill(helper[0], 1);
        for (int row = 1; row < coins.length; row++)
            for (int col = 0; col <= n; col++)
                // 状态转移方程 helper[row][col]=helper[row - 1][col] + helper[row -
                // 1][col-coins[row]]+...+ helper[row - 1][col-n*coins[row]]
                for (int cnt = col; cnt >= 0; cnt -= coins[row])
                    helper[row][col] += helper[row - 1][cnt];
        return helper[coins.length - 1][n];
    }
}
