package algorithms.dp.path;

/**
 * 这道题是在 62. 不同路径 的基础上，增加了路径成本概念
 */
public class _64最小路径和 {
    int[][] dp;
    int m, n;

    // 0,0 -> m,n
    public int minPathSum(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        dp = new int[m][n];// 到达(i,j)的最小路径和
        dp[0][0] = grid[0][0];
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i > 0 && j > 0)
                    dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
                else if (i > 0)
                    dp[i][j] = dp[i - 1][j] + grid[i][j];
                else if (j > 0)
                    dp[i][j] = dp[i][j - 1] + grid[i][j];
            }
        }
        return dp[m - 1][n - 1];
    }

    // 如果要输出总和最低的路径呢
    int[] g;

    public int minPathSum1(int[][] grid) {
        m = grid.length;
        n = grid[0].length;
        dp = new int[m][n];// 从(m,n)到达(i,j)的最小路径和
        g = new int[m * n];
        dp[m - 1][n - 1] = grid[m - 1][n - 1];
        for (int i = m - 1; i >= 0; --i) {
            for (int j = n - 1; j >= 0; --j) {
                if (i < m - 1 && j < n - 1) {
                    dp[i][j] = Math.min(dp[i + 1][j], dp[i][j + 1]) + grid[i][j];
                    g[getIdx(i, j)] = dp[i + 1][j] < dp[i][j + 1] ? getIdx(i + 1, j) : getIdx(i, j + 1);
                } else if (i < m - 1) {
                    dp[i][j] = dp[i + 1][j] + grid[i][j];
                    g[getIdx(i, j)] = getIdx(i + 1, j);
                } else if (j < n - 1) {
                    dp[i][j] = dp[i][j + 1] + grid[i][j];
                    g[getIdx(i, j)] = getIdx(i, j + 1);
                }
            }
        }
        printPath();
        return dp[0][0];
    }

    void printPath() {
        int idx = getIdx(0, 0);
        for (int i = 1; i <= m + n; i++) {
            if (i == m + n)
                continue;
            int x = parseIdx(idx)[0], y = parseIdx(idx)[1];
            System.out.print("(" + x + "," + y + ") ");
            idx = g[idx];
        }
        System.out.println(" ");

    }

    int[] parseIdx(int idx) {
        return new int[] { idx / n, idx % n };
    }

    int getIdx(int x, int y) {
        return x * n + y;
    }

    public static void main(String[] args) {
        int[][] grid = { { 1, 3, 1 }, { 1, 5, 1 }, { 4, 2, 1 } };
        _64最小路径和 a = new _64最小路径和();
        int ms = a.minPathSum1(grid);
        System.out.println(ms);
    }

}
