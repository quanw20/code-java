package algorithms.dp.path;

import algorithms.Util;

public class _931下降路径最小和 {
    int dp[][];// (i,j) :->(i,i)的最短路径
    int m, n;

    public int minFallingPathSum1(int[][] matrix) {
        m = matrix.length;
        n = matrix[0].length;
        for (int i = m - 2; i >= 0; --i) {
            for (int j = 0; j < n; ++j) {
                int min = matrix[i + 1][j];
                if (j > 0)
                    min = Math.min(min, matrix[i + 1][j - 1]);
                if (j < n - 1)
                    min = Math.min(min, matrix[i + 1][j + 1]);
                matrix[i][j] += min;
            }
        }
        int ans = Integer.MAX_VALUE;
        for (int x : matrix[0])
            ans = Math.min(ans, x);
        return ans;
    }

    public int minFallingPathSum(int[][] grid) {
        int n = grid.length;
        int first_sum = 0, first_pos = -1, second_sum = 0;
        for (int i = 0; i < n; ++i) {
            int fs = Integer.MAX_VALUE, fp = -1, ss = Integer.MAX_VALUE;
            for (int j = 0; j < n; ++j) {
                int cur_sum = (first_pos != j ? first_sum : second_sum) + grid[i][j];
                if (cur_sum < fs) {
                    ss = fs;
                    fs = cur_sum;
                    fp = j;
                }
                else if (cur_sum < ss) {
                    ss = cur_sum;
                }
            }
            first_sum = fs;
            first_pos = fp;
            second_sum = ss;
        }
        return first_sum;
    }

    public static void main(String[] args) {
        _931下降路径最小和 a = new _931下降路径最小和();
        int[][] matrix = { { -73, 61, 43, -48, -36 }, { 3, 30, 27, 57, 10 }, { 96, -76, 84, 59, -15 },
                { 5, -49, 76, 31, -7 }, { 97, 91, 61, -46, 67 } };
        Util.print(matrix);
        int m = a.minFallingPathSum(matrix);
        System.out.println(m);
    }
}
