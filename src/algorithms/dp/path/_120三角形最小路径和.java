package algorithms.dp.path;

import java.util.ArrayList;
import java.util.List;

public class _120三角形最小路径和 {
    int[] dp;//[无后效性]

    public int minimumTotal(List<List<Integer>> triangle) {
        // 在求第 i 行的状态时只依赖于第 i+1 行的状态 
        List<Integer> ret = null;
        for (int i = triangle.size() - 1; i > 0; --i) {
            ret = triangle.get(i - 1);
            List<Integer> ls = triangle.get(i);
            for (int j = 0, m = ret.size(); j < m; ++j) {
                ret.set(j, Math.min(ls.get(j), ls.get(j + 1)) + ret.get(j));
            }
        }
        System.out.println(ret);
        return ret.get(0);
    }

    public static void main(String[] args) {
        List<List<Integer>> al = new ArrayList<>();
        al.add(new ArrayList<>() {
            {
                add(2);
            }
        });
        al.add(new ArrayList<>() {
            {
                add(3);
                add(4);
            }
        });
        al.add(new ArrayList<>() {
            {
                add(6);
                add(5);
                add(7);
            }
        });
        al.add(new ArrayList<>() {
            {
                add(4);
                add(1);
                add(8);
                add(3);
            }
        });
        _120三角形最小路径和 a = new _120三角形最小路径和();
        int m = a.minimumTotal(al);
        System.out.println(m);

    }
}
