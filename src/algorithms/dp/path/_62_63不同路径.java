package algorithms.dp.path;

public class _62_63不同路径 {
    /**
     * <pre>
     * 1. 如何确定可以使用动态规划来求解问题
     * 
     * 通常我们要从「有无后效性」进行入手分析。
     * 如果对于某个状态，我们可以只关注状态的值，而不需要关注状态是如何转移过来的话，那么这就是一个无后效性的问题，可以考虑使用 DP 解决。
     * 
     * 另外一个更加实在的技巧，我们还可以通过 数据范围 来猜测是不是可以用 DP 来做。
     * 因为 DP 是一个递推的过程，因此如果数据范围是 10^5 ~10^6的话，可以考虑是不是可以使用一维 DP 来解决；
     * 如果数据范围是 10^2~10^3的话，可以考虑是不是可以使用二维 DP 来做 ...
     * 
     * 2. 如何确定本题的状态定义
     * 
     * 相当一部分题目的状态定义是与「结尾」或「答案」有所关联的
     * 
     * 3. 如何确定状态转移方程
     * 
     * 如果我们的状态定义猜对了，状态转移方程就是对「最后一步的分情况讨论」。
     * 如果我们有一个对的状态定义的话，基本上状态转移方程就是呼之欲出。
     * 因此一定程度上，状态转移方程可以反过来验证我们状态定义猜得是否正确：
     * 如果猜了一个状态定义，然后发现无法列出涵盖所有情况（不漏）的状态转移方程，多半就是状态定义猜错了，赶紧换个思路，而不是去死磕状态转移方程。
     * 
     * 4. 对状态转移的要求是什么
     * 
     * 我们的状态转移是要做到「不漏」还是「不重不漏」取决于问题本身：
     * 如果是求最值的话，我们只需要确保「不漏」即可，因为重复不影响结果。
     * 如果是求方案数的话，我们需要确保「不重不漏」。
     * 
     * 5. 如何分析动态规划的时间复杂度
     * 
     * 对于动态规划的复杂度/计算量分析，有多少个状态，复杂度/计算量就是多少。
     * 因此一维 DP 的复杂度通常是线性的 O(n)，而二维 DP 的复杂度通常是平方的 O(n^2)
     * </pre>
     */
    int[][] dp;

    public int uniquePaths(int m, int n) {
        dp = new int[m + 1][n + 1];
        dp[1][1] = 1;
        for (int i = 2; i <= m; ++i)
            dp[i][1] = 1;
        for (int i = 2; i <= n; ++i)
            dp[1][i] = 1;
        return dfs1(m, n);
    }

    private int dfs1(int m, int n) {
        if (m == 1 || n == 1)
            return dp[m][n];
        if (dp[m][n] != 0)
            return dp[m][n];
        dp[m][n] = dfs1(m - 1, n) + dfs1(m, n - 1);
        return dp[m][n];
    }

    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid[0][0] == 1)
            return 0;
        int m = obstacleGrid.length, n = obstacleGrid[0].length;
        dp = new int[m][n];// dp[i][j] 到达(i,j)处有dp[i][j]中走法
        dp[0][0] = 1;
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (obstacleGrid[i][j] != 1) {
                    if (i > 0 && j > 0 && obstacleGrid[i - 1][j] != 1 && obstacleGrid[i][j - 1] != 1)
                        dp[i][j] = dp[i - 1][j] + dp[i][j - 1];// 从上边,右边都能到当前位置
                    else if (i > 0 && obstacleGrid[i - 1][j] != 1)
                        dp[i][j] = dp[i - 1][j];// 只能从上边到达当前位置 eg.左边界和左边有障碍的情况
                    else if (j > 0 && obstacleGrid[i][j - 1] != 1)
                        dp[i][j] = dp[i][j - 1];// 只能从左边到达当前位置 eg.上边界和上边有障碍的情况
                }
            }
        }
        return dp[m - 1][n - 1];
    }

    public static void main(String[] args) {

    }
}
