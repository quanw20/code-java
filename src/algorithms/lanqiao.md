# 蓝桥杯考点

## 1.  基本解法

1、举例法:具体例子,到一般规则(公式符号化)

2、模式匹配法:相似问题=>现有问题(经典的变体)

3、简化推广法:从筒化版,到复杂版(修改约束条件)

4、简单构造法:从n=1开始(递推或递归)

5、数据结构头脑风暴法:
    链表? 数组? 二叉树? 堆栈? 队列? 前缀和? 树状数组? 区间树?
    
## 1.  基本算法

    枚举 enumerat  
    排序 sort  
    搜索 search  
    计数 count 
    分治 devide&conquer  
    前缀和 prefix sum 
        一维、二维  
    差分 difference   
    倍增 increase 
    尺取 segment take 
    贪心 greed  

## 2.  动态规划 Dynamic Plan

    背包问题  
    线性 DP  
    最长上升子序列(LIS)  
    数位(计数) DP  
    压状(状态压缩) DP  

## 3.  数学

    数论 numbertheory  
        余数 remainder    
        GCD Greatest Common Divisor   
        LCM  
        (质)因子分解  
        唯一分解定理  
    组合数学    
    快速幂  

## 4.  字符串

    简单字符串处理  

## 5.  图论

    遍历: DFS, BFS  
    最短路  
        Dijkstra, BellmanFord, Floyd  
    最小生成树   
        Prime, Kruskal  

## 6.  数据结构

    数组/链表 ArrayList/LinkedList(几乎不用)
    字符串  String, StringBuilder
    队列 Queue
    栈 Stack
    树 BinaryTree
    图 Graph
    堆 PriorityQueue
    平衡树 AVL tree
    线段树 SegmentTree
    树状数组 Tree like Array  
    并查集 Union Find

t2013
_01世纪末的星期   时间日期API
_02振兴中华       递归
_02马虎的算式     枚举
_03梅森数        大数
_04颠倒的价格牌   枚举
_04黄金连分数     递归（递推）
_05三部排序       快速排序（三指针）
_06逆波兰表达式    递归（递推）整体考虑
_07错误票据       字符处理
_08带分数         全排列、枚举（剪枝）
_08幸运数         数组
_09剪格子         深搜+回溯
_10大臣的旅费      图+深搜+树的直径

_01猜年龄       枚举
_02李白打酒     dfs
_03神奇算式     枚举、字符串
_05锦标赛       数组、二叉堆
_06六角填数     全排列
_07绳圈         递推
_08兰顿蚂蚁     模拟
_09斐波那契*    矩阵算fib,矩阵快速幂     
_10波动数列*    高级动态规划、状态压缩


