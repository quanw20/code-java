package algorithms.search;

import org.junit.jupiter.api.Test;

/**
 * 二分
 * 
 * <pre>
 * 
 * 狭义: 在有序数组中搜索指定元素, 单调函数找零点
 * 
 * 广义: 单调函数f(x)中,找 x 满足f(x)=target
 * 
 * 首先，你要从题目中抽象出一个自变量 x，一个关于 x 的函数 f(x)，以及一个目标值 target。
 * 同时，x, f(x), target 还要满足以下条件：
 *      1、f(x) 必须是在 x 上的单调函数（单调增单调减都可以）。
 *      2、题目是让你计算满足约束条件 f(x) == target 时的 x 的值。
 * </pre>
 */
public class BinarySearch {
    // 单调函数 f(x)
    int f(int[] arr, int x) {
        return arr[x];// 具体情况具体分析
    }

    /**
     * 二分查找 - 左边界(>= tar 的第一个值的下标)
     * 
     * 如果「有序数组」中存在多个 target 元素，
     * 那么这些元素肯定挨在一起，这里就涉及到算法应该返回最左侧的那个 target 元素的索引
     * 还是最右侧的那个 target 元素的索引
     * 
     * @param arr    数组
     * @param target 目标值
     * @return [0,arr.length-1]
     */
    int leftBound(int[] arr, int target) {
        int lo = 0, hi = arr.length;// 右边界取不到
        while (lo < hi) {
            int mid = lo + (hi - lo >> 1);
            if (f(arr, mid) >= target)
                hi = mid;
            else
                lo = mid + 1;
        }
        return lo;
    }

    /**
     * 二分查找 - 右边界(> target)的第一个值
     * 
     * @param arr    数组
     * @param target 目标值
     * @return [0,arr.length-1]
     */
    int rightBound(int[] arr, int target) {
        int lo = 0, hi = arr.length;
        while (lo < hi) {
            int mid = lo + (hi - lo >> 1);
            if (f(arr, mid) <= target)
                lo = mid + 1;
            else
                hi = mid;
        }
        return lo;
    }

    /**
     * 二分查找(整数二分)
     * 先找到确定可能的答案区间，就是我要找的那个答案肯定在这个区间里。
     * 然后根据题目的条件，判断中值是否满足题意。
     * 不管满不满足我最后都能丢掉一半的区间，进一步逼近答案，最后得到答案
     * 
     * <pre>
     * bool check(int mid){
     *      if （满足题意）return true;
     *      else return false;
     * }
     * int bsearch(int left, int right) {
     *      while (left < right) {
     *      int mid = (left + right) / 2;
     *      if (check(mid))
     *          right = mid - 1;
     *      else
     *      left = mid;
     *      }
     *      return left;
     * }
     * </pre>
     * 
     * @param arr    数组
     * @param target 目标值
     * @return
     */
    int binarySearch(int[] arr, int target) {
        int lo = 0, hi = arr.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo >> 1);
            int f = f(arr, mid);
            if (f == target)
                return mid;
            else if (f > target)
                hi = mid - 1;
            else if (f < target)
                lo = mid + 1;
        }
        return -1;
    }

    @Test
    void testBS() {
        BinarySearch bs = new BinarySearch();
        int[] a = new int[] { 0, 1, 2, 3, 4, 5, 6 };

        int lb = bs.leftBound(a, -1);
        int lb1 = bs.leftBound(a, 7);
        System.out.println(lb);
        System.out.println(lb1);

        int rb = bs.rightBound(a, -1);
        int rb1 = bs.rightBound(a, 7);
        System.out.println(rb);
        System.out.println(rb1);
    }
}