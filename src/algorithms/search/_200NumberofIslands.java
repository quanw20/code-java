package algorithms.search;
/**
 * dfs
 */
public class _200NumberofIslands {
    int[][] d = { { 1, 0 }, { 0, -1 }, { -1, 0 }, { 0, 1 } };

    public int numIslands(char[][] grid) {
        int ret = 0;
        for (int i = 0; i < grid.length; ++i) {
            for (int j = 0; j < grid[0].length; ++j) {
                if (grid[i][j] == '1') {
                    ++ret;
                    dfs(grid, i, j);
                }
            }
        }
        return ret;
    }

    private void dfs(char[][] grid, int i, int j) {
        grid[i][j] = 0;
        for (int k = 0; k < d.length; k++) {
            int x = i + d[k][0], y = j + d[k][1];
            if (0 <= x && x < grid.length && 0 <= y && y < grid[0].length && grid[x][y] == '1')
                dfs(grid, x, y);
        }
    }

    public static void main(String[] args) {
        char[][] grid = {
                { '1', '1', '1', '1', '0' },
                { '1', '1', '0', '1', '0' },
                { '1', '1', '0', '0', '0' },
                { '0', '0', '0', '0', '0' }
        };
        System.out.println(new _200NumberofIslands().numIslands(grid));
    }
}
