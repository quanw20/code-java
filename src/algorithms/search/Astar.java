package algorithms.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

public class Astar {

  private static final int[][] DIREC = { { -1, 0 }, { -1, 1 }, { 0, 1 }, { 1, 1 },
      { 1, 0 }, { 1, -1 }, { 0, -1 }, { -1, -1 } };

  public static void main(String[] args) {
    Random r = new Random(1L);
    final int rows = r.nextInt(10, 20);
    final int cols = r.nextInt(10, 20);
    final int x1 = r.nextInt(0, rows);
    final int y1 = r.nextInt(0, cols);
    final int x2 = r.nextInt(1, rows);
    final int y2 = r.nextInt(1, cols);

    // generate a two-dimension array filled with 0
    int map[][] = new int[rows][cols];

    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        if (r.nextInt(0, 10) > 6) // p=4/10
          map[i][j] = 1;
      }
    }

    print(map);

    findPath(map, x1, y1, x2, y2);
  }

  private static void print(int[][] map) {
    System.out.print("   ");
    for (int i = 0; i < map[1].length; i++) {
      System.out.printf("%2d", i);
    }
    System.out.println();
    for (int i = 0; i < map.length; i++) {
      System.out.printf("%2d: ", i);
      for (int j = 0; j < map[1].length; j++) {
        System.out.print(map[i][j] + " ");
      }
      System.out.println();
    }
  }

  private static void findPath(int[][] map, int x1, int y1, int x2, int y2) {
    List<Point> openList = new ArrayList<Point>();
    List<Point> closeList = new ArrayList<Point>();
    boolean findFlag = false;
    Point termPos = null;
    // 起始点
    Point startPos = new Point(x1, y1, calcH(x1, y1, x2, y2));
    openList.add(startPos);
    do {
      // 通过在开启列表中找到F值最小的点作为当前点
      Point currentPos = openList.get(0);
      for (int i = 0; i < openList.size(); i++) {
        if (currentPos.F > openList.get(i).F) {
          currentPos = openList.get(i);
        }
      }
      // 将找到的当前点放到关闭列表中，并从开启列表中删除
      closeList.add(currentPos);
      openList.remove(currentPos);

      // 遍历当前点对应的8个相邻点
      for (int i = 0; i < DIREC.length; i++) {
        int tmpX = currentPos.row + DIREC[i][0];
        int tmpY = currentPos.col + DIREC[i][1];
        if (tmpX < 0 || tmpX >= map.length || tmpY < 0 || tmpY >= map[0].length) {
          continue;
        }
        // 创建对应的点
        Point tmpPos = new Point(tmpX, tmpY, calcH(tmpX, tmpY, x2, y2), currentPos);
        // map中对应的格子中的值为1（障碍）， 或对应的点已经在关闭列表中
        if (map[tmpX][tmpY] == 1 || closeList.contains(tmpPos)) {
          continue;
        }
        // 如果不在开启列表中，则加入到开启列表
        if (!openList.contains(tmpPos)) {
          openList.add(tmpPos);
        } else {
          // 如果已经存在开启列表中，则用G值考察新的路径是否更好，如果该路径更好，则把父节点改成当前格并从新计算FGH
          Point prePos = null;
          for (Point pos : openList) {
            if (pos.row == tmpX && pos.col == tmpY) {
              prePos = pos;
              break;
            }
          }
          if (tmpPos.G < prePos.G) {
            prePos.setFa(currentPos);
          }
        }
      }
      // 判断终点是否在开启列表中
      for (Point tpos : openList) {
        if (tpos.row == x2 && tpos.col == y2) {
          termPos = tpos;
          findFlag = true;
          break;
        }
      }

    } while (openList.size() != 0);

    if (!findFlag) {
      System.out.println("no valid path!");
      return;
    }

    Stack<String> resStack = new Stack<String>();System.out.println(".()");
    String pattern = "(%d, %d)";
    if (termPos != null) {
      resStack.push(String.format(pattern, termPos.row, termPos.col));
      while (termPos.fa != null) {
        termPos = termPos.fa;
        resStack.push(String.format(pattern, termPos.row, termPos.col));
      }
    }
    StringBuilder sb = new StringBuilder();
    while (!resStack.empty()) {
      sb.append(resStack.pop());
      if (!resStack.empty()) {
        sb.append(" -> ");
      }
    }
    System.out.println(sb.toString());
  }

  /**
   * 计算某个格子的H值
   * 
   * @param x
   * @param y
   * @param tx 终点的x值
   * @param ty 终点的y值
   * @return
   */
  private static int calcH(int x, int y, int tx, int ty) {
    int diff = Math.abs(x - tx) + Math.abs(y - ty);
    return diff * 10;
  }

  static class Point {
    public int F;// Cost Function
    public int G;// Move Function
    public int H;// Heuristic Function
    public Point fa;
    public int row;
    public int col;

    public Point(int row, int col, int H) {
      this(row, col, H, null);
    }

    public Point(int row, int col, int H, Point pos) {
      this.H = H;
      this.row = row;
      this.col = col;
      this.fa = pos;
      this.G = calcG();
      this.F = G + H;
    }

    /**
     * 计算某个点到起始点的代价G
     * 
     * @return
     */
    private int calcG() {
      if (fa == null)
        return 0;
      if (fa.row != this.row && fa.col != this.col) {
        return 14 + fa.G;
      }
      return 10 + fa.G;
    }

    public void setFa(Point pos) {
      this.fa = pos;
      this.G = calcG();
      this.F = G + H;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj == null || !(obj instanceof Point)) 
        return false;
      Point pos = (Point) obj;
      return this.row == pos.row && this.col == pos.col;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + row;
      result = prime * result + col;
      return result;
    }

  }

}
