package algorithms;

import static algorithms.Util.*;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class UtilTests {
    @Test
    public void test_range() {
        print(range(10));
        print(range(2, 10));
        print(range(2, 10, 2));
    }

    @Test
    public void test_getRandomArray() {
        print(getRandomArray(6));
    }

    @Test
    public void test_m2d() {
        print(m2d(3, 4));
    }

    @Test
    public void test_toIntArray() {
        int[] a = { 1, 2, 3, 4, 5 };
        String s = Arrays.toString(a);
        System.out.println(s);
        int[] b = toIntArray(s);
        print(a);
        System.out.println();
        print(b);
    }
}
