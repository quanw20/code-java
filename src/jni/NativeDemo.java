package jni;

public class NativeDemo {
    public native void sayHello();

    public static void main(String[] args) {
        new NativeDemo().sayHello();// java.lang.UnsatisfiedLinkError: 'void jni.NativeDemo.sayHello()'
    }
}